-- MySQL dump 10.13  Distrib 5.1.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: conscientfootball_production
-- ------------------------------------------------------
-- Server version	5.1.41-3ubuntu12.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_size` int(11) DEFAULT NULL,
  `group_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alt_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `asset_order` int(11) DEFAULT NULL,
  `attachable_id` int(11) DEFAULT NULL,
  `attachable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
INSERT INTO `assets` VALUES (2,'press.jpg','image/jpeg',141795,'assets_overview',NULL,NULL,NULL,NULL,1,'award','2017-02-21 11:38:07','2017-02-21 11:38:07'),(4,'12717769_452992908239223_1700440983087642988_n.jpg','image/jpeg',125427,'assets_overview',NULL,NULL,NULL,NULL,1,'award','2017-02-21 11:38:11','2017-02-21 11:38:11'),(5,'12647450_452993544905826_5343263848541701497_n.jpg','image/jpeg',119405,'assets_overview',NULL,NULL,NULL,NULL,1,'award','2017-02-21 11:38:13','2017-02-21 11:38:13'),(8,'Mariya_Tahreem_Khan.jpg','image/jpeg',114482,'main_image',NULL,NULL,NULL,NULL,3,'Award','2017-02-21 11:41:09','2017-02-21 11:41:09'),(9,'Mariya_Tahreem_Khan.jpg','image/jpeg',114482,'assets_overview',NULL,NULL,NULL,NULL,3,'award','2017-02-21 11:41:20','2017-02-21 11:41:20'),(10,'13315414_489436467928200_2539794058584474268_n.jpg','image/jpeg',71474,'main_image',NULL,NULL,NULL,NULL,4,'Award','2017-02-21 11:42:14','2017-02-21 11:42:14'),(11,'13329420_489437091261471_8525549116422737862_o.jpg','image/jpeg',274535,'assets_overview',NULL,NULL,NULL,NULL,4,'award','2017-02-21 11:42:23','2017-02-21 11:42:23'),(12,'13315414_489436467928200_2539794058584474268_n.jpg','image/jpeg',71474,'assets_overview',NULL,NULL,NULL,NULL,4,'award','2017-02-21 11:42:25','2017-02-21 11:42:25'),(13,'13315251_489436847928162_3087638132968395740_n.jpg','image/jpeg',65403,'assets_overview',NULL,NULL,NULL,NULL,4,'award','2017-02-21 11:42:27','2017-02-21 11:42:27'),(14,'13325642_489436734594840_4335697617795509667_n.jpg','image/jpeg',51401,'assets_overview',NULL,NULL,NULL,NULL,4,'award','2017-02-21 11:42:29','2017-02-21 11:42:29'),(15,'img-2.jpg','image/jpeg',65834,'main_image',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:40','2017-02-21 11:44:40'),(16,'img-3.jpg','image/jpeg',66383,'assets_gallery',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:44','2017-02-21 11:44:44'),(17,'img-2.jpg','image/jpeg',65834,'assets_gallery',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:47','2017-02-21 11:44:47'),(18,'img-4.jpg','image/jpeg',62860,'assets_gallery',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:49','2017-02-21 11:44:49'),(19,'img-6.jpg','image/jpeg',54313,'assets_gallery',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:53','2017-02-21 11:44:53'),(20,'img-1.jpg','image/jpeg',53394,'assets_gallery',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:55','2017-02-21 11:44:55'),(21,'img-7.jpg','image/jpeg',50861,'assets_gallery',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:57','2017-02-21 11:44:57'),(22,'img-5.jpg','image/jpeg',48244,'assets_gallery',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:58','2017-02-21 11:44:58'),(36,'8.jpg','image/jpeg',16013,'main_image',NULL,NULL,NULL,NULL,1,'Blog','2017-03-30 14:35:20','2017-03-30 14:35:20'),(37,'12.jpg','image/jpeg',85921,'main_image',NULL,NULL,NULL,NULL,5,'Award','2017-04-03 09:21:14','2017-04-03 09:21:14'),(40,'12.jpg','image/jpeg',85921,'assets_overview',NULL,NULL,NULL,NULL,5,'award','2017-04-04 05:53:40','2017-04-04 05:53:40'),(49,'122.png','image/png',13750,'main_image',NULL,NULL,NULL,NULL,1,'Video','2017-04-21 12:21:53','2017-04-21 12:21:53'),(50,'122.png','image/png',13750,'main_image',NULL,NULL,NULL,NULL,2,'Video','2017-04-21 12:22:42','2017-04-21 12:22:42'),(58,'12717769_452992908239223_1700440983087642988_n.jpg','image/jpeg',125427,'main_image',NULL,NULL,NULL,NULL,1,'Award','2017-05-19 10:04:35','2017-05-19 10:04:35'),(59,'12694806_452993001572547_2352199351388008052_o.jpg','image/jpeg',125958,'assets_overview',NULL,NULL,NULL,NULL,7,'award','2017-05-19 10:05:38','2017-05-19 10:05:38'),(60,'press.jpg','image/jpeg',141795,'assets_overview',NULL,NULL,NULL,NULL,7,'award','2017-05-19 10:06:03','2017-05-19 10:06:03'),(62,'12694806_452993001572547_2352199351388008052_o.jpg','image/jpeg',80401,'main_image',NULL,NULL,NULL,NULL,7,'Award','2017-05-19 10:08:38','2017-05-19 10:08:38'),(64,'Screen_Shot_2017-05-19_at_17.54.38.png','image/png',165693,'main_image',NULL,NULL,NULL,NULL,4,'News','2017-05-19 12:38:32','2017-05-19 12:38:32'),(66,'Screen_Shot_2017-05-19_at_18.56.12_1.png','image/png',685792,'main_image',NULL,NULL,NULL,NULL,3,'News','2017-05-19 13:34:31','2017-05-19 13:34:31'),(67,'sliderbanner15.jpg','image/jpeg',72828,'main_image',NULL,NULL,NULL,NULL,5,'News','2017-05-19 13:41:20','2017-05-19 13:41:20'),(68,'Screen_Shot_2017-05-19_at_19.05.02.png','image/png',437713,'main_image',NULL,NULL,NULL,NULL,6,'News','2017-05-19 13:43:47','2017-05-19 13:43:47'),(69,'getimage.jpeg','image/jpeg',38631,'main_image',NULL,NULL,NULL,NULL,7,'News','2017-05-19 19:12:18','2017-05-19 19:12:18'),(72,'Screen_Shot_2017-05-20_at_00.37.49.png','image/png',53886,'main_image',NULL,NULL,NULL,NULL,9,'News','2017-05-19 19:23:09','2017-05-19 19:23:09'),(73,'Screen_Shot_2017-05-20_at_00.51.22.png','image/png',31902,'main_image',NULL,NULL,NULL,NULL,10,'News','2017-05-19 19:29:25','2017-05-19 19:29:25'),(78,'Screen_Shot_2017-05-20_at_01.06.51.png','image/png',40182,'main_image',NULL,NULL,NULL,NULL,51,'PressRelease','2017-05-19 19:45:49','2017-05-19 19:45:49'),(80,'Screen_Shot_2017-05-20_at_01.10.58.png','image/png',27252,'main_image',NULL,NULL,NULL,NULL,50,'PressRelease','2017-05-19 19:48:52','2017-05-19 19:48:52'),(81,'barcelona300.jpg','image/jpeg',16738,'main_image',NULL,NULL,NULL,NULL,49,'PressRelease','2017-05-19 19:49:54','2017-05-19 19:49:54'),(82,'Screen_Shot_2017-05-20_at_01.17.28.png','image/png',199172,'main_image',NULL,NULL,NULL,NULL,47,'PressRelease','2017-05-19 19:56:02','2017-05-19 19:56:02'),(83,'Screen_Shot_2017-05-20_at_01.19.04.png','image/png',79282,'main_image',NULL,NULL,NULL,NULL,46,'PressRelease','2017-05-19 19:57:24','2017-05-19 19:57:24'),(84,'bar-963767.jpg','image/jpeg',64595,'main_image',NULL,NULL,NULL,NULL,44,'PressRelease','2017-05-19 19:59:06','2017-05-19 19:59:06'),(85,'Screen_Shot_2017-05-20_at_01.22.36.png','image/png',332531,'main_image',NULL,NULL,NULL,NULL,40,'PressRelease','2017-05-19 20:00:51','2017-05-19 20:00:51'),(86,'youth-859407.jpg','image/jpeg',75650,'main_image',NULL,NULL,NULL,NULL,38,'PressRelease','2017-05-19 20:02:22','2017-05-19 20:02:22'),(87,'youth-859407.jpg','image/jpeg',75650,'main_image',NULL,NULL,NULL,NULL,37,'PressRelease','2017-05-19 20:03:56','2017-05-19 20:03:56'),(88,'Screen_Shot_2017-05-20_at_01.26.39.png','image/png',158836,'main_image',NULL,NULL,NULL,NULL,35,'PressRelease','2017-05-19 20:04:47','2017-05-19 20:04:47'),(89,'WhatsApp_Image_2017-05-20_at_07.30.46.jpeg','image/jpeg',100446,'main_image',NULL,NULL,NULL,NULL,8,'Award','2017-05-20 05:53:14','2017-05-20 05:53:14'),(91,'DSC09442-min.JPG','image/jpeg',1192071,'main_image',NULL,NULL,NULL,NULL,12,'Award','2017-05-20 06:02:24','2017-05-20 06:02:24'),(92,'The_Conscient_football_premiur_Leuge.JPG','image/jpeg',38444,'main_image',NULL,NULL,NULL,NULL,6,'PressRelease','2017-05-20 07:32:07','2017-05-20 07:32:07'),(93,'Fcb_Startfootball_camp_delhi.jpg','image/jpeg',20241,'main_image',NULL,NULL,NULL,NULL,11,'PressRelease','2017-05-20 07:38:44','2017-05-20 07:38:44'),(94,'Conscient_Football_to_provide_Coaching.JPG','image/jpeg',38161,'main_image',NULL,NULL,NULL,NULL,8,'PressRelease','2017-05-20 07:43:57','2017-05-20 07:43:57'),(95,'The_Conscient_football_premiur_Leuge_Closes_its_2010-2011.JPG','image/jpeg',32033,'main_image',NULL,NULL,NULL,NULL,7,'PressRelease','2017-05-20 07:46:33','2017-05-20 07:46:33'),(96,'fc_Barcalone_Camp_Comes_to_Bangalore.jpg','image/jpeg',19911,'main_image',NULL,NULL,NULL,NULL,15,'PressRelease','2017-05-20 08:29:53','2017-05-20 08:29:53'),(97,'Interview_with_Formar.jpg','image/jpeg',17852,'main_image',NULL,NULL,NULL,NULL,13,'PressRelease','2017-05-20 08:32:17','2017-05-20 08:32:17'),(98,'Fc_BArcelona_to_be_First_Club_To_Set_Up_Official_football_School_in_India.jpg','image/jpeg',737114,'main_image',NULL,NULL,NULL,NULL,24,'PressRelease','2017-05-20 08:35:14','2017-05-20 08:35:14'),(99,'barcalona_to_open_football_school_in_India.jpg','image/jpeg',59930,'main_image',NULL,NULL,NULL,NULL,22,'PressRelease','2017-05-20 08:38:31','2017-05-20 08:38:31'),(100,'New_Pic.JPG','image/jpeg',65179,'main_image',NULL,NULL,NULL,NULL,31,'PressRelease','2017-05-20 08:42:37','2017-05-20 08:42:37'),(101,'Fc_Barcalona_Set_Up_New_Pic.jpg','image/jpeg',15320,'main_image',NULL,NULL,NULL,NULL,28,'PressRelease','2017-05-20 08:44:19','2017-05-20 08:44:19'),(102,'Fc_Barcalona_Set_Up_New_Pic.jpg','image/jpeg',15320,'main_image',NULL,NULL,NULL,NULL,32,'PressRelease','2017-05-20 08:45:21','2017-05-20 08:45:21'),(104,'Prithviraj_Kumar.jpg','image/jpeg',56960,'main_image',NULL,NULL,NULL,NULL,2,'Award','2017-05-20 09:04:45','2017-05-20 09:04:45'),(105,'fai-india-u17-futsal-team-690x460.jpg','image/jpeg',84963,'main_image',NULL,NULL,NULL,NULL,10,'Award','2017-05-20 09:06:18','2017-05-20 09:06:18'),(106,'Divyansh_Narang.jpg','image/jpeg',71033,'main_image',NULL,NULL,NULL,NULL,9,'Award','2017-05-20 09:07:55','2017-05-20 09:07:55'),(107,'Akash_Sir_.jpg','image/jpeg',183252,'main_image',NULL,NULL,NULL,NULL,48,'PressRelease','2017-05-20 10:30:05','2017-05-20 10:30:05'),(108,'Akash_Sir_.jpg','image/jpeg',183252,'main_image',NULL,NULL,NULL,NULL,45,'PressRelease','2017-05-20 10:31:48','2017-05-20 10:31:48'),(109,'Akash_Sir_.jpg','image/jpeg',183252,'main_image',NULL,NULL,NULL,NULL,42,'PressRelease','2017-05-20 10:33:09','2017-05-20 10:33:09'),(110,'Akash_Sir_.jpg','image/jpeg',183252,'main_image',NULL,NULL,NULL,NULL,39,'PressRelease','2017-05-20 10:34:58','2017-05-20 10:34:58'),(111,'Akash_Sir_.jpg','image/jpeg',183252,'main_image',NULL,NULL,NULL,NULL,34,'PressRelease','2017-05-20 10:36:29','2017-05-20 10:36:29'),(112,'Akash_Sir_.jpg','image/jpeg',183252,'main_image',NULL,NULL,NULL,NULL,12,'PressRelease','2017-05-20 10:38:21','2017-05-20 10:38:21'),(113,'Akash_Sir_.jpg','image/jpeg',183252,'main_image',NULL,NULL,NULL,NULL,14,'PressRelease','2017-05-20 10:39:33','2017-05-20 10:39:33'),(114,'Akash_Sir_.jpg','image/jpeg',183252,'main_image',NULL,NULL,NULL,NULL,26,'PressRelease','2017-05-20 10:42:36','2017-05-20 10:42:36'),(115,'Akash_Sir_.jpg','image/jpeg',183252,'main_image',NULL,NULL,NULL,NULL,25,'PressRelease','2017-05-20 10:44:07','2017-05-20 10:44:07'),(116,'Akash_Sir_.jpg','image/jpeg',183252,'main_image',NULL,NULL,NULL,NULL,23,'PressRelease','2017-05-20 10:45:49','2017-05-20 10:45:49'),(117,'Akash_Sir_.jpg','image/jpeg',183252,'main_image',NULL,NULL,NULL,NULL,33,'PressRelease','2017-05-20 10:47:40','2017-05-20 10:47:40'),(118,'Akash_Sir_.jpg','image/jpeg',183252,'main_image',NULL,NULL,NULL,NULL,27,'PressRelease','2017-05-20 10:49:18','2017-05-20 10:49:18'),(120,'IMG_20170521_095124.jpg','image/jpeg',2371539,'assets_overview',NULL,NULL,NULL,NULL,13,'award','2017-05-23 06:08:40','2017-05-23 06:08:40'),(122,'IMG_20170521_095122.jpg','image/jpeg',2363262,'main_image',NULL,NULL,NULL,NULL,13,'Award','2017-05-23 06:09:47','2017-05-23 06:09:47'),(124,'Prithviraj_Kumar_(1).jpg','image/jpeg',56960,'assets_overview',NULL,NULL,NULL,NULL,2,'award','2017-05-23 06:12:50','2017-05-23 06:12:50'),(126,'Divyansh_Narang.jpg','image/jpeg',71033,'assets_overview',NULL,NULL,NULL,NULL,9,'award','2017-05-23 06:14:07','2017-05-23 06:14:07'),(127,'fai-india-u17-futsal-team-690x460_(1).jpg','image/jpeg',84963,'assets_overview',NULL,NULL,NULL,NULL,10,'award','2017-05-23 06:15:37','2017-05-23 06:15:37'),(128,'fai-india-u17-futsal-team-690x460_(1).jpg','image/jpeg',84963,'assets_overview',NULL,NULL,NULL,NULL,9,'award','2017-05-23 06:16:04','2017-05-23 06:16:04'),(129,'fai-india-u17-futsal-team-690x460_(1).jpg','image/jpeg',84963,'assets_overview',NULL,NULL,NULL,NULL,2,'award','2017-05-23 06:17:12','2017-05-23 06:17:12'),(130,'DSC09440.JPG','image/jpeg',4699811,'assets_overview',NULL,NULL,NULL,NULL,12,'award','2017-05-23 07:11:27','2017-05-23 07:11:27'),(132,'DSC09442.JPG','image/jpeg',4231449,'assets_overview',NULL,NULL,NULL,NULL,12,'award','2017-05-23 07:12:18','2017-05-23 07:12:18'),(133,'WhatsApp_Image_2017-05-20_at_07.30.46.jpeg','image/jpeg',100446,'assets_overview',NULL,NULL,NULL,NULL,8,'award','2017-05-23 07:15:30','2017-05-23 07:15:30'),(134,'11113840_368326000039248_5212229329907703849_n.jpg','image/jpeg',40544,'main_image',NULL,NULL,NULL,NULL,12,'News','2017-05-23 07:46:53','2017-05-23 07:46:53'),(135,'av.jpg','image/jpeg',141795,'main_image',NULL,NULL,NULL,NULL,13,'News','2017-05-23 07:53:47','2017-05-23 07:53:47'),(136,'11959996_742187875910990_8110314698371474165_n.png','image/png',766123,'main_image',NULL,NULL,NULL,NULL,52,'PressRelease','2017-05-23 07:58:43','2017-05-23 07:58:43'),(137,'11148456_399951563543358_7967207614143276923_o.jpg','image/jpeg',524043,'main_image',NULL,NULL,NULL,NULL,14,'News','2017-05-23 08:02:16','2017-05-23 08:02:16'),(138,'17678_374497249422123_2570185159677895271_n.jpg','image/jpeg',116198,'main_image',NULL,NULL,NULL,NULL,15,'News','2017-05-23 08:05:21','2017-05-23 08:05:21'),(139,'11136634_365090603696121_952971049378834065_n.jpg','image/jpeg',59675,'main_image',NULL,NULL,NULL,NULL,16,'News','2017-05-23 08:07:59','2017-05-23 08:07:59'),(142,'Koala.jpg','image/jpeg',780831,'main_image',NULL,NULL,NULL,NULL,14,'Award','2017-05-23 09:48:17','2017-05-23 09:48:17');
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `awards`
--

DROP TABLE IF EXISTS `awards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `featured` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `awards`
--

LOCK TABLES `awards` WRITE;
/*!40000 ALTER TABLE `awards` DISABLE KEYS */;
INSERT INTO `awards` VALUES (1,'Avika Singh','','FCBEscola South Delhi center student Avika Singh was a part of the Delhi State Sub- Junior (Under - 14) Girls Football Team in the Nationals held during January 15th and Febuary 2nd in Cuttack, Odisha. Mahira captained the team.',1,1,'2017-02-21 11:36:56','2017-05-20 05:56:25','avika-singh','2016-01-10 00:00:00'),(2,'Prithviraj Kumar','','Prithviraj Kumar who trained with FCBEscola Delhi NCR at our Gurgaon Center got selected for the U-17 Futsal World Cup 2016 in Paraguay.',1,1,'2017-02-21 11:38:50','2017-05-19 12:19:18','prithviraj-kumar','2016-07-09 00:00:00'),(3,'Mariya Tahreem Khan','','FCBEscola student Mariya Tehreem Khan represented Delhi U-18 Juniors national football championship.\r\nThe Tournament was held in Goa.\r\nThe team played against Meghalaya, Goa, Mizoram, Orissa, Chhattisgarh and Manipur. She scored 6 goals in the whole tournament and the team reached semi finals for the first time in 52 years. Well done Mariya!',1,1,'2017-02-21 11:40:41','2017-05-19 10:43:42','mariya-tahreem-khan','2016-02-16 00:00:00'),(4,'Rohit Gusain','','FCBEscola Delhi NCR player Rohit Gusain(number 7) representing India Under 14 team at the AFC U-14 Festival of Football held in Dushanbe, Tajikistan. The team played against Nepal, Iran, Kyrgyzstan, Tajikistan and Afghanistan. He was also a part of the Conscient Football Under 15 team that played in AIFF youth League held in Chandigarh. Rohit, affectionately known as Speedy by his coaches and teammates at FCBEscola has made us proud.',1,1,'2017-02-21 11:41:44','2017-05-19 10:37:13','rohit-gusain','2016-05-23 00:00:00'),(5,'Gilman Muzaffar','','Gilman Muzaffar, who has been training with us for last 5 years at our Gurgaon center got selected in Reliance Young Champs- ISL official Grassroots Scholarship program. He was one of the 9 boys selected from all over India . ',1,1,'2017-04-03 09:20:16','2017-05-19 10:38:02','gilman-muzaffar','2016-06-19 00:00:00'),(7,'Mahira Jalan','','Mahira Jalan was the captain of the Delhi State Sub- Junior (Under - 14) Girls Football Team in the Nationals held during January 15th and Febuary 2nd in Cuttack, Odisha.',1,1,'2017-05-17 12:48:05','2017-05-19 10:33:00','mahira-jalan','2016-01-10 00:00:00'),(8,'Kshitij Kumar','','Kshitij Kumar, who trained with us for 2 years at our South Delhi center got selected in Reliance Young Champs- ISL official Grassroots Scholarship program. He was one of the 9 boys selected from all over India .',1,1,'2017-05-19 10:24:56','2017-05-19 10:33:34','kshitij-kumar','2016-06-19 00:00:00'),(9,'Divyansh Narang','','Divyansh Narang who trained with FCBEscola Delhi NCR at our Gurgaon Center got selected for the U-17 Futsal World Cup 2016 in Paraguay.',1,1,'2017-05-19 12:17:15','2017-05-19 12:17:48','divyansh-narang','2016-07-09 00:00:00'),(10,'Sreejit Neelkanth','','Sreejit Neelkanth who trained with FCBEscola Delhi NCR at our Gurgaon Center got selected for the U-17 Futsal World Cup 2016 in Paraguay.',1,1,'2017-05-19 12:18:34','2017-05-23 06:15:09','sreejit-neelkanth','2016-07-09 00:00:00'),(11,'Anya Goel','','FCBEscola South Delhi center student Anya Goel was a part of the Delhi State Sub- Junior (Under - 14) Girls Football Team at the Nationals held in Cuttack, Odisha.',1,1,'2017-05-20 05:58:02','2017-05-20 05:58:11','anya-goel','2016-01-10 00:00:00'),(12,'Medha Jairath Verma','','FCBEscola South Delhi center student Medha Jairath Verma was a part of the Delhi State Sub- Junior (Under - 14) Girls Football Team at the Nationals held in Cuttack, Odisha.',1,1,'2017-05-20 05:59:37','2017-05-20 05:59:37','medha-jairath-verma','2016-01-10 00:00:00'),(13,'Advait Sumbly','','Advait Sumbly represented Haryana in the U-14 Nationals held at Varanasi in Dec 2016-Jan 2017.',1,1,'2017-05-20 06:43:48','2017-05-23 06:07:46','advait-sumbly','2017-05-20 00:00:00'),(14,'Test Success Stories 1','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',0,1,'2017-05-23 09:40:29','2017-05-23 09:40:29','test-success-stories-1','2017-05-23 00:00:00');
/*!40000 ALTER TABLE `awards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `short_description` text COLLATE utf8_unicode_ci,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `published_on` date DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metatitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metakeyword` text COLLATE utf8_unicode_ci,
  `metadescription` text COLLATE utf8_unicode_ci,
  `featured` tinyint(1) DEFAULT NULL,
  `counter` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogs`
--

LOCK TABLES `blogs` WRITE;
/*!40000 ALTER TABLE `blogs` DISABLE KEYS */;
INSERT INTO `blogs` VALUES (1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tum ille timide vel potius verecunde: Facio, inquit. Nam memini etiam quae nolo, oblivisci non possum Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tum ille timide vel potius verecunde: Facio, inquit. Nam memini etiam quae nolo, oblivisci non possum','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tum ille timide vel potius verecunde: Facio, inquit. Nam memini etiam quae nolo, oblivisci non possum Lorem ipsum dolor sit amet','lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit',1,'2017-02-17','uncategorized',NULL,NULL,NULL,1,0,'2017-02-17 11:28:57','2017-02-17 11:28:57'),(2,'Test Blog 1','<p><strong style=\"margin: 0px; padding: 0px; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><strong style=\"margin: 0px; padding: 0px; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','test-blog-1',0,'2017-05-23','','','','',0,0,'2017-05-23 09:37:50','2017-05-23 09:37:50');
/*!40000 ALTER TABLE `blogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `careers`
--

DROP TABLE IF EXISTS `careers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `careers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume_file_size` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `careers`
--

LOCK TABLES `careers` WRITE;
/*!40000 ALTER TABLE `careers` DISABLE KEYS */;
INSERT INTO `careers` VALUES (1,'sonu','sonupajai22@gmail.com','9898989898','Amura_Attendance_Policy.pdf','application/pdf',72283,'2017-04-04 14:40:45','2017-04-04 14:40:45'),(2,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:02','2017-04-06 20:12:02'),(3,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:07','2017-04-06 20:12:07'),(4,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:12','2017-04-06 20:12:12'),(5,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:16','2017-04-06 20:12:16'),(6,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:20','2017-04-06 20:12:20'),(7,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:24','2017-04-06 20:12:24'),(8,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:29','2017-04-06 20:12:29'),(9,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:33','2017-04-06 20:12:33'),(10,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:38','2017-04-06 20:12:38'),(11,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:39','2017-04-06 20:12:39'),(12,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:42','2017-04-06 20:12:42'),(13,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:44','2017-04-06 20:12:44'),(14,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:48','2017-04-06 20:12:48'),(15,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:49','2017-04-06 20:12:49'),(16,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:52','2017-04-06 20:12:52'),(17,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:53','2017-04-06 20:12:53'),(18,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:57','2017-04-06 20:12:57'),(19,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:58','2017-04-06 20:12:58'),(20,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:13:02','2017-04-06 20:13:02'),(21,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:13:03','2017-04-06 20:13:03'),(22,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:13:07','2017-04-06 20:13:07'),(23,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:13:08','2017-04-06 20:13:08'),(24,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:13:09','2017-04-06 20:13:09'),(25,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:13:11','2017-04-06 20:13:11'),(26,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:13:12','2017-04-06 20:13:12'),(27,'sonu','sonupajai22@gmail.com','9898989898','code_standards.txt','text/plain',18285,'2017-04-07 06:35:32','2017-04-07 06:35:32'),(28,'sonu','sonupajai22@gmail.com','9898989898','code_standards.txt','text/plain',18285,'2017-04-07 06:36:04','2017-04-07 06:36:04'),(29,'atul','atul.saini@amuratech.com','8983477765','imp.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',16081,'2017-04-07 06:47:14','2017-04-07 06:47:14'),(30,'sonu','sonupajai22@gmail.com','9898989898','code_standards.txt','text/plain',18285,'2017-04-07 09:28:40','2017-04-07 09:28:40'),(31,'sonu','sonupajai22@gmail.com','9898989898','code_standards.txt','text/plain',18285,'2017-04-07 10:06:52','2017-04-07 10:06:52'),(32,'Piyush Sethi','piyush@amuratech.com','8878697280','Untitled_Document_1.txt','text/plain',13,'2017-04-25 12:59:09','2017-04-25 12:59:09'),(33,'Rathin saha','ratshamale@yahoo.com.au','0420809710','CV_.pdf','application/pdf',22427,'2017-05-26 04:47:23','2017-05-26 04:47:23');
/*!40000 ALTER TABLE `careers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `category_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_group_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinic_registrations`
--

DROP TABLE IF EXISTS `clinic_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinic_registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobileno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pincode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `school_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `standard` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `food_preference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_mobileno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_relationship` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adult_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_terms` tinyint(1) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `order_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tracking_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `back_ref_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `failure_message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `data_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_size` int(11) DEFAULT NULL,
  `youth_kit_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount_pay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinic_registrations`
--

LOCK TABLES `clinic_registrations` WRITE;
/*!40000 ALTER TABLE `clinic_registrations` DISABLE KEYS */;
INSERT INTO `clinic_registrations` VALUES (1,'sunil',NULL,NULL,'Bibwewadi-Katraj','pune','411046','maharashtra','Male','2017-02-01 00:00:00','sdds','sdf','Veg','sdf','8551018446','sunil@amuratech.com','ssd','l',NULL,673358546,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-02-20 12:03:25','2017-02-20 12:03:25','ccavenu.png','image/png',176491,'l','200000'),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-03 06:35:40','2017-03-03 06:35:40',NULL,NULL,NULL,NULL,NULL),(3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-03 10:16:05','2017-03-03 10:16:05',NULL,NULL,NULL,NULL,NULL),(4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-03 10:16:15','2017-03-03 10:16:15',NULL,NULL,NULL,NULL,NULL),(5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-03 10:16:18','2017-03-03 10:16:18',NULL,NULL,NULL,NULL,NULL),(6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-03 10:16:18','2017-03-03 10:16:18',NULL,NULL,NULL,NULL,NULL),(7,'test',NULL,NULL,'test','test','411035','test','Male','2017-03-07 00:00:00','test','','Non-Veg','test','1234567890','test@gmail.com','test',NULL,NULL,789333095,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-07 07:00:29','2017-03-07 07:00:29','index.png','image/png',3142,'s','200000');
/*!40000 ALTER TABLE `clinic_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `blog_id` int(11) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mailer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campaign` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profession` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_area` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `organization` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enquiry_about` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entity_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entity_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `query` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hidden_data` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpfl_registration_news`
--

DROP TABLE IF EXISTS `cpfl_registration_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpfl_registration_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `school_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `standard` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_size` int(11) DEFAULT NULL,
  `youth_kit_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adult_kit_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount_pay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tracking_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_ref_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `failure_message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpfl_registration_news`
--

LOCK TABLES `cpfl_registration_news` WRITE;
/*!40000 ALTER TABLE `cpfl_registration_news` DISABLE KEYS */;
INSERT INTO `cpfl_registration_news` VALUES (1,'sunil','Male','2017-02-18 00:00:00','s','s','8551018446','sunil@amuratech.com','Maple.jpg','image/jpeg',98761,'s','m','200000','2017-02-17 06:37:19','2017-02-17 06:37:19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'sunil','Male','2017-02-21 00:00:00','ss','s','8551018446','sunil@amuratech.com','2_thumbnail_16Feb17.jpg','image/jpeg',42104,'s','l','200000','2017-02-17 07:02:10','2017-02-17 07:02:10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'sunil','Male','2017-02-23 00:00:00','s','s','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 07:36:53','2017-02-17 07:36:53',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'sunil','Male','2017-02-09 00:00:00','s','s','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 09:18:29','2017-02-17 09:18:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'sunil','Male','2017-02-21 00:00:00','ss','ss','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 09:27:44','2017-02-17 09:27:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'sunil','Male','2017-02-22 00:00:00','s','sd','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 09:43:05','2017-02-17 09:43:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'sunil','Male','2017-02-22 00:00:00','s','s','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','l','200000','2017-02-17 11:08:07','2017-02-17 11:08:07',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'sunil','Male','2017-02-16 00:00:00','ss','ss','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 11:31:00','2017-02-17 11:31:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'sunil','Male','2017-02-10 00:00:00','ss','sss','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 12:24:00','2017-02-17 12:24:00','398292615',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,'sunil','Male','2017-02-22 00:00:00','ss','dd','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 12:30:40','2017-02-17 12:37:44','829586691','ABC','12345678',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'sunil','Male','2017-02-14 00:00:00','ss','s','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 12:51:24','2017-02-17 12:51:52','665328327','Success','306003049565','1487335557410','','s','Pune','Maharashtrass','411025','India'),(12,'ss','Male','2017-02-09 00:00:00','ss','ss','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 12:54:25','2017-02-17 12:54:50','61074865','Aborted','306003049575','null','','','','','','India'),(13,'sunil','Male','2017-02-14 00:00:00','sss','ss','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 13:28:30','2017-02-17 13:29:07','394734602','Success','306003049629','1487337793855','','ss','ss','ss','411024','India');
/*!40000 ALTER TABLE `cpfl_registration_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpfl_registrations`
--

DROP TABLE IF EXISTS `cpfl_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpfl_registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `school_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_size` int(11) DEFAULT NULL,
  `kit_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `former_participant` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `former_participant_event` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `football_experience` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_problem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mediacal_app_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_year_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_dob` datetime DEFAULT NULL,
  `medical_gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_physician_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_contact_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_allergies` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_reaction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_reaaction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_hospitalization_occurred` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_allergy_medication` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_asthma` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_tetanus_injection` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_terms` tinyint(1) DEFAULT NULL,
  `medical_parent_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_print_nameapp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_relation_toapp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_father_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_father_mobileno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_father_emailid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_mother_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_mother_mobileno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_mother_emailid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_father_volunteer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_father_kit_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_mother_volunteer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_mother_kit_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emergency_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emergency_mobileno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emergency_relation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fee_structure` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `terms_condition` tinyint(1) DEFAULT NULL,
  `paren_consent` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpfl_registrations`
--

LOCK TABLES `cpfl_registrations` WRITE;
/*!40000 ALTER TABLE `cpfl_registrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpfl_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cryptos`
--

DROP TABLE IF EXISTS `cryptos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cryptos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cryptos`
--

LOCK TABLES `cryptos` WRITE;
/*!40000 ALTER TABLE `cryptos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cryptos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `published_date` date DEFAULT NULL,
  `publication` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disclaimer` text COLLATE utf8_unicode_ci,
  `info` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fcbescola_trials`
--

DROP TABLE IF EXISTS `fcbescola_trials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fcbescola_trials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `school_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `traning_venue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hear_about` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fcbescola_trials`
--

LOCK TABLES `fcbescola_trials` WRITE;
/*!40000 ALTER TABLE `fcbescola_trials` DISABLE KEYS */;
/*!40000 ALTER TABLE `fcbescola_trials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_date` datetime DEFAULT NULL,
  `publication` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `featured` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (1,'Programa Elite - Gurgaon','','2016-01-21 10:38:00',NULL,'programa-elite-gurgaon',NULL,1,NULL,'2017-02-21 11:43:24','2017-05-23 09:57:30');
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metacontents`
--

DROP TABLE IF EXISTS `metacontents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metacontents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_titles` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_content` text COLLATE utf8_unicode_ci,
  `meta_text` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metacontents`
--

LOCK TABLES `metacontents` WRITE;
/*!40000 ALTER TABLE `metacontents` DISABLE KEYS */;
/*!40000 ALTER TABLE `metacontents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `published_date` date DEFAULT NULL,
  `publication` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disclaimer` text COLLATE utf8_unicode_ci,
  `info` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `doc_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doc_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doc_file_size` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (3,'Five Goans To Live Barca Dream','<p>Timesofindia.com</p>',NULL,NULL,'five-goans-to-live-barca-dream','',NULL,'',1,NULL,'2017-04-21 11:30:03','2017-04-21 11:30:03','Untitled.jpg','image/jpeg',175569),(4,'Footie fever in Pune','<p>FCBEscola Camp in Pune.</p>','2012-11-14',NULL,'footie-fever-in-pune','http://conscientfootball.in/pdf/Footie-fever-in-the-city-Pune_Mid-Day.pdf',NULL,'',1,NULL,'2017-05-19 12:27:22','2017-05-19 12:32:35','Footie-fever-in-the-city-Pune_Mid-Day.pdf','application/pdf',489659),(5,'Conscient Football & FC Barcelona Partnership','','2011-10-20',NULL,'conscient-football-fc-barcelona-partnership','',NULL,'Consolidated coverage of Conscient Football and FC Barcelona partnership.',1,NULL,'2017-05-19 13:38:33','2017-05-19 13:38:33','Consolidated-coverage-of-CF-FCB-partnership.pdf','application/pdf',2537748),(6,'FCBarcelona sets out to develop India’s Lionel Messi','','2011-10-19',NULL,'fcbarcelona-sets-out-to-develop-india-s-lionel-messi','',NULL,'',1,NULL,'2017-05-19 13:43:21','2017-05-19 13:43:21','Press-Release-with-photo_updated.pdf','application/pdf',732413),(7,'Play the Barcelona way','','2012-04-14',NULL,'play-the-barcelona-way','Times News Network',NULL,'',1,NULL,'2017-05-19 19:11:57','2017-05-19 19:13:48','Play_the_Barcelona_way.pdf','application/pdf',177302),(9,'Barca touch to Bangalore Football','','2012-05-20',NULL,'barca-touch-to-bangalore-football','Deccan Herald',NULL,'',1,NULL,'2017-05-19 19:19:30','2017-05-19 19:38:14','Download_File.compressed.pdf','application/pdf',417073),(10,'Barcelona aware of it\'s responsibility','',NULL,NULL,'barcelona-aware-of-it-s-responsibility','',NULL,'',1,NULL,'2017-05-19 19:28:52','2017-05-19 19:28:52','Article_Window.pdf','application/pdf',137469),(12,'Qatar Airways FCBEscola Football Fiesta in the Asian Age!','','2017-05-21',NULL,'qatar-airways-fcbescola-football-fiesta-in-the-asian-age','Asian Age',NULL,'',1,NULL,'2017-05-23 07:46:28','2017-05-23 07:46:28','11113840_368326000039248_5212229329907703849_n.jpg','image/jpeg',40544),(13,'FCBEscola students selected for nationals','','2016-01-16',NULL,'fcbescola-students-selected-for-nationals','',NULL,'',1,NULL,'2017-05-23 07:49:38','2017-05-23 07:50:02','av.jpg','image/jpeg',141795),(14,'FCBEscola Camp Dehradun featured in The Himachal Times','','2015-06-25',NULL,'fcbescola-camp-dehradun-featured-in-the-himachal-times','Himachal Times',NULL,'',1,NULL,'2017-05-23 08:01:08','2017-05-23 08:01:08','11148456_399951563543358_7967207614143276923_o.jpg','image/jpeg',524043),(15,'FCBEscola Camp Mumbai in the news!','','2015-06-04',NULL,'fcbescola-camp-mumbai-in-the-news','Afternoon DC - Mumbai',NULL,'',1,NULL,'2017-05-23 08:05:04','2017-05-23 08:05:04','17678_374497249422123_2570185159677895271_n.jpg','image/jpeg',116198),(16,'FCBEscola\'s new training center','','2015-05-10',NULL,'fcbescola-s-new-training-center','',NULL,'',1,NULL,'2017-05-23 08:07:39','2017-05-23 08:07:39','11136634_365090603696121_952971049378834065_n.jpg','image/jpeg',59675),(17,'Media Heading','',NULL,NULL,'media-heading','',NULL,'',0,NULL,'2017-05-24 12:32:01','2017-05-24 12:32:01','18_BeautyPlus_20161030182344_save.jpg','image/jpeg',642243),(18,'dsdsd','',NULL,NULL,'dsdsd','',NULL,'',0,NULL,'2017-05-24 12:33:17','2017-05-24 12:33:17','14_code_standards.txt','text/plain',18285);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tagline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `overview` text COLLATE utf8_unicode_ci,
  `metakeywords` text COLLATE utf8_unicode_ci,
  `metadescription` text COLLATE utf8_unicode_ci,
  `metatitle` text COLLATE utf8_unicode_ci,
  `show_in_header` tinyint(1) DEFAULT NULL,
  `show_on_site` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `press_releases`
--

DROP TABLE IF EXISTS `press_releases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `press_releases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `short_description` text COLLATE utf8_unicode_ci,
  `featured` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `press_releases`
--

LOCK TABLES `press_releases` WRITE;
/*!40000 ALTER TABLE `press_releases` DISABLE KEYS */;
INSERT INTO `press_releases` VALUES (6,'The Conscient Premier Football League to close with an action packed closing ceremony','the-conscient-premier-football-league-to-close-with-an-action-packed-closing-ceremony','','',1,1,'2017-05-18 10:01:25','2017-05-18 10:01:40','http://timesofindia.indiatimes.com/sports/football/top-stories/The-Conscient-Premier-Football-League-to-close-with-an-action-packed-closing-ceremony/articleshow/7602446.cms?referral=PM','2011-03-01'),(7,'The Conscient Premier Football League closes its 2010-2011 season','the-conscient-premier-football-league-closes-its-2010-2011-season','','',1,1,'2017-05-18 10:04:14','2017-05-18 10:04:14','http://timesofindia.indiatimes.com/sports/football/top-stories/The-Conscient-Premier-Football-League-closes-its-2010-2011-season/articleshow/7619689.cms?referral=PM','2011-03-03'),(8,'Conscient Football to provide football coaching','conscient-football-to-provide-football-coaching','','',1,1,'2017-05-18 10:05:06','2017-05-18 10:05:06','http://timesofindia.indiatimes.com/sports/football/top-stories/Conscient-Football-to-provide-football-coaching/articleshow/7665498.cms?referral=PM','2011-03-09'),(11,'FC Barcelona To Start Football Camp In Delhi','fc-barcelona-to-start-football-camp-in-delhi','','',1,1,'2017-05-18 10:07:53','2017-05-18 10:07:53','http://www.goal.com/en-india/news/136/india/2011/12/10/2796905/fc-barcelona-to-start-football-camp-in-delhi','2011-12-10'),(12,'FC Barcelona-Conscient  soccer training camps kick off 30 Dec','fc-barcelona-conscient-soccer-training-camps-kick-off-30-dec','','',1,1,'2017-05-18 10:08:54','2017-05-18 10:09:02','http://www.sportzpower.com/?q=content/subscribe-sportzpower','2011-12-12'),(13,'Interview with former FC Barcelona player and FCB Escola coach Josep Moratalla','interview-with-former-fc-barcelona-player-and-fcb-escola-coach-josep-moratalla','','',1,1,'2017-05-18 10:10:24','2017-05-18 10:10:24','https://www.sportskeeda.com/football/interview-with-former-fc-barcelona-player-and-fcb-escola-coach-josep-moratalla','2012-01-07'),(14,'FCBEscola wants to inspire future champs out of kids in India','fcbescola-wants-to-inspire-future-champs-out-of-kids-in-india','','',1,1,'2017-05-18 10:11:46','2017-05-18 10:11:54','http://timesofindia.indiatimes.com/sports/football/top-stories/FCBEscola-wants-to-inspire-future-champs-out-of-kids-in-India/articleshow/11404675.cms','2012-01-07'),(15,'FC Barcelona Camp comes to Bangalore','fc-barcelona-camp-comes-to-bangalore','','',1,1,'2017-05-18 10:12:55','2017-05-18 10:12:55','https://www.sportskeeda.com/general-sports/fc-barcelona-camp-comes-to-bangalore','2012-04-14'),(22,'FC Barcelona to open  football school in India','fc-barcelona-to-open-football-school-in-india','','',1,1,'2017-05-18 10:19:59','2017-05-18 10:19:59','https://in.news.yahoo.com/fc-barcelona-open-football-school-india-110648122.html','2012-05-16'),(23,'FC Barcelona to open  football school in India','fc-barcelona-to-open-football-school-in-india--2','','',1,1,'2017-05-18 10:20:51','2017-05-18 10:20:51','http://www.newstrackindia.com/newsdetails/2012/05/16/233--FC-Barcelona-to-open-football-school-in-India-.html','2012-05-16'),(24,'FC Barcelona to be the FIRST CLUB to set up Official Football School','fc-barcelona-to-be-the-first-club-to-set-up-official-football-school','','',1,1,'2017-05-18 10:21:29','2017-05-18 10:21:29','https://www.sportskeeda.com/football/fc-barcelona-to-be-the-first-club-to-set-up-official-football-school-in-india','2012-05-16'),(25,'Barcelona to open football school','barcelona-to-open-football-school','','',1,1,'2017-05-18 10:23:16','2017-05-18 10:23:16','http://www.deccanherald.com/content/250018/barcelona-open-football-school.html','2012-05-16'),(26,'Barcelona to open  football school in Delhi','barcelona-to-open-football-school-in-delhi','','',1,1,'2017-05-18 10:23:53','2017-05-18 10:23:53','http://www.dnaindia.com/sport/report-barcelona-to-open-football-school-in-india-1689643','2012-05-16'),(27,'Barcelona to open  football school in Delhi','barcelona-to-open-football-school-in-delhi--2','','',1,1,'2017-05-18 10:24:43','2017-05-20 11:32:22','http://www.thehindu.com/sport/football/barcelona-to-open-football-school-in-delhi/article3426235.ece','2012-05-16'),(28,'FC Barcelona set up  football school in India','fc-barcelona-set-up-football-school-in-india','','',1,1,'2017-05-18 10:25:23','2017-05-18 10:25:23','http://www.business-standard.com/article/pti-stories/fc-barcelona-set-up-football-school-in-india-112051600755_1.html','2012-05-16'),(31,'FC Barcelona teams  up with Conscient for  football school','fc-barcelona-teams-up-with-conscient-for-football-school--3','','',1,1,'2017-05-18 10:26:06','2017-05-18 10:26:06','http://www.thehindubusinessline.com/companies/fc-barcelona-teams-up-with-conscient-for-football-school/article3425341.ece','2012-05-16'),(32,'FC Barcelona sets up football school in India','fc-barcelona-sets-up-football-school-in-india','','',1,1,'2017-05-18 10:26:40','2017-05-18 10:26:46','http://www.livemint.com/Consumer/Nz5ZxC3C0ish9KndP2ng4J/FC-Barcelona-sets-up-football-school-in-India.html','2012-05-16'),(33,'FCBEscola to produce  talented footballers in India','fcbescola-to-produce-talented-footballers-in-india','','',1,1,'2017-05-18 10:27:53','2017-05-18 10:27:53','http://www.thesundayindian.com/en/story/fcbescola-to-produce-talented-footballers-in-india/4/35006/','2012-05-17'),(34,'FC Barcelona to launch soccer school in India','fc-barcelona-to-launch-soccer-school-in-india','','',1,1,'2017-05-18 10:28:21','2017-05-18 10:28:21','http://www.financialexpress.com/archive/fc-barcelona-to-launch-soccer-school-in-india/950396/','2012-05-17'),(35,'FC Barcelona\'s official football school','fc-barcelona-s-official-football-school','','',1,1,'2017-05-18 10:29:15','2017-05-18 10:29:15','http://economictimes.indiatimes.com/et-cetera/fc-barcelonas-official-football-school-fcbescola-to-set-up-training-club-in-gurgaon/articleshow/13180315.cms','2012-05-17'),(37,'FC Barcelona set  to launch camps  in Jaipur, Pune...','fc-barcelona-set-to-launch-camps-in-jaipur-pune','','',1,1,'2017-05-18 10:30:35','2017-05-18 10:30:35','https://www.sportskeeda.com/football/fc-barcelona-set-to-launch-camps-in-jaipur-pune-and-varodara','2012-10-19'),(38,'FC Barcelona to  conduct camp  in city','fc-barcelona-to-conduct-camp-in-city','','',1,1,'2017-05-18 10:31:06','2017-05-18 10:31:06','http://archive.indianexpress.com/news/fc-barcelona-to-conduct-camp-in-city/1019000/','2012-10-19'),(39,'Barcelona to train  Vadodara\'s wannabe  Messis','barcelona-to-train-vadodara-s-wannabe-messis','','',1,1,'2017-05-18 10:32:22','2017-05-18 10:32:22','http://articles.timesofindia.indiatimes.com/2012-10-19/vadodara/34583358_1_conscient-football-fcbescola-young-footballers','2012-10-19'),(40,'\"Physique is not  important to become a top footballer\"','physique-is-not-important-to-become-a-top-footballer','','',1,1,'2017-05-18 10:33:05','2017-05-18 10:33:20','http://www.goal.com/en-india/news/136/india/2012/10/25/3476751/physique-is-not-important-to-become-a-top-footballer','2012-10-25'),(41,'Footie fever in the city','footie-fever-in-the-city','FCBEscola Camp in Pune.','',0,0,'2017-05-18 10:34:06','2017-05-19 12:26:40','http://conscientfootball.in/pdf/Footie-fever-in-the-city-Pune_Mid-Day.pdf','2012-11-14'),(42,'Aspiring footballers  yearn for more at  Barcelona camp','aspiring-footballers-yearn-for-more-at-barcelona-camp','','',1,1,'2017-05-18 10:34:49','2017-05-18 10:34:49','http://timesofindia.indiatimes.com/city/vadodara/Aspiring-footballers-yearn-for-more-at-Barcelona-camp/articleshow/17287038.cms?referral=PM','2012-11-20'),(44,'FC Barcelona to start  FCBEscola India Clinics from December onwards','fc-barcelona-to-start-fcbescola-india-clinics-from-december-onwards','','',1,1,'2017-05-18 10:36:25','2017-05-18 10:36:25','https://www.sportskeeda.com/football/fc-barcelona-to-start-fcbescola-india-clinics-from-december-onwards','2012-11-16'),(45,'FC Barcelona to start FCBEscola India Clinics from December','fc-barcelona-to-start-fcbescola-india-clinics-from-december','','',1,1,'2017-05-18 10:37:00','2017-05-18 10:37:00','http://economictimes.indiatimes.com/et-cetera/fc-barcelona-to-start-fcbescola-india-clinics-from-december/articleshow/17243726.cms?intenttarget=no','2012-11-16'),(46,'FC Barcelona Clinics  to scout for talent','fc-barcelona-clinics-to-scout-for-talent','','',1,1,'2017-05-18 10:37:42','2017-05-18 10:37:42','http://www.thehindubusinessline.com/companies/fc-barcelona-clinics-to-scout-for-talent/article4102209.ece','2016-11-16'),(47,'Barcelona to start  FCBEscola clinics in India from December','barcelona-to-start-fcbescola-clinics-in-india-from-december','','',1,1,'2017-05-18 10:38:36','2017-05-18 10:38:49','http://www.goal.com/en-india/news/136/india/2012/11/21/3544235/barcelona-to-start-fcbescola-clinics-in-india-from-december?source=breakingnews','2012-11-21'),(48,'Five Goa boys selected for Barcelona tourney','five-goa-boys-selected-for-barcelona-tourney','','',1,1,'2017-05-18 10:40:33','2017-05-18 10:40:33','http://zeenews.india.com/sports/football/five-goa-boys-selected-for-barcelona-tournament_756839.html','2013-02-21'),(49,'Five Goa boys selected for Barcelona tourney','five-goa-boys-selected-for-barcelona-tourney--2','','',1,1,'2017-05-18 10:41:16','2017-05-18 10:41:16','https://sports.ndtv.com/football/five-goa-boys-selected-for-barcelona-tournament-1540487','2013-02-21'),(50,'Five Goa boys selected  for Barcelona tourney','five-goa-boys-selected-for-barcelona-tourney--3','','',1,1,'2017-05-18 10:41:57','2017-05-18 10:41:57','http://www.business-standard.com/article/pti-stories/five-goa-boys-selected-for-barcelona-tournament-113022100799_1.html','2013-02-21'),(51,'Five Goans to live Barca dream','five-goans-to-live-barca-dream','','',1,1,'2017-05-18 10:43:01','2017-05-18 10:43:01','http://timesofindia.indiatimes.com/city/goa/Five-Goans-to-live-Barca-dream/articleshow/18622592.cms?referral=PM','2013-02-22'),(52,'HT Gurgaon - Teacher\'s Day','ht-gurgaon-teacher-s-day','','',1,1,'2017-05-23 07:58:03','2017-05-23 07:58:03','https://business.facebook.com/HTGurgaon/photos/a.327813614015087.1073741828.319028678226914/742187875910990/?type=3&permPage=1','2015-09-05'),(53,'Test Online Coverage 1','test-online-coverage-1','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',1,1,'2017-05-23 09:20:25','2017-05-23 09:26:04','https://business.facebook.com/test-online-coverage-1','2017-05-23');
/*!40000 ALTER TABLE `press_releases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selldocrmid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tagline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `started_at` date DEFAULT NULL,
  `completion_date` date DEFAULT NULL,
  `project_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_selling_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `availability` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clients` text COLLATE utf8_unicode_ci,
  `towers` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `area` int(11) DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `map_description` text COLLATE utf8_unicode_ci,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `neighbourhood` text COLLATE utf8_unicode_ci,
  `tags` text COLLATE utf8_unicode_ci,
  `meta_title` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `show_order` int(11) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `show_on_website` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `youth_kit_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount_pay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `redirects`
--

DROP TABLE IF EXISTS `redirects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `redirects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `old_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `redirects`
--

LOCK TABLES `redirects` WRITE;
/*!40000 ALTER TABLE `redirects` DISABLE KEYS */;
/*!40000 ALTER TABLE `redirects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumes`
--

DROP TABLE IF EXISTS `resumes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resumes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_of_interest` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume_file_size` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumes`
--

LOCK TABLES `resumes` WRITE;
/*!40000 ALTER TABLE `resumes` DISABLE KEYS */;
/*!40000 ALTER TABLE `resumes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20130213093505'),('20130213114756'),('20130213114766'),('20130213114768'),('20130220121323'),('20130226073804'),('20131014111337'),('20131014144549'),('20131019092301'),('20150603061620'),('20150603061825'),('20150603061827'),('20150828073507'),('20150828073703'),('20150907055235'),('20160225064450'),('20170214103132'),('20170214111645'),('20170215070649'),('20170215135619'),('20170216095958'),('20170216120142'),('20170217100214'),('20170217135646'),('20170218073629'),('20170220114111'),('20170220114510'),('20170221093558'),('20170221094102'),('20170221103303'),('20170330140158'),('20170331120702'),('20170404072901'),('20170404113223'),('20170404132002'),('20170518091652');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `school_registrations`
--

DROP TABLE IF EXISTS `school_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_of_participation` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school_registrations`
--

LOCK TABLES `school_registrations` WRITE;
/*!40000 ALTER TABLE `school_registrations` DISABLE KEYS */;
INSERT INTO `school_registrations` VALUES (1,'MP','SSS','sonu@amuratech.com','3434343434',1,'2017-04-04 12:12:14','2017-04-04 12:12:14'),(2,'test school','delhi','test@amuratech.com','9876543210',1,'2017-04-06 20:22:15','2017-04-06 20:22:15'),(3,'atul','baner','atul.saini@amuratech.com','8984566549',3,'2017-04-07 07:19:12','2017-04-07 07:19:12'),(4,'fcb','bavdhan','atul.saini+1@amuratech.com','78767776767',1,'2017-04-07 07:19:49','2017-04-07 07:19:49'),(5,'atul','atul','atul.saini+camps@amuratech.com','8983477785',1,'2017-04-07 07:32:48','2017-04-07 07:32:48'),(6,'MP','SSS','sonu@amuratech.com','1212121212',1,'2017-04-07 09:40:04','2017-04-07 09:40:04'),(7,'MP','SSS','sonu@amuratech.com','1212121212',1,'2017-04-07 09:45:09','2017-04-07 09:45:09'),(8,'MP','SSS','sonu@amuratech.com','1212121212',1,'2017-04-07 09:45:40','2017-04-07 09:45:40'),(9,'MP','SSS','sonu@amuratech.com','1212121212',1,'2017-04-07 09:46:38','2017-04-07 09:46:38'),(10,'MP','test','sonu@amuratech.com','09898989898',1,'2017-04-20 09:54:20','2017-04-20 09:54:20'),(11,'','','','',NULL,'2017-04-28 13:41:07','2017-04-28 13:41:07'),(12,'DY Patil','Sinhgad','piyush@amuratech.com','8878697280',6,'2017-04-28 13:58:29','2017-04-28 13:58:29'),(13,'Patil','Sinhgad','piyush@amuratech.com','8878697280',3,'2017-04-28 14:03:50','2017-04-28 14:03:50'),(14,'faris hasan','nc 24 canal colony okhla new delhi','qamar.hasan2@gmail.com','8588914577',9,'2017-05-13 06:55:07','2017-05-13 06:55:07');
/*!40000 ALTER TABLE `school_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_settings`
--

DROP TABLE IF EXISTS `site_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sitetitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_comments` tinyint(1) DEFAULT NULL,
  `xmlsitemap_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xmlsitemap_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xmlsitemap_file_size` int(11) DEFAULT NULL,
  `htmlsitemap_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `htmlsitemap_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `htmlsitemap_file_size` int(11) DEFAULT NULL,
  `googlemeta_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `googlemeta_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `googlemeta_file_size` int(11) DEFAULT NULL,
  `google_verification_code` text COLLATE utf8_unicode_ci,
  `google_analytics_code` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_settings`
--

LOCK TABLES `site_settings` WRITE;
/*!40000 ALTER TABLE `site_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_info` text COLLATE utf8_unicode_ci,
  `information` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `testimonial_text` text COLLATE utf8_unicode_ci,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_date` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiny_prints`
--

DROP TABLE IF EXISTS `tiny_prints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiny_prints` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiny_prints`
--

LOCK TABLES `tiny_prints` WRITE;
/*!40000 ALTER TABLE `tiny_prints` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiny_prints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiny_videos`
--

DROP TABLE IF EXISTS `tiny_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiny_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `original_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_file_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiny_videos`
--

LOCK TABLES `tiny_videos` WRITE;
/*!40000 ALTER TABLE `tiny_videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiny_videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tuples`
--

DROP TABLE IF EXISTS `tuples`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tuples` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `related_tuple_id` int(11) DEFAULT NULL,
  `related_tuple_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tuples`
--

LOCK TABLES `tuples` WRITE;
/*!40000 ALTER TABLE `tuples` DISABLE KEYS */;
/*!40000 ALTER TABLE `tuples` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin@amuratech.com','$2a$10$GA9/hWurLgOPrL5J2kVS6OS8HOHx0Wlwpwsj7jCT0svqGVGLmUSTq',NULL,NULL,'2017-05-23 06:06:55',52,'2017-05-25 13:37:14','2017-05-25 06:48:00','113.30.243.66','203.110.85.186','2017-02-17 11:28:57','2017-05-25 13:37:14'),(2,'moderator@amuratech.com','$2a$10$jg0vxi3mH/snGh7eten0k.KSgrHu4/JT3IEj5PbHBnpznOfbRiySG',NULL,NULL,NULL,20,'2017-05-25 13:09:48','2017-05-20 07:23:45','49.248.250.102','203.110.85.186','2017-05-17 09:28:45','2017-05-25 13:09:48');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `published_date` date DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` VALUES (1,'FCBCamp Pune November - News on DD Sahyadri','https://www.youtube.com/embed/eET612s9Afw',1,'2012-11-28','Conscient Football','2017-04-04 12:08:53','2017-04-21 12:43:01'),(2,'Headlines Today coverage of FCBEscola Delhi NCR','https://www.youtube.com/embed/9vdjpA-hzCQ',1,'2012-05-30','Conscient Football','2017-04-21 11:37:30','2017-05-19 13:39:35'),(3,'FCBESCOLA India team - NDTV24x7','https://www.youtube.com/embed/Q9oQGt57OtU',1,'2017-05-19','NDTV','2017-05-19 12:44:34','2017-05-19 12:58:22'),(4,'FCBEscola Football School India on Ten Sports','https://www.youtube.com/embed/6lRc0_xcrLM',1,'2015-09-18','Ten Sports','2017-05-19 12:53:45','2017-05-19 13:00:10'),(5,'FCBEscola India featured in the local news in Barcelona.','https://www.youtube.com/embed/m2SoGdjU4gc',1,'2015-05-20','Local News','2017-05-20 10:36:24','2017-05-20 11:31:33');
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-26  7:11:29
