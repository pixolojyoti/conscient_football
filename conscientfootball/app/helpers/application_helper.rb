module ApplicationHelper

  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  end

  def link_to_add_fields(name, f, association, field_partial=nil)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render_path = field_partial.nil? ? association.to_s.pluralize : field_partial
      render(render_path  + "/" + association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, h("add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")"))
  end

  def link_to_add_fields_with_callback(name, f, association, field_partial, callback)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render_path = field_partial.nil? ? association.to_s.pluralize : field_partial
      render(render_path  + "/" + association.to_s.singularize + "_fields", :f => builder)
    end
	test = escape_javascript(fields)
	return "<a href='#' onclick='add_fields_with_callback(this, \"tuples\", \"#{test}\", \"init_editor\"); return false;'>#{name}</a>".html_safe
  end

  def get_projects
   x= []
   p=Project.find :all, :select => "distinct(name)"
   x = p.collect{|x| x.name}
   return x
  end  

end
