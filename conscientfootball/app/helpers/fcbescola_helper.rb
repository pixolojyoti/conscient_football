module FcbescolaHelper

	def url_registeration
		'/fcbescola/trial-registration'
	end
	def url_privacy_policy
		'/fcbescola/privacy-policy'
	end
	def url_terms_condition
		'/fcbescola/terms-and-conditions'
	end
end
