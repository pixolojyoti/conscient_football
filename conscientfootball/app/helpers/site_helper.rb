module SiteHelper

	def url_index
	  "/"
	end

	def url_aboutus
	  "/about-us"
	end

	def url_ourprogram
		"/our-programs"
	end	

	def url_social_responsibility
		"/social-responsibility"
	end	
	def url_mgmt
		"/facility-management"
	end	

	def url_success_stories
		"/success-stories"
	end	
	def url_success_stories_details  b = ""
      if b.present?
         "/success-stories-details/#{b.slug}"
      else
         "/success-stories-details"
       end		
	end	
	def url_about_barca_clinic
		"/about-barca-clinic"
	end	

	def url_gallery
		"/gallery"
	end	

	def url_gallery_details b=nil
      if b.present?
         "/gallery-details/#{b.slug}"
      else
         "/gallery-details"
       end	
	end	

	def url_media
		"/media"
	end	

	def url_news
		"/media/news"
	end	

	def url_videos
		"/media/videos"
	end

	def url_press_release
		"/media/online-coverage"
	end

	def url_blog_list
		"/media/blogs"
	end

	def url_contactus
		"/contact-us"
	end	

	def url_careers
		"/join-us"
	end

	def url_training
		"/football-coaching"
	end	

	def url_annual_calendar
		"/annual-calendar"
	end	

	def url_barcelona_clinic
		"/football-camps"
	end	

	def url_camps
		"/football-clinic-and-camps"
	end	
	def url_summercamps
		"/football-summer-camps"
	end	


	def url_cpfl
		"/football-tournaments-and-leagues"
	end	

	def url_cpfl_form
		"/cpfl-form"
	end	

	def url_faq
		"/faq"
	end	


	def url_faq_overview
		"/faq-overview"
	end	

	def url_fcbse_venue
		"/fcbse-venue"
	end	

	def url_football_camps
		"/football-camps"
	end	


	def url_how_we_train
		"/how-we-train"
	end	

	def url_league_table
		"/league-table"
	end	

	def url_our_program_training
		"/our-program-training"
	end	


	def url_overview_fcbescola
		"/overview-fcbescola"
	end	

	def url_rules_regulations
		"/rules-regulations"
	end	


	def url_schedule
		"/schedule"
	end	

	def url_training_program
		"/training-program"
	end	


	def url_view_center
		"/view-center"
	end	


	def url_why_join
		"/why-join"
	end	

	def url_camp_schedule
		"/camp-schedule"
	end	

	def url_upcoming_events
		"/upcoming-events"
	end

	 def url_upcoming_events_details b = ""
      if b.present?
         "/upcoming_event_details/#{b.slug}"
      else
         "/upcoming_event_details"
       end
	end	
	def url_blog(blog)
		"/blog-detail/#{blog.slug}"
	end
	def url_terms_condition
		"/terms-and-conditions"
	end	

	def url_terms_condition_clinics
		"/terms-condition-clinics"
	end	
	def url_gallery_detail(y)
		if y==""
			"#"
		else
			"/gallery-details/#{y}"
		end
	end
	def get_gallery(y,cat)
		Gallery.where(:status=>true,:category_id=>cat.id).where(:published_year => y)
	end

end
