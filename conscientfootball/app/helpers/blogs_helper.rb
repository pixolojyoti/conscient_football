module BlogsHelper
  
  def url_blog b = ""
      if b.present?
         "/blog/#{b.slug}"
      else
         "/blog"
       end
  end
  
end
