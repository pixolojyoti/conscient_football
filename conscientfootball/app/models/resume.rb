class Resume < ActiveRecord::Base
	
	has_attached_file :resume,  :url => "/resumes/:id_:filename"
	 validates_attachment_presence :resume
	validates_attachment_content_type :resume,  :content_type =>  
	['text/plain', 'application/msword', 'application/vnd.ms-powerpoint','text/html','application/pdf', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.presentationml.presentation'] 

end
