class Testimonial < ActiveRecord::Base

	has_many :assets, :as => :attachable, :dependent => :destroy, :class_name => "Asset", :order => "asset_order ASC"
	has_one :main_image, :as => :attachable, :dependent => :destroy, :class_name => "Asset",:conditions => {:group_id => "main_image"}
	TESTIMONIAL_MULTIPLE_ASSETS.each do |type|
		self.class_eval <<-EOV
		 has_many :assets_#{type}, :as => :attachable, :dependent => :destroy, :class_name => "Asset", :conditions => {:group_id => "assets_#{type}"}, :order => "asset_order ASC"
		EOV
	end
end
