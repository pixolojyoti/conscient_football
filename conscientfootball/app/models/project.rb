class Project < ActiveRecord::Base
	has_many :tuples, :as => :related_tuple, :dependent => :destroy
	has_many :assets, :as => :attachable, :dependent => :destroy, :class_name => "Asset", :order => "asset_order ASC"   
	extend FriendlyId
	friendly_id :name, :use => :slugged
	accepts_nested_attributes_for :tuples, :allow_destroy => true
	has_one :thumbnail, :as => :attachable, :dependent => :destroy, :class_name => "Asset",:conditions => {:group_id => "thumbnail"}
         has_one :main_image, :as => :attachable, :dependent => :destroy, :class_name => "Asset",:conditions => {:group_id => "main_image"}
         has_one :logo, :as => :attachable, :dependent => :destroy, :class_name => "Asset",:conditions => {:group_id => "logo"}
         has_one :brochure, :as => :attachable, :dependent => :destroy, :class_name => "Asset",:conditions => {:group_id => "brochure"}
         has_one :featured_image, :as => :attachable, :dependent => :destroy, :class_name => "Asset",:conditions => {:group_id => "featured_image"}
	
	SELECT_NAME = {
					"city_id" => ["city","area","city_id"],
					"enquiry_city_id" => ["city","area","enquiry_area_id"],
					"area_id" => ["area","project_type"],
					"enquiry_area_id" => ["area","project_type","project_area_id"],
					"enquiry_select_area_id" => ["area" , "name", "project_name_id"],
					"project_name_id" => ["name","project_type","configuration_id"]
	}

	
	PROJECT_MULTIPLE_ASSETS.each do |type|
		self.class_eval <<-EOV
			has_many :assets_#{type}, :as => :attachable, :dependent => :destroy, :class_name => "Asset", :conditions => {:group_id => "assets_#{type}"}, :order => "asset_order ASC"
		EOV
	end

	validates_presence_of :name
	#default_scope where(:show_on_website => true)
	scope :residential, where(:project_category => "Residential")
	scope :commercial, where(:project_category => "Commercial")
	scope :upcoming, where(:project_status => "Upcoming")
	scope :ongoing, where(:project_status => "Ongoing")
	scope :completed, where(:project_status => "Completed")


end
