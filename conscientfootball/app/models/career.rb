class Career < ActiveRecord::Base
	 has_attached_file :resume, 
      :url  => "/resume/:basename.:extension",
      :path => ":rails_root/public/resume/:id_:basename.:extension"
       validates_attachment_presence :resume
	validates_attachment_content_type :resume,  :content_type =>  
	['text/plain', 'application/msword', 'application/vnd.ms-powerpoint','text/html','application/pdf', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.presentationml.presentation'] 

end
