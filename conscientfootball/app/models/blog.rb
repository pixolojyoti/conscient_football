class Blog < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, :use => :slugged
	validates_presence_of :title, :description
	has_many :comments
	has_one :main_image, :as => :attachable, :dependent => :destroy, :class_name => "Asset",:conditions => {:group_id => "main_image"}
	has_one :thumbnail, :as => :attachable, :dependent => :destroy, :class_name => "Asset",:conditions => {:group_id => "thumbnail"}
	 
	 
 BLOG_MULTIPLE_ASSETS.each do |type|
      self.class_eval <<-EOV
         has_many :assets_#{type}, :as => :attachable, :dependent => :destroy, :class_name => "Asset", :conditions => {:group_id => "assets_#{type}"}, :order => "asset_order ASC"
      EOV
   end
end
