class Tuple < ActiveRecord::Base
  belongs_to :related_tuple, :polymorphic => true
  
  validates_presence_of :key, :value
end
