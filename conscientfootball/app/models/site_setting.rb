class SiteSetting < ActiveRecord::Base
	#validates_presence_of :notification_email
	has_attached_file :xmlsitemap, :url => "/:basename.:extension" 
	has_attached_file :htmlsitemap, :url => "/:basename.:extension"
	has_attached_file :googlemeta, :url => "/:basename.:extension"
end
