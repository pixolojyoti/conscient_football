class Page < ActiveRecord::Base
	
	
  has_many :sub_pages
  belongs_to :parent, :class_name => "Page", :foreign_key => "parent_id"
  extend FriendlyId
  friendly_id :title, :use => :slugged 
  #safter_save :update_nested_status
  before_save :update_parent_id
  before_destroy :check_if_parent
  validates_presence_of :title, :overview
  

  scope :main_pages, {:conditions => "parent_id = ''"}
  
  
  
  
	has_many :assets, :as => :attachable, :dependent => :destroy, :class_name => "Asset", :order => "asset_order ASC"
	has_one :main_image, :as => :attachable, :dependent => :destroy, :class_name => "Asset",:conditions => {:group_id => "main_image"}
	PAGE_MULTIPLE_ASSETS.each do |type|
		self.class_eval <<-EOV
		 has_many :assets_#{type}, :as => :attachable, :dependent => :destroy, :class_name => "Asset", :conditions => {:group_id => "assets_#{type}"}, :order => "asset_order ASC"
		EOV
	end
  
  
  
  

  def main_page
    if(self.parent_id == nil || self.parent_id == "")
      true
    else 
      false
    end 
  end
 
  def sub_pages
    return Page.find(:all, :conditions => ["parent_id = ? ", self.id])
  end
  
  
  def validate
    obj = Page.all
    if((obj - [self]).any?{|x| x.title.to_url == self.title.to_url } )
      self.errors.add("title", "The Title is already taken.")
    end
  end
  
  def check_if_parent
    Page.destroy_all "parent_id = #{self.id}"
  end
  
  def self.by_default attr
    a = (Page.all.select{|x|  x.title.to_url == attr}).first
  end  

  def url
		if self.parent.present?
			self.parent.url + "/" + self.title.to_url
		else
			self.title.to_url
		end
	end
   def update_parent_id
    if self.parent_id ==0 || self.parent_id == nil 
      self.parent_id = self.id
    end
  end
 	protected
 # def update_nested_status
 	# 	self.sub_pages.each{|x| x.update_attributes(:status => self.status)}
  #end
  
end
