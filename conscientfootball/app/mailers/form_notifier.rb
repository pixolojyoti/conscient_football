class FormNotifier < ActionMailer::Base
  add_template_helper(ApplicationHelper)
  default :from => "\"conscientfootball\" <no-reply@conscientfootball.com>"
include ApplicationHelper

  def notify_contact (contact, subj="Contact Form")
    @contact = contact
  	if RAILS_ENV == "development"
  	  to_mail = "sonu@amuratech.com"
  	else
  	  to_mail = "vallary@amuratech.com"
  	end
    mail(:subject => subj, :to => to_mail)
  end

  def notify_reg_contact(reg, subj="Registration Contact Form Mumbai | Trial Registration")
    @reg = reg
    if RAILS_ENV == "development"
      to_mail = "qa@amuratech.com"
    else
      to_mail = "chaitany.thakare@conscientfootball.com, fcbescola.mumbai@conscientfootball.com"
    end
    mail(:subject => subj, :to => to_mail)
  end


  def notify_reg_end_user(reg, subj="Registration Contact Form Mumbai | Trial Registration")
    @reg = reg
     if RAILS_ENV == "development"
      to_mail = "bhushan.chavan@amuratech.com, qa@amuratech.com"
    else
      to_mail = "chaitany.thakare@conscientfootball.com, fcbescola.mumbai@conscientfootball.com"
    end
    mail(:subject => subj, :to => @reg.guardians_email )
  end

 def notify_reg_contact_delhi(reg, subj="Registration Contact Form Delhi | Trial Registration")
    @reg = reg
    if RAILS_ENV == "development"
      to_mail = "siddheshwar@amuratech.com, qa@amuratech.com"
    else
      to_mail = "chaitany.thakare@conscientfootball.com, fcbescola.mumbai@conscientfootball.com"
    end
    mail(:subject => subj, :to => to_mail)
  end


  def notify_reg_end_user_delhi(reg, subj="Registration Contact Form Delhi | Trial Registration")
    @reg = reg
     if RAILS_ENV == "development"
      to_mail = "siddheshwar@amuratech.com, qa@amuratech.com"
    else
      to_mail = "chaitany.thakare@conscientfootball.com, fcbescola.mumbai@conscientfootball.com"
    end
    mail(:subject => subj, :to => @reg.guardians_email )
  end


  def notify_reg_contact_juhu(reg, subj="Registration Contact Form | Juhu")
    @reg = reg
    if RAILS_ENV == "development"
      to_mail = "ashwin@amuratech.com"
    else
      to_mail = "fcbescola.mumbai@conscientfootball.com"
    end
    mail(:subject => subj, :to => to_mail)
  end


  def notify_reg_end_user_juhu(reg, subj="Registration Contact Form | Juhu")
    @reg = reg
    mail(:subject => subj, :to => @reg.guardians_email )
  end


  def notify_reg_contact_south_mumbai(reg, subj="Registration Contact Form | South Mumbai")
    @reg = reg
    if RAILS_ENV == "development"
      to_mail = "ashwin@amuratech.com"
    else
      to_mail = "fcbescola.mumbai@conscientfootball.com"
    end
    mail(:subject => subj, :to => to_mail)
  end


  def notify_reg_end_user_south_mumbai(reg, subj="Registration Contact Form | South Mumbai")
    @reg = reg
    mail(:subject => subj, :to => @reg.guardians_email )
  end


  def notify_reg_contact_matunga(reg, subj="Registration Contact Form | Matunga")
    @reg = reg
    if RAILS_ENV == "development"
      to_mail = "ashwin@amuratech.com"
    else
      to_mail = "fcbescola.mumbai@conscientfootball.com"
    end
    mail(:subject => subj, :to => to_mail)
  end


  def notify_reg_end_user_matunga(reg, subj="Registration Contact Form | Matunga")
    @reg = reg
    mail(:subject => subj, :to => @reg.guardians_email )
  end


  def notify_reg_contact_andheri(reg, subj="Registration Contact Form | Andheri")
    @reg = reg
    if RAILS_ENV == "development"
      to_mail = "ashwin@amuratech.com"
    else
      to_mail = "qa@amuratech.com, fcbescola.mumbai@conscientfootball.com"
    end
    mail(:subject => subj, :to => to_mail)
  end

  def notify_facility (contact, subj="Conscient Facility Management")
    @contact = contact
    if RAILS_ENV == "development"
      to_mail = "dhruv.arora@conscientfootball.com, ravi.sharma@conscientfootball.com"
    else
      to_mail = "dhruv.arora@conscientfootball.com, ravi.sharma@conscientfootball.com"
    end
    mail(:subject => subj, :to => to_mail)
  end

  def notify_end_user_facility(reg, subj="Conscient Facility Management")
    @reg = reg
    if RAILS_ENV == "development"
      to_mail = "dhruv.arora@conscientfootball.com, ravi.sharma@conscientfootball.com"
    else
      to_mail = "dhruv.arora@conscientfootball.com, ravi.sharma@conscientfootball.com"
    end
    mail(:subject => subj, :to => to_mail)
  end

  def notify_reg_end_user_andheri(reg, subj="Registration Contact Form | Andheri")
    @reg = reg
    mail(:subject => subj, :to => @reg.guardians_email )
  end





  def notify_end_user (contact, subj="Contact Form")
    @contact = contact
	  mail(:subject => subj, :to => @contact.email )
  end


  def notify_career_admin (career, subj="Contact Form")
      @career = career
      if @career.resume.url.present?
        attachments["#{@career.resume_file_name}"] = File.read("#{Rails.root}/public/resume/#{@career.id}_#{@career.resume_file_name}")
      end
      if RAILS_ENV == "development"
        to_mail = "sonu@amuratech.com"
      else
        to_mail = "hr@conscientfootball.com"
      end
      mail(:subject => subj, :to => to_mail)
  end
  def notify_career_end_user(career, subj="Registration Successful")
      @career = career
       mail(:subject => subj, :to => @career.email )
  end
  def notify_school_registration_end_user(school_registration, subj="School Registration Successful")
    @school_registration = school_registration
    mail(:subject => subj, :to => @school_registration.email )
  end
  def notify_school_registration_admin (school_registration, subj="School Registration Successful")
    @school_registration = school_registration
    if RAILS_ENV == "development"
      to_mail = "sonu@amuratech.com , aishwarya@amuratech.com"
    else
      to_mail = "vallary@amuratech.com"
    end
    mail(:subject => subj, :to => to_mail)
  end
  def notify_fcbescola_prereg (contact, subj="Contact Form")
    @contact = contact
    if RAILS_ENV == "development"
      to_mail = "sonu@amuratech.com"
    else
      to_mail = "vallary@amuratech.com"
    end
    mail(:subject => subj, :to => to_mail)
  end

  def notify_end_user_fcbescola_prereg (contact, subj="Contact Form")
    @contact = contact
    mail(:subject => subj, :to => @contact.email )
  end

  def notify_cpflreg (contact, subj="Contact Form")
    @contact = contact
    if RAILS_ENV == "development"
           to_mail = "sonu@amuratech.com"
    else
          to_mail = "vallary@amuratech.com,"
    end
    mail(:subject => subj, :to => to_mail)
  end

  def notify_end_user_cpfl (contact, subj="Contact Form")
        @contact = contact
        mail(:subject => subj, :to => @contact.email )
  end

end
