class ApplicationController < ActionController::Base
protect_from_forgery
layout :layout
helper_method :mobile_device? 
before_filter  :set_metatags, :set_cookies

def set_metatags
  @metakeywords=""
  @metatitles = "Conscient Football"   
  @metadescription = ""
  @metacontent = Metacontent.find_by_url(request.request_uri)

  @redirect = Redirect.find_by_old_uri(request.path)
  Rails.logger.info "=====#{@redirect}====="
  if(@metacontent.present?)
    if (@metacontent.meta_titles != nil and  @metacontent.meta_titles != "") 
      @metatitles  =@metacontent.meta_titles
    end
    
    if (@metacontent.meta_description != nil and  @metacontent.meta_description != "") 
      @metadescription  =@metacontent.meta_description
    end
    
    if (@metacontent.meta_keywords != nil and  @metacontent.meta_keywords != "") 
      @metakeywords  =@metacontent.meta_keywords
    end
  end
  
  if @redirect
    redirect_to @redirect.new_uri
  end
end
  def method_missing(args)
    redirect_to "/404",:status => 301
  end
def check_for_mobile
  if session[:mobile].present? and params[:mobile].blank?
    prepare_for_mobile  if session[:mobile] == true
  else
    prepare_for_mobile if mobile_device?
  end
end 


def prepare_for_mobile
  prepend_view_path Rails.root + 'app' + 'views_mobile'
end


def mobile_device?
  if params[:mobile].present? 
    if params[:mobile].to_s == "true"
      session[:mobile] = true
      return true
  
    elsif params[:mobile].to_s == "false"
      session[:mobile] = false
      return false
    end

  elsif request.user_agent.present?
    if(request.user_agent.downcase.include?("mobile") && (request.user_agent.downcase.include?("ipad")))
      session[:mobile] = true
     return truesrd
    else
      session[:mobile] = false
      return false
    end
  end


end  

private  
  def layout
    logger.info self       
    if is_a?(Devise::SessionsController)
    "devise"
    elsif is_a?(Devise::PasswordsController)
    "devise"
    elsif is_a?(Devise::UnlocksController)
    "devise"
    elsif is_a?(Devise::RegistrationsController)
    "devise"
    else        
    "devise"
    end       
  end

  def set_cookies
    tmp = params[:srd]
    if cookies[:srd].blank?
      if tmp.present?
        cookies[:srd] = tmp
      else
        cookies[:srd] = "website"
      end
    end
  end

end
