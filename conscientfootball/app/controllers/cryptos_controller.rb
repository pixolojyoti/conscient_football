class CryptosController < ApplicationController
  # GET /cryptos
  # GET /cryptos.xml
  def index
    @cryptos = Crypto.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @cryptos }
    end
  end

  # GET /cryptos/1
  # GET /cryptos/1.xml
  def show
    @crypto = Crypto.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @crypto }
    end
  end

  # GET /cryptos/new
  # GET /cryptos/new.xml
  def new
    @crypto = Crypto.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @crypto }
    end
  end

  # GET /cryptos/1/edit
  def edit
    @crypto = Crypto.find(params[:id])
  end

  # POST /cryptos
  # POST /cryptos.xml
  def create
    @crypto = Crypto.new(params[:crypto])

    respond_to do |format|
      if @crypto.save
        format.html { redirect_to(@crypto, :notice => 'Crypto was successfully created.') }
        format.xml  { render :xml => @crypto, :status => :created, :location => @crypto }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @crypto.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /cryptos/1
  # PUT /cryptos/1.xml
  def update
    @crypto = Crypto.find(params[:id])

    respond_to do |format|
      if @crypto.update_attributes(params[:crypto])
        format.html { redirect_to(@crypto, :notice => 'Crypto was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @crypto.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /cryptos/1
  # DELETE /cryptos/1.xml
  def destroy
    @crypto = Crypto.find(params[:id])
    @crypto.destroy

    respond_to do |format|
      format.html { redirect_to(cryptos_url) }
      format.xml  { head :ok }
    end
  end
end
