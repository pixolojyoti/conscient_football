class FacebookContactsController < ApplicationController
  # GET /facebook_contacts
  # GET /facebook_contacts.xml
   before_filter :authenticate_user!
   layout "admin"
  
  def index
    @facebook_contacts = FacebookContact.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @facebook_contacts }
    end
  end

  # GET /facebook_contacts/1
  # GET /facebook_contacts/1.xml
  def show
    @facebook_contact = FacebookContact.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @facebook_contact }
    end
  end

  # GET /facebook_contacts/new
  # GET /facebook_contacts/new.xml
  def new
    @facebook_contact = FacebookContact.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @facebook_contact }
    end
  end

  # GET /facebook_contacts/1/edit
  def edit
    @facebook_contact = FacebookContact.find(params[:id])
  end

  # POST /facebook_contacts
  # POST /facebook_contacts.xml
  def create
    @facebook_contact = FacebookContact.new(params[:facebook_contact])

    respond_to do |format|
      if @facebook_contact.save
        format.html { redirect_to(@facebook_contact, :notice => 'Facebook contact was successfully created.') }
        format.xml  { render :xml => @facebook_contact, :status => :created, :location => @facebook_contact }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @facebook_contact.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /facebook_contacts/1
  # PUT /facebook_contacts/1.xml
  def update
    @facebook_contact = FacebookContact.find(params[:id])

    respond_to do |format|
      if @facebook_contact.update_attributes(params[:facebook_contact])
        format.html { redirect_to(@facebook_contact, :notice => 'Facebook contact was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @facebook_contact.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /facebook_contacts/1
  # DELETE /facebook_contacts/1.xml
  def destroy
    @facebook_contact = FacebookContact.find(params[:id])
    @facebook_contact.destroy

    respond_to do |format|
      format.html { redirect_to(facebook_contacts_url) }
      format.xml  { head :ok }
    end
  end
end
