class FcbescolaMumbaiController < ApplicationController
layout "fcbescola_mumbai"
	
	def registeration
		render :layout => false
	end

	def fcbescola_juhu_center
		render :layout => false
	end

	def fcbescola_south_mumbai_center
		render :layout => false
	end

	def fcbescola_matunga_center
		render :layout => false
	end

	def fcbescola_andheri_center
		render :layout => false
	end

	def reg_juhu
		 @trialreg = TrialRegistration.new(params[:reg])
          if @trialreg.save
               FormNotifier.notify_reg_contact_juhu(@trialreg, "Reference Lead Details - Conscient Football | Juhu").deliver 
               FormNotifier.notify_reg_end_user_juhu(@trialreg, "Thank you for your interest in Conscient Football | Juhu").deliver
               render :json => {:status => "success", :mesg => "Thankyou", :contact => @trialreg}
          else
                render :json => {:status => "error", :error => @trialreg.errors.full_messages}
          end
	end

	def reg_south_mumbai
		 @trialreg = TrialRegistration.new(params[:reg])
          if @trialreg.save
               FormNotifier.notify_reg_contact_south_mumbai(@trialreg, "Reference Lead Details - Conscient Football | South Mumbai").deliver 
               FormNotifier.notify_reg_end_user_south_mumbai(@trialreg, "Thank you for your interest in Conscient Football | South Mumbai").deliver
               render :json => {:status => "success", :mesg => "Thankyou", :contact => @trialreg}
          else
                render :json => {:status => "error", :error => @trialreg.errors.full_messages}
          end
	end

	def reg_matunga
		 @trialreg = TrialRegistration.new(params[:reg])
          if @trialreg.save
               FormNotifier.notify_reg_contact_matunga(@trialreg, "Reference Lead Details - Conscient Football | Matunga").deliver 
               FormNotifier.notify_reg_end_user_matunga(@trialreg, "Thank you for your interest in Conscient Football | Matunga").deliver
               render :json => {:status => "success", :mesg => "Thankyou", :contact => @trialreg}
          else
                render :json => {:status => "error", :error => @trialreg.errors.full_messages}
          end
	end

	def reg_andheri
		 @trialreg = TrialRegistration.new(params[:reg])
          if @trialreg.save
               FormNotifier.notify_reg_contact_andheri(@trialreg, "Reference Lead Details - Conscient Football | Andheri").deliver 
               FormNotifier.notify_reg_end_user_andheri(@trialreg, "Thank you for your interest in Conscient Football | Andheri").deliver
               render :json => {:status => "success", :mesg => "Thankyou", :contact => @trialreg}
          else
                render :json => {:status => "error", :error => @trialreg.errors.full_messages}
          end
	end
	
end
