class ClinicRegistrationsController < ApplicationController
  # GET /clinic_registrations
  # GET /clinic_registrations.xml
  layout "admin"
  before_filter :authenticate_user!  
  def index
    @clinic_registrations = ClinicRegistration.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @clinic_registrations }
    end
  end

  # GET /clinic_registrations/1
  # GET /clinic_registrations/1.xml
  def show
    @clinic_registration = ClinicRegistration.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @clinic_registration }
    end
  end

  # GET /clinic_registrations/new
  # GET /clinic_registrations/new.xml
  def new
    @clinic_registration = ClinicRegistration.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @clinic_registration }
    end
  end

  # GET /clinic_registrations/1/edit
  def edit
    @clinic_registration = ClinicRegistration.find(params[:id])
  end

  # POST /clinic_registrations
  # POST /clinic_registrations.xml
  def create
    @clinic_registration = ClinicRegistration.new(params[:clinic_registration])

    respond_to do |format|
      if @clinic_registration.save
        format.html { redirect_to(@clinic_registration, :notice => 'Clinic registration was successfully created.') }
        format.xml  { render :xml => @clinic_registration, :status => :created, :location => @clinic_registration }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @clinic_registration.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /clinic_registrations/1
  # PUT /clinic_registrations/1.xml
  def update
    @clinic_registration = ClinicRegistration.find(params[:id])

    respond_to do |format|
      if @clinic_registration.update_attributes(params[:clinic_registration])
        format.html { redirect_to(@clinic_registration, :notice => 'Clinic registration was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @clinic_registration.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /clinic_registrations/1
  # DELETE /clinic_registrations/1.xml
  def destroy
    @clinic_registration = ClinicRegistration.find(params[:id])
    @clinic_registration.destroy

    respond_to do |format|
      format.html { redirect_to(clinic_registrations_url) }
      format.xml  { head :ok }
    end
  end
end
