class FcbescolaTrialsController < ApplicationController
  # GET /fcbescola_trials
  # GET /fcbescola_trials.xml
  layout "admin"
  before_filter :authenticate_user!
  def index
    @fcbescola_trials = FcbescolaTrial.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @fcbescola_trials }
    end
  end

  # GET /fcbescola_trials/1
  # GET /fcbescola_trials/1.xml
  def show
    @fcbescola_trial = FcbescolaTrial.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @fcbescola_trial }
    end
  end

  # GET /fcbescola_trials/new
  # GET /fcbescola_trials/new.xml
  def new
    @fcbescola_trial = FcbescolaTrial.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @fcbescola_trial }
    end
  end

  # GET /fcbescola_trials/1/edit
  def edit
    @fcbescola_trial = FcbescolaTrial.find(params[:id])
  end

  # POST /fcbescola_trials
  # POST /fcbescola_trials.xml
  def create
    @fcbescola_trial = FcbescolaTrial.new(params[:fcbescola_trial])

    respond_to do |format|
      if @fcbescola_trial.save
        format.html { redirect_to(@fcbescola_trial, :notice => 'Fcbescola trial was successfully created.') }
        format.xml  { render :xml => @fcbescola_trial, :status => :created, :location => @fcbescola_trial }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @fcbescola_trial.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /fcbescola_trials/1
  # PUT /fcbescola_trials/1.xml
  def update
    @fcbescola_trial = FcbescolaTrial.find(params[:id])

    respond_to do |format|
      if @fcbescola_trial.update_attributes(params[:fcbescola_trial])
        format.html { redirect_to(@fcbescola_trial, :notice => 'Fcbescola trial was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @fcbescola_trial.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /fcbescola_trials/1
  # DELETE /fcbescola_trials/1.xml
  def destroy
    @fcbescola_trial = FcbescolaTrial.find(params[:id])
    @fcbescola_trial.destroy

    respond_to do |format|
      format.html { redirect_to(fcbescola_trials_url) }
      format.xml  { head :ok }
    end
  end
end
