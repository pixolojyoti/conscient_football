class AssetsController < ApplicationController
  before_filter :authenticate_user!
 layout nil
  include ApplicationHelper
 
  def show
    @asset = Asset.find(params[:id])
    # do security check here
    #send_file asset.data.path, :type => asset.data_content_type
  end
  
  def destroy
    asset = Asset.find(params[:id])
    @asset_id = asset.id.to_s
    @asset_group_id = asset.group_id.to_s
    asset.destroy
  end
  
  def reorder
  	order_arr = params[:order_string].split(",") rescue []
  	@group_id = params[:group_id]
  	entity_id = params[:entity_id]
  	@class_name = params[:class_name]
  	@entity = eval("#{@class_name}.find(#{entity_id})")
  	order_arr.each_with_index do |asset_id, index|
  		a = Asset.find(asset_id)
		  logger.info "#{index} - #{a.id}"
  		a.asset_order = index.to_i
		  a.save
  	end
  end
  
  def show_update_asset
  	 @asset = Asset.find(params[:asset_id])
  end
  
  def update
  	@asset = Asset.find(params[:id])
    @asset.update_attributes(params[:asset])
  end
  
  def add
    newparams = coerce(params)
    @asset = Asset.new(newparams[:upload])
    if @asset.save
      flash[:notice] = "Successfully created upload."
      respond_to do |format|
	      format.json {render :json => { :result => 'success', :asset => "/assets/#{@asset.id}" } }
     end
    else
       respond_to do |format|
	      format.json {render :json => { :result => 'error', :message => errors_for(@asset) }}
       end
    end
  end
  
  def add_asset_to_class
    newparams = coerce(params)
    @entity = eval("#{newparams[:upload][:attachable_type]}.find(#{newparams[:upload][:attachable_id]})")
    method_name = "#{newparams[:upload][:group_id]}".gsub('assets_','')
    eval("@entity.#{method_name} = (newparams[:upload][:data])")
    if @entity.save
      flash[:notice] = "Successfully created upload."
      respond_to do |format|
	      format.text {render :text => "#{method_name.titleize} has been saved"}
	   end
    else
      respond_to do |format|
	      format.text {render :text => "#{method_name.titleize} could not be saved"}
	     end
    end
  end
   
   def add_assets_for
	  begin
			classname = params[:attachable_type]
			id=params[:attachable_id]
			asset_type=params[:group_id]
			singular=params[:singular]
			classname=classname.split(".").last	
			scope=Module.const_get classname
			object=scope.find_by_id id
		  newparams = coerce_flex_params(params)
		  @asset = Asset.new(newparams[:upload])
		  if singular == "true"
		  	asset = eval("object.#{asset_type}")
		  	if asset
		  		asset.destroy
	  		end	
		  	eval("object.#{asset_type} = @asset")
		  	object.save!
		  	asset = eval("object.#{asset_type}")
		  	xml = "<xml><status>success</status><path>#{asset.url}</path></xml>"
		  else
			  if @asset.save
			 	 	logger.info "Asset created..."
				  xml = "<xml><status>success</status><path>#{@asset.url}</path></xml>"
			  else
			    logger.info "Errors : #{@asset.errors}"
				  xml = "<xml><status>failure</status><error><![CDATA[#{@asset.errors} could not be saved]]></error></xml>"
			  end
			end
  	rescue Exception => e
  		xml = "<xml><status>failure</status><error><![CDATA[#{e.message}]]></error></xml>"
  	end
  	render :text => xml
end

  def add_logo
  	 # Delete old logo
    entity_type = params[:attachable_type]
    entity_type = Module.const_get entity_type.classify
    entity = entity_type.find_by_id(params[:attachable_id])
    group_id = params[:group_id]
    if entity and eval("entity.#{group_id}")
    	eval("entity.#{group_id}").destroy
    end
    newparams = coerce(params)
    @asset = Asset.new(newparams[:upload])
    @asset.user = current_user
    if @asset.save
      flash[:notice] = "Successfully created upload."
      #render :update do |page|
	    #  page.replace_html "singular_assets_for_#{group_id}", :partial => "assets/singular_assets", :locals => {:for_class => "#{entity.class.to_s}", :entity => entity }
	    #end
	    respond_to do |format|
	      format.json {render :json => { :result => 'success', :asset => "/assets/#{@asset.id}?singular=true" } }
	   	end
    else
      #render :update do |page|
	    #  page.replace_html "upload_errors_#{group_id}", errors_for(@asset)
	    #end
	    respond_to do |format|
	      format.json {render :json => { :result => 'failed' } }
	   	end
    end
  end
   
 private
  def coerce(params)
    if params[:upload].nil?
      h = Hash.new
      h[:upload] = Hash.new
      h[:upload][:data] = params[:Filedata]
      h[:upload][:data].content_type = MIME::Types.type_for(h[:upload][:data].original_filename).to_s
      h[:upload][:group_id] = params[:group_id]
      h[:upload][:attachable_type] = params[:attachable_type]
      h[:upload][:attachable_id] = params[:attachable_id]
      h
    else
      params
    end
  end

		def coerce_flex_params(params)
    if params[:upload].nil?
      h = Hash.new
      h[:upload] = Hash.new
      h[:upload][:data] = params[:Filedata]
      h[:upload][:data].content_type = MIME::Types.type_for(params[:Filename]).to_s
      h[:upload][:group_id] = params[:group_id]
      h[:upload][:attachable_type] = params[:attachable_type]
      h[:upload][:attachable_id] = params[:attachable_id]
      h
    else
      params
    end
  end

end
