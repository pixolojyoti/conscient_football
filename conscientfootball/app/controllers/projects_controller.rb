class ProjectsController < ApplicationController
   before_filter :authenticate_user!
   layout "admin"
   

  def reorder
    	order = params[:order_string]
    	Rails.logger.info "==============#{order}==========="
  	order_arr = order.split(",")
  	Rails.logger.info "==============#{order_arr}==========="
  	order_arr.each_with_index do |pid, index|
	   Rails.logger.info "==============#{pid}==========="
  		p = Project.find_by_id(pid)
  		Rails.logger.info "============1111==#{p.show_order}==========="
		p.show_order = index.to_i
		if p.save!
		   Rails.logger.info "============#{p.name }-Success============"
	       end
	end
  	 @projects = Project.order("show_order")
  	 if (request.xhr?)
		respond_to do |format|
			format.js {}
		end	
	end
  end	
    
  def assets
    @project = Project.find params[:id]
  end


  # GET /projects
  # GET /projects.xml
  def index
    @projects = Project.order("show_order")

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @projects }
    end
  end

  # GET /projects/1
  # GET /projects/1.xml
  def show
    @project = Project.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @project }
    end
  end

  # GET /projects/new
  # GET /projects/new.xml
  def new
    @project = Project.new
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @project }
    end
  end

  # GET /projects/1/edit
  def edit
    @project = Project.find(params[:id])
  end

  # POST /projects
  # POST /projects.xml
  def create
    @project = Project.new(params[:project])
     respond_to do |format|
      if @project.save
        format.html { redirect_to(@project, :notice => 'Project was successfully created.') }
        format.xml  { render :xml => @project, :status => :created, :location => @project }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @project.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /projects/1
  # PUT /projects/1.xml
  def update
    @project = Project.find(params[:id])
    
    respond_to do |format|
      if @project.update_attributes(params[:project])
        format.html { redirect_to(@project, :notice => 'Project was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @project.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.xml
  def destroy
    @project = Project.find(params[:id])
    @project.destroy

    respond_to do |format|
      format.html { redirect_to(projects_url) }
      format.xml  { head :ok }
    end
  end
end
