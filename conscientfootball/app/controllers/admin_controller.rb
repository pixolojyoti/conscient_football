class AdminController < ApplicationController
   layout "admin"
   before_filter :authenticate_user!
    
    def assign
    	id = params[:entity_id]
    	type = params[:entity_type]
		@project = eval("#{type.classify}.find(id)")
		if eval("params[:#{type}].present?") and eval("params[:#{type}][:user_#{type}_ids].present?")
			users = []
			eval("params[:#{type}][:user_#{type}_ids]").each do |x|
				users.push User.find x
			end
			@project.users = users
		else
			@project.users.clear
		end
		if @project.save
			flash[:notice_for_assignment] = "The members have been assigned"
		else
			flash[:notice_for_assignment] = "Error assigning the users"
		end
		redirect_to "/#{type.tableize}/#{@project.cached_slug}"
    end    
    
    
    def createexcel    	   
        require "csv" 
        file = "#{Rails.root}/contacts.csv"    	   
		 scope = Contact.order("created_at DESC")
		 @contacts=scope.all
		 CSV.open(file, "wb") do |row|
	       row << ["Date", "Name", "Email", "Phone", "Comments"] 
	       @contacts.each_with_index do |n, index|
		   row << [n.created_at.to_s(:notification), n.name, n.email, n.phone, n.comments ]
	       end
		 end		   
		File.open(file, 'r') do |f|
		    send_data f.read, :type => "text/csv", :filename => "contacts.csv"
		end
   	end

   	def export_excel    	   
        require "csv" 
        file = "#{Rails.root}/contacts.csv"    	   
        if params[:center].present? && params[:center] !=  ""
        	scope = TrialRegistration.where("preferred_center = ?",params[:center]).order("created_at DESC")		
		else
		 scope = TrialRegistration.order("created_at DESC")
		end 
		 @contacts=scope.all
		 CSV.open(file, "wb") do |row|
	       row << ["Full Name", "Guardians Phone", "Guardians Email", "Gender", "DOB", "Preferred Center", "School Name", "Siblings Name", "Siblings Number"] 
	       @contacts.each_with_index do |n, index|
		   row << [n.created_at.to_s(:notification), n.fullname, n.guardians_phone, n.guardians_email, n.gender, n.date_of_birth, n.preferred_center, n.school_name, n.sibling_name, n.sibling_number ]
	       end
		 end		   
		File.open(file, 'r') do |f|
		    send_data f.read, :type => "text/csv", :filename => "contacts.csv"
		end
   	end
    
    
    
    def index
    	@projects = Project.all
		#@users = User.all	      	
		#@featured_projects = Project.featured
		#@enquiry = Contact.all
		#@testi = Testimonial.all
		#@news = NewsEvent.all
    end
    
    def tiny_link
		render :layout => false
    end
    
   	def destroy_tuple
		if (request.xhr?)
		  @tuple = Tuple.find(params[:id])
		  @tuple.destroy
		  render :update do |page|
		  page.alert 'Tuple Deleted'
		  end
		end
    end
    
    def destroy_logo
		if (request.xhr?)
		  @project = Project.find(params[:pid])
		  @project.update_attributes(:logo_file_name => '')
		  render :update do |page|
		  page.alert 'Logo Deleted'
		  end
		end
    end
end
