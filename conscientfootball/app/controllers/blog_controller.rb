class BlogController < ApplicationController
 before_filter :authenticate_user!
	def blog
		scope=Blog.where(:status => true)
		@pblogs = scope.order("counter DESC")
		@blogs = scope.order("created_at DESC")		
		render :layout => false
	end
	
	def showblog
		scope=Blog.where(:status => true)
		@blogs = scope.order("created_at DESC").limit(2)
		@pblogs =scope.order("counter DESC").limit(2)
		@blog = Blog.find(params[:id])
		@blog.update_attributes(:counter => @blog.counter+1)	
	
		render :layout => false
	end
	
	def addcomment
	  @comment = Comment.new(params[:comment])		
	  if @comment.save
		render :json => {:status => "success"}
	  else
		render :json => {:status => "error"}
	  end
	end

end
