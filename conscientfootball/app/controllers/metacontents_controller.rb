class MetacontentsController < ApplicationController
  before_filter :authenticate_user!
  # GET /metacontents
  # GET /metacontents.xml
layout "admin"
  def index
    @metacontents = Metacontent.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @metacontents }
    end
  end

  # GET /metacontents/1
  # GET /metacontents/1.xml
  def show
    @metacontent = Metacontent.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @metacontent }
    end
  end

  # GET /metacontents/new
  # GET /metacontents/new.xml
  def new
    @metacontent = Metacontent.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @metacontent }
    end
  end

  # GET /metacontents/1/edit
  def edit
    @metacontent = Metacontent.find(params[:id])
  end

  # POST /metacontents
  # POST /metacontents.xml
  def create
    @metacontent = Metacontent.new(params[:metacontent])

    respond_to do |format|
      if @metacontent.save
        format.html { redirect_to(@metacontent, :notice => 'Metacontent was successfully created.') }
        format.xml  { render :xml => @metacontent, :status => :created, :location => @metacontent }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @metacontent.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /metacontents/1
  # PUT /metacontents/1.xml
  def update
    @metacontent = Metacontent.find(params[:id])

    respond_to do |format|
      if @metacontent.update_attributes(params[:metacontent])
        format.html { redirect_to(@metacontent, :notice => 'Metacontent was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @metacontent.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /metacontents/1
  # DELETE /metacontents/1.xml
  def destroy
    @metacontent = Metacontent.find(params[:id])
    @metacontent.destroy

    respond_to do |format|
      format.html { redirect_to(metacontents_url) }
      format.xml  { head :ok }
    end
  end
  private
  def sort_column
    Metacontent.column_names.include?(params[:sort]) ? params[:sort] : "url"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end

end
