class TrialRegistrationsController < ApplicationController
  # GET /trial_registrations
  # GET /trial_registrations.xml
  layout 'admin'
  def index
    @trial_registrations = TrialRegistration.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @trial_registrations }
    end
  end

  # GET /trial_registrations/1
  # GET /trial_registrations/1.xml
  def show
    @trial_registration = TrialRegistration.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @trial_registration }
    end
  end

  # GET /trial_registrations/new
  # GET /trial_registrations/new.xml
  def new
    @trial_registration = TrialRegistration.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @trial_registration }
    end
  end

  # GET /trial_registrations/1/edit
  def edit
    @trial_registration = TrialRegistration.find(params[:id])
  end

  # POST /trial_registrations
  # POST /trial_registrations.xml
  def create
    @trial_registration = TrialRegistration.new(params[:trial_registration])

    respond_to do |format|
      if @trial_registration.save
        format.html { redirect_to(@trial_registration, :notice => 'Trial registration was successfully created.') }
        format.xml  { render :xml => @trial_registration, :status => :created, :location => @trial_registration }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @trial_registration.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /trial_registrations/1
  # PUT /trial_registrations/1.xml
  def update
    @trial_registration = TrialRegistration.find(params[:id])

    respond_to do |format|
      if @trial_registration.update_attributes(params[:trial_registration])
        format.html { redirect_to(@trial_registration, :notice => 'Trial registration was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @trial_registration.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /trial_registrations/1
  # DELETE /trial_registrations/1.xml
  def destroy
    @trial_registration = TrialRegistration.find(params[:id])
    @trial_registration.destroy

    respond_to do |format|
      format.html { redirect_to(trial_registrations_url) }
      format.xml  { head :ok }
    end
  end
end
