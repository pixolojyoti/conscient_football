class SiteController < ApplicationController
		
	layout "site"
	before_filter :set_year, :only=> [:gallery,:gallery_details,:gallery_details_new]
	require "net/http"
  	require "net/https"
  	require "uri"
	def index
	end

	def aboutus
	end

	def not_found
  	 respond_to do |format|
      format.html { render :status => 404 }
    end
  	end

	def fb_lead
		@api_key='233c3aee023eb2b721d0efce532a3302'
		@facebook_contacts=FacebookContact.new
		@facebook_contacts.fullname=params[:player_name] rescue ''
		@facebook_contacts.email=params[:email]
		@facebook_contacts.phone=params[:phone]
		@facebook_contacts.gender=params[:gender] rescue ''
		@facebook_contacts.utm_campaign=params[:campaign] rescue ''
		@facebook_contacts.form_id=params[:form_id] rescue ''

		@facebook_contacts.school_name=params[:player_school_name] rescue ''
		@facebook_contacts.date_of_birth="01/01/#{params[:player_year_of_birth]}"
		srd = ""
		if(params[:preferred_coaching_center].present? && params[:preferred_coaching_center] != "")
			@facebook_contacts.preferred_center= params[:preferred_coaching_center]
			@protemp = @facebook_contacts.preferred_center
			
			if @protemp.include? "matunga"
				@project = "573e9c375413853d070000e8"
				srd="59a416ff3bb2f825a00020a6"
			elsif @protemp.include? "andheri"
				@project = "573e9c803bb2f8a95d00002e"
				srd="59a416ff3bb2f825a00020a6"
			elsif @protemp.include? "gurgaon"
				@project = "573e9a64a7a0396c6f0000ef"
				srd="59a416c73bb2f85d700022e1"
			elsif @protemp.include? "noida"
				@project = "573e9be0a7a0396c6f00011e"
				srd="59a416c73bb2f85d700022e1"
			elsif @protemp.include? "clinics"
				@project = "596dbb8b3bb2f83a7c000a84"
			elsif @protemp.include? "vasant"
				@project = "599426973bb2f8755300154e"
				srd="59a416c73bb2f85d700022e1"
			elsif @protemp.include? "saket"
				@project = "573e99825413856c68000146"
				srd="59a416c73bb2f85d700022e1"
			elsif @protemp.include? "rajouri_garden"
				@project = "573e99fd3bb2f81091000095"
				srd="59a416c73bb2f85d700022e1"
			elsif @protemp.include? "south_mumbai"
				@project = "59703f94541385984000029e"
				srd="59a416ff3bb2f825a00020a6"
			else
				@project = ""
			end
		else
			  @project = ""
		end
		if(@facebook_contacts.form_id == "758183164388363")
			srd="59a52aa4541385abc200048e"
			@project = "596dbb8b3bb2f83a7c000a84"
		end
		@facebook_contacts.project = @project
		if @facebook_contacts.save
			Rails.logger.info "save......."
			 begin
              url = "http://estate.sell.do/api/leads/create?sell_do[form][lead][name]=#{@facebook_contacts.fullname}&sell_do[form][lead][phone]=#{@facebook_contacts.phone}&sell_do[form][lead][email]=#{@facebook_contacts.email}&sell_do[form][lead][sex]=#{@facebook_contacts.gender}&sell_do[form][custom][birth_date]=#{@facebook_contacts.date_of_birth}&sell_do[form][address][city]=#{@facebook_contacts.city}&sell_do[form][lead][project_id]=#{@project}&sell_do[form][custom][school_name]=#{@facebook_contacts.school_name}&sell_do[form][custom][sibling_name]=&sell_do[form][custom][sibling_number]=&api_key=#{@api_key}&sell_do[campaign][srd]=#{srd}&sell_do[form][note][content]="
           		  Rails.logger.info "-------#{url}----------"
             	  url = URI::escape url
                  result = Net::HTTP.get(URI.parse(url))
                  Rails.logger.info "___________________________________#{result}_______________________________________-"
            rescue Exception => e
                 Rails.logger.info "--------Error--#{e}-------"
            end
		else
			render :json => {:status => "error"}	
		end
	end
	
	def aboutus1
	end

	def ourprogram_new
	end
	def gallery
	end
	def gallery_details
		@category=Category.where(:status => true)
		if params[:year].present?
			@year=params[:year] 
		else
			@year=@years.first 
		end
	end

	def media
						
	end

	def news
		@news = News.where(:status => true).order("show_order DESC").order("created_at DESC")
	end

	def videos
		@videos= Video.where(:status => true).order("show_order DESC").order("created_at DESC")
	end

	def press_release
		@press_release= PressRelease.where(:status => true).order("show_order DESC").order("created_at DESC")	
	end

	def blog_list
		@blogs= Blog.where(:status => true).order("created_at DESC")	
	end

	def blog_details
		@blog=Blog.find(params[:id])
	end
	def careers
	end

	def registeration
		render :layout => false
	end

	def registeration_clinics
		render :layout => false
	end
	def create_registration
 		@school_registration = SchoolRegistration.new(params[:school_registration])
     	if @school_registration.save
     	    render :json => {:status => "success", :mesg => "Thankyou", :contact => @school_registration}
        	FormNotifier.notify_school_registration_end_user(@school_registration, "School Registration  Successful").deliver
       		FormNotifier.notify_school_registration_admin(@school_registration, "School Registration  Successful").deliver
        else
            render :json => {:status => "error", :error => @school_registration.errors.full_messages}
        end
	end
	def create_career
 		@career = Career.new(params[:career])
     	if @career.save
     	    render :json => {:status => "success", :mesg => "Thankyou", :career => @career}
        	 #FormNotifier.notify_career_end_user(@career, "Registration Successful").deliver
       		 FormNotifier.notify_career_admin(@career, "Career lead Details").deliver
        else
            render :json => {:status => "error", :error => @career.errors.full_messages}
        end
	end

	def gallery_details_new
		@category=Category.where(:status => true)
		if params[:year].present?
			@year=params[:year] 
		else
			@year=@years.first 
		end
	end
	def training
	end

	def annual_calendar
	end

	def barcelona_clinic
	end

	def camps
	end

	def cpfl
	end

	def cpfl_form
	end

	def faq
	end

	def faq_overview
	end

	def fcbse_venue
	end

	def football_camps
	end

	def gallery_old
	end


	def how_we_train
	end


	def league_table
	end


	def our_program_training
	end

	def overview_fcbescola
	end


	def rules_regulations
	end


	def schedule
	end

	def training_program
	end

	def view_center
	end

	def why_join
	end

	def camp_schedule
	end

	def fcbescolanew
		
	end

	def social_responsiblity
		
	end

	def terms_condition_clinics

	end
	def summers_camps
		
	end
	def upcoming_events
		@events = Event.where(:status => true).order("created_at DESC")		
	end
	def upcoming_event_details
		@event = Event.find(params[:id])		
	end
	def success_stories
		@awards = Award.where(:status => true).order("show_order DESC").order("published_date DESC")		
	end
	def success_stories_details
		@award = Award.find(params[:id])		
	end
	def about_barca_clinic
	end

	def terms_condition
	end
	def fcbescola_preregistration
	end
	def fcbescola_goals

	end
	def ccavRequestHandler
		render :layout => false
	end
	def ccavResponseHandler
		  workingKey="82117D2FD02D73B7078F3D968781318E"#Put in the 32 Bit Working Key provided by CCAVENUES.   
		  encResponse=params[:encResp]
		  crypto = Crypto.new 
		  @decResp=crypto.decrypt(encResponse,workingKey) rescue ''
		 # decResp1=crypto.decrypt(encResponse,workingKey);
		  @decResp = @decResp.split("&") rescue []
		  #Rails.logger.info "#{decResp.inspect}"
	  		@decResp.each do |key|
			  	if key.from(0).to(key.index("=")-1)=='order_status'
			  		@order_status=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='tracking_id'
			  		@tracking_id=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='bank_ref_no'
			  		@bank_ref_no=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='failure_message'
			  		@failure_message=key.from(key.index("=")+1).to(-1)	
			  	end			  				  	
			  	if key.from(0).to(key.index("=")-1)=='billing_address'
			  		@billing_address=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='billing_city'
			  		@billing_city=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='billing_state'
			  		@billing_state=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='billing_zip'
			  		@billing_zip=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='billing_country'
			  		@billing_country=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='order_id'
			  		@order_id=key.from(key.index("=")+1).to(-1)	
			  	end			  				  				  				  	
	  		end 
	  		#Rails.logger.info "#{@order_id}---- #{@order_status}----#{@billing_zip}"
	 if @order_id.present?
		@cpfl_reg = CpflRegistrationNew.where('order_id=?',@order_id).first
		if @cpfl_reg.present?
			#@cpfl_reg.update_attributes(:order_status=> "ABC", :tracking_id=> "12345678")
			@cpfl_reg.update_attributes(:order_status => @order_status,:tracking_id => @tracking_id,:bank_ref_no => @bank_ref_no,:failure_message => @failure_message,:billing_address => @billing_address,:billing_city => @billing_city,:billing_state => @billing_state,:billing_zip => @billing_zip,:billing_country => @billing_country)
			@cpfl_reg1 = CpflRegistrationNew.where('order_id=?',@order_id).first
			     FormNotifier.notify_cpflreg(@cpfl_reg1, "Reference Lead Details CPFL Reg").deliver
			 if @order_status=='Success'
                 FormNotifier.notify_end_user_cpfl(@cpfl_reg1, "Thank you for your interest in Conscient Football").deliver
             end

		end
	end
	end
	def ccavResponseHandler_clinic
		  workingKey="82117D2FD02D73B7078F3D968781318E"#Put in the 32 Bit Working Key provided by CCAVENUES.   
		  encResponse=params[:encResp]
		  crypto = Crypto.new 
		  @decResp=crypto.decrypt(encResponse,workingKey) rescue ''
		 # decResp1=crypto.decrypt(encResponse,workingKey);
		  @decResp = @decResp.split("&") rescue []
		  #Rails.logger.info "#{decResp.inspect}"
	  		@decResp.each do |key|
			  	if key.from(0).to(key.index("=")-1)=='order_status'
			  		@order_status=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='tracking_id'
			  		@tracking_id=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='bank_ref_no'
			  		@bank_ref_no=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='failure_message'
			  		@failure_message=key.from(key.index("=")+1).to(-1)	
			  	end			  				  	
			  	if key.from(0).to(key.index("=")-1)=='billing_address'
			  		@billing_address=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='billing_city'
			  		@billing_city=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='billing_state'
			  		@billing_state=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='billing_zip'
			  		@billing_zip=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='billing_country'
			  		@billing_country=key.from(key.index("=")+1).to(-1)	
			  	end
			  	if key.from(0).to(key.index("=")-1)=='order_id'
			  		@order_id=key.from(key.index("=")+1).to(-1)	
			  	end			  				  				  				  	
	  		end 
	  		#Rails.logger.info "#{@order_id}---- #{@order_status}----#{@billing_zip}"
	 if @order_id.present?
		@cpfl_reg = ClinicRegistration.where('order_id=?',@order_id).first
		if @cpfl_reg.present?
			#@cpfl_reg.update_attributes(:order_status=> "ABC", :tracking_id=> "12345678")
			@cpfl_reg.update_attributes(:order_status => @order_status,:tracking_id => @tracking_id,:bank_ref_no => @bank_ref_no,:failure_message => @failure_message,:billing_address => @billing_address,:billing_city => @billing_city,:billing_state => @billing_state,:billing_zip => @billing_zip,:billing_country => @billing_country)
			@cpfl_reg1 = ClinicRegistration.where('order_id=?',@order_id).first
			     FormNotifier.notify_clinicreg(@cpfl_reg1, "Reference Lead Details Clinic Reg").deliver
			 if @order_status=='Success'
                 FormNotifier.notify_end_user_clinic(@cpfl_reg1, "Thank you for your interest in Conscient Football").deliver
             end

		end
	end
	end	
	
   def enquireForm    
          @contact = FcbescolaTrial.new(params[:contact])
          if @contact.save
            FormNotifier.notify_fcbescola_prereg(@contact, "Reference Lead Details FCBEscola Pre Trail").deliver
            FormNotifier.notify_end_user_fcbescola_prereg(@contact, "Thank you for your interest in Conscient Football").deliver
            render :json => {:status => "success", :mesg => "Thankyou", :contact => @contact}
          else
            render :json => {:status => "error", :error => @contact.errors.full_messages}
          end
          
   end	
   def cpflenquireForm
          @contact = CpflRegistrationNew.new(params[:contact])
          if @contact.save
             #FormNotifier.notify_fcbescola_prereg(@contact, "Reference Lead Details CPFL").deliver
             #FormNotifier.notify_end_user_fcbescola_prereg(@contact, "Thank you for your interest in Conscient Football").deliver
            render :json => {:status => "success", :mesg => "Thankyou", :contact => @contact}
          else
            render :json => {:status => "error", :error => @contact.errors.full_messages}
          end   
   end 

    def facility
          @contact = Facility.new(params[:contact])
          if @contact.save
             FormNotifier.notify_facility(@contact, "Reference Lead Details ").deliver
             FormNotifier.notify_end_user_facility(@contact, "Thank you for your interest in Conscient Football").deliver
            render :json => {:status => "success", :mesg => "Thankyou", :contact => @contact}
          else
            render :json => {:status => "error", :error => @contact.errors.full_messages}
          end   
   end

   def clinics_form
          @contact = ClinicRegistration.new(params[:contact])
          if @contact.save
             #FormNotifier.notify_fcbescola_prereg(@contact, "Reference Lead Details CPFL").deliver
             #FormNotifier.notify_end_user_fcbescola_prereg(@contact, "Thank you for your interest in Conscient Football").deliver
            render :json => {:status => "success", :mesg => "Thankyou", :contact => @contact}
          else
            render :json => {:status => "error", :error => @contact.errors.full_messages}
          end   
   end 
   private
  	def set_year
   		@years = Gallery.where(:status=>true).select("published_year").order("published_year desc").collect{|x| x.published_year}.uniq.compact rescue []
  	end

end
