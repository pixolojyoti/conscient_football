class SchoolRegistrationsController < ApplicationController
  before_filter :authenticate_user!
  # GET /contacts
  # GET /contacts.xml
  
     layout "admin"
  def index
    @school_registrations = SchoolRegistration.order('created_at DESC')

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @school_registrations }
    end
  end

  # GET /school_registrations/1
  # GET /school_registrations/1.xml
  def show
    @school_registration = SchoolRegistration.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @school_registration }
    end
  end

  # GET /school_registrations/new
  # GET /school_registrations/new.xml
  def new
    @school_registration = SchoolRegistration.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @school_registration }
    end
  end

  # GET /school_registrations/1/edit
  def edit
    @school_registration = SchoolRegistration.find(params[:id])
  end

  # POST /school_registrations
  # POST /school_registrations.xml
  def create
    @school_registration = SchoolRegistration.new(params[:school_registration])

    respond_to do |format|
      if @school_registration.save
        format.html { redirect_to(@school_registration, :notice => 'School registration was successfully created.') }
        format.xml  { render :xml => @school_registration, :status => :created, :location => @school_registration }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @school_registration.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /school_registrations/1
  # PUT /school_registrations/1.xml
  def update
    @school_registration = SchoolRegistration.find(params[:id])

    respond_to do |format|
      if @school_registration.update_attributes(params[:school_registration])
        format.html { redirect_to(@school_registration, :notice => 'School registration was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @school_registration.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /school_registrations/1
  # DELETE /school_registrations/1.xml
  def destroy
    @school_registration = SchoolRegistration.find(params[:id])
    @school_registration.destroy

    respond_to do |format|
      format.html { redirect_to(school_registrations_url) }
      format.xml  { head :ok }
    end
  end
  def registration_data
    require "csv"
    file = "#{Rails.root}/registration_data.csv"
    scope = SchoolRegistration.where("created_at >= ? AND created_at <= ?", params[:from_date],params[:to_date].to_date+ 1.days).order('created_at DESC')
    if params[:enquire_type]!=""
      scope=scope.where(:enquire_type=>params[:enquire_type])
    end
    @registration=scope.all
    CSV.open(file, "wb") do |row|
      row << ["Date", "School name", "Address", "Email", "Mobile", "Enquire Type", "No of participant"]
      @registration.each_with_index do |n, index|
        row << [n.created_at.to_s(:notification), n.school_name, n.address, n.email, n.mobile, n.enquire_type, n.no_of_participation]
      end
    end
    File.open(file, 'r') do |f|
      send_data f.read, :type => "text/csv", :filename => "registration_data.csv"
    end
  end
end
