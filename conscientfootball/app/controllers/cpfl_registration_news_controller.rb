class CpflRegistrationNewsController < ApplicationController
  # GET /cpfl_registration_news
  # GET /cpfl_registration_news.xml
    layout "admin"
       before_filter :authenticate_user!
  def index
    @cpfl_registration_news = CpflRegistrationNew.order("created_at DESC")

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @cpfl_registration_news }
    end
  end

  # GET /cpfl_registration_news/1
  # GET /cpfl_registration_news/1.xml
  def show
    @cpfl_registration_news = CpflRegistrationNew.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @cpfl_registration_news }
    end
  end

  # GET /cpfl_registration_news/new
  # GET /cpfl_registration_news/new.xml
  def new
    @cpfl_registration_news = CpflRegistrationNew.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @cpfl_registration_news }
    end
  end

  # GET /cpfl_registration_news/1/edit
  def edit
    @cpfl_registration_news = CpflRegistrationNew.find(params[:id])
  end

  # POST /cpfl_registration_news
  # POST /cpfl_registration_news.xml
  def create
    @cpfl_registration_news = CpflRegistrationNew.new(params[:cpfl_registration_news])

    respond_to do |format|
      if @cpfl_registration_news.save
        format.html { redirect_to(@cpfl_registration_news, :notice => 'Cpfl registration new was successfully created.') }
        format.xml  { render :xml => @cpfl_registration_news, :status => :created, :location => @cpfl_registration_news }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @cpfl_registration_news.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /cpfl_registration_news/1
  # PUT /cpfl_registration_news/1.xml
  def update
    @cpfl_registration_news = CpflRegistrationNew.find(params[:id])

    respond_to do |format|
      if @cpfl_registration_news.update_attributes(params[:cpfl_registration_news])
        format.html { redirect_to(@cpfl_registration_news, :notice => 'Cpfl registration new was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @cpfl_registration_news.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /cpfl_registration_news/1
  # DELETE /cpfl_registration_news/1.xml
  def destroy
    @cpfl_registration_news = CpflRegistrationNew.find(params[:id])
    @cpfl_registration_news.destroy

    respond_to do |format|
      format.html { redirect_to(cpfl_registration_news_url) }
      format.xml  { head :ok }
    end
  end
end
