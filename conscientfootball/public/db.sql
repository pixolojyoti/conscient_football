-- MySQL dump 10.13  Distrib 5.1.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: conscientfootball_production
-- ------------------------------------------------------
-- Server version	5.1.41-3ubuntu12.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_size` int(11) DEFAULT NULL,
  `group_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alt_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `asset_order` int(11) DEFAULT NULL,
  `attachable_id` int(11) DEFAULT NULL,
  `attachable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
INSERT INTO `assets` VALUES (1,'12647450_452993544905826_5343263848541701497_n.jpg','image/jpeg',119405,'main_image',NULL,NULL,NULL,NULL,1,'Award','2017-02-21 11:37:57','2017-02-21 11:37:57'),(2,'press.jpg','image/jpeg',141795,'assets_overview',NULL,NULL,NULL,NULL,1,'award','2017-02-21 11:38:07','2017-02-21 11:38:07'),(3,'12694806_452993001572547_2352199351388008052_o.jpg','image/jpeg',125958,'assets_overview',NULL,NULL,NULL,NULL,1,'award','2017-02-21 11:38:10','2017-02-21 11:38:10'),(4,'12717769_452992908239223_1700440983087642988_n.jpg','image/jpeg',125427,'assets_overview',NULL,NULL,NULL,NULL,1,'award','2017-02-21 11:38:11','2017-02-21 11:38:11'),(5,'12647450_452993544905826_5343263848541701497_n.jpg','image/jpeg',119405,'assets_overview',NULL,NULL,NULL,NULL,1,'award','2017-02-21 11:38:13','2017-02-21 11:38:13'),(6,'imgfutsal.jpg','image/jpeg',69568,'assets_overview',NULL,NULL,NULL,NULL,2,'award','2017-02-21 11:40:00','2017-02-21 11:40:00'),(7,'imgfutsal.jpg','image/jpeg',69568,'main_image',NULL,NULL,NULL,NULL,2,'Award','2017-02-21 11:40:00','2017-02-21 11:40:00'),(8,'Mariya_Tahreem_Khan.jpg','image/jpeg',114482,'main_image',NULL,NULL,NULL,NULL,3,'Award','2017-02-21 11:41:09','2017-02-21 11:41:09'),(9,'Mariya_Tahreem_Khan.jpg','image/jpeg',114482,'assets_overview',NULL,NULL,NULL,NULL,3,'award','2017-02-21 11:41:20','2017-02-21 11:41:20'),(10,'13315414_489436467928200_2539794058584474268_n.jpg','image/jpeg',71474,'main_image',NULL,NULL,NULL,NULL,4,'Award','2017-02-21 11:42:14','2017-02-21 11:42:14'),(11,'13329420_489437091261471_8525549116422737862_o.jpg','image/jpeg',274535,'assets_overview',NULL,NULL,NULL,NULL,4,'award','2017-02-21 11:42:23','2017-02-21 11:42:23'),(12,'13315414_489436467928200_2539794058584474268_n.jpg','image/jpeg',71474,'assets_overview',NULL,NULL,NULL,NULL,4,'award','2017-02-21 11:42:25','2017-02-21 11:42:25'),(13,'13315251_489436847928162_3087638132968395740_n.jpg','image/jpeg',65403,'assets_overview',NULL,NULL,NULL,NULL,4,'award','2017-02-21 11:42:27','2017-02-21 11:42:27'),(14,'13325642_489436734594840_4335697617795509667_n.jpg','image/jpeg',51401,'assets_overview',NULL,NULL,NULL,NULL,4,'award','2017-02-21 11:42:29','2017-02-21 11:42:29'),(15,'img-2.jpg','image/jpeg',65834,'main_image',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:40','2017-02-21 11:44:40'),(16,'img-3.jpg','image/jpeg',66383,'assets_gallery',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:44','2017-02-21 11:44:44'),(17,'img-2.jpg','image/jpeg',65834,'assets_gallery',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:47','2017-02-21 11:44:47'),(18,'img-4.jpg','image/jpeg',62860,'assets_gallery',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:49','2017-02-21 11:44:49'),(19,'img-6.jpg','image/jpeg',54313,'assets_gallery',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:53','2017-02-21 11:44:53'),(20,'img-1.jpg','image/jpeg',53394,'assets_gallery',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:55','2017-02-21 11:44:55'),(21,'img-7.jpg','image/jpeg',50861,'assets_gallery',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:57','2017-02-21 11:44:57'),(22,'img-5.jpg','image/jpeg',48244,'assets_gallery',NULL,NULL,NULL,NULL,1,'Gallery','2017-02-21 11:44:58','2017-02-21 11:44:58'),(23,'img-1.jpg','image/jpeg',63289,'assets_gallery',NULL,NULL,NULL,NULL,2,'Gallery','2017-02-21 11:47:08','2017-02-21 11:47:08'),(24,'img-1.jpg','image/jpeg',63289,'main_image',NULL,NULL,NULL,NULL,2,'Gallery','2017-02-21 11:47:08','2017-02-21 11:47:08'),(25,'img-2.jpg','image/jpeg',62137,'assets_gallery',NULL,NULL,NULL,NULL,2,'Gallery','2017-02-21 11:47:10','2017-02-21 11:47:10'),(26,'img-3.jpg','image/jpeg',60221,'assets_gallery',NULL,NULL,NULL,NULL,2,'Gallery','2017-02-21 11:47:11','2017-02-21 11:47:11'),(27,'img-4.jpg','image/jpeg',53544,'assets_gallery',NULL,NULL,NULL,NULL,2,'Gallery','2017-02-21 11:47:12','2017-02-21 11:47:12'),(28,'img-5.jpg','image/jpeg',49423,'assets_gallery',NULL,NULL,NULL,NULL,2,'Gallery','2017-02-21 11:47:13','2017-02-21 11:47:13'),(29,'img-1.jpg','image/jpeg',67261,'main_image',NULL,NULL,NULL,NULL,3,'Gallery','2017-02-21 11:50:00','2017-02-21 11:50:00'),(30,'img-2.jpg','image/jpeg',67407,'assets_gallery',NULL,NULL,NULL,NULL,3,'Gallery','2017-02-21 11:50:05','2017-02-21 11:50:05'),(31,'img-1.jpg','image/jpeg',67261,'assets_gallery',NULL,NULL,NULL,NULL,3,'Gallery','2017-02-21 11:50:06','2017-02-21 11:50:06'),(32,'img-6.jpg','image/jpeg',59232,'assets_gallery',NULL,NULL,NULL,NULL,3,'Gallery','2017-02-21 11:50:07','2017-02-21 11:50:07'),(33,'img-3.jpg','image/jpeg',57149,'assets_gallery',NULL,NULL,NULL,NULL,3,'Gallery','2017-02-21 11:50:09','2017-02-21 11:50:09'),(34,'img-5.jpg','image/jpeg',56106,'assets_gallery',NULL,NULL,NULL,NULL,3,'Gallery','2017-02-21 11:50:11','2017-02-21 11:50:11'),(35,'img-4.jpg','image/jpeg',49825,'assets_gallery',NULL,NULL,NULL,NULL,3,'Gallery','2017-02-21 11:50:12','2017-02-21 11:50:12'),(36,'8.jpg','image/jpeg',16013,'main_image',NULL,NULL,NULL,NULL,1,'Blog','2017-03-30 14:35:20','2017-03-30 14:35:20'),(37,'12.jpg','image/jpeg',85921,'main_image',NULL,NULL,NULL,NULL,5,'Award','2017-04-03 09:21:14','2017-04-03 09:21:14'),(39,'CPFL.jpg','image/jpeg',84708,'assets_overview',NULL,NULL,NULL,NULL,2,'award','2017-04-03 09:21:58','2017-04-03 09:21:58'),(40,'12.jpg','image/jpeg',85921,'assets_overview',NULL,NULL,NULL,NULL,5,'award','2017-04-04 05:53:40','2017-04-04 05:53:40'),(49,'122.png','image/png',13750,'main_image',NULL,NULL,NULL,NULL,1,'Video','2017-04-21 12:21:53','2017-04-21 12:21:53'),(50,'122.png','image/png',13750,'main_image',NULL,NULL,NULL,NULL,2,'Video','2017-04-21 12:22:42','2017-04-21 12:22:42'),(51,'9k_.jpg','image/jpeg',79892,'main_image',NULL,NULL,NULL,NULL,1,'PressRelease','2017-04-21 12:31:35','2017-04-21 12:31:35'),(53,'9k_.jpg','image/jpeg',17737,'main_image',NULL,NULL,NULL,NULL,2,'PressRelease','2017-04-21 12:34:39','2017-04-21 12:34:39');
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `awards`
--

DROP TABLE IF EXISTS `awards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `awards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `featured` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `awards`
--

LOCK TABLES `awards` WRITE;
/*!40000 ALTER TABLE `awards` DISABLE KEYS */;
INSERT INTO `awards` VALUES (1,'Avika & Mahira','','FCBEscola South Delhi center students Avika Singh and Mahira Jalan were a part of the Delhi State Sub- Junior (Under - 14) Girls Football Team in the Nationals held during January 15th and Febuary 2nd in Cuttack, Odisha. Mahira captained the team. Well done girls!',1,1,'2017-02-21 11:36:56','2017-02-21 11:36:56','avika-mahira','2017-01-16 00:00:00'),(2,'Futsal World Cup','','Three players from FCBEscola Delhi NCR selected for the India squad. Congratulations Prithviraj, Divyansh and Sreejit!',1,1,'2017-02-21 11:38:50','2017-02-21 11:38:50','futsal-world-cup','2017-01-09 00:00:00'),(3,'Mariya Tahreem Khan','','FCBEscola student Mariya Tehreem Khan represented Delhi U-18 Juniors national football championship.\r\nThe Tournament was held in Goa.\r\nThe team played against Meghalaya, Goa, Mizoram, Orissa, Chhattisgarh and Manipur. She scored 6 goals in the whole tournament and the team reached semi finals for the first time in 52 years. Well done Mariya!',1,1,'2017-02-21 11:40:41','2017-02-21 11:40:41','mariya-tahreem-khan','2017-01-21 00:00:00'),(4,'Rohit Gusain','','FCBEscola player Rohit Gusain(number 7) representing India Under 14 team at the AFC U-14 Festival of Football held in Dushanbe, Tajikistan. The team played against Nepal, Iran, Kyrgyzstan, Tajikistan and Afghanistan. Rohit, affectionately known as Speedy by his coaches and teammates at FCBEscola has made us proud. He was also a part of the Conscient Football Under 15 team that played in AIFF youth League held in Chandigarh.We congratulate him on this great achievement.',1,1,'2017-02-21 11:41:44','2017-02-21 11:41:44','rohit-gusain','2017-01-21 00:00:00'),(5,'Gilman and Kshitij','','Gilman and Kshitij, who have been training with us for last 5 years got selected out of the 9 boys selected from all over India in Reliance Young Champs- ISL official Grassroots Scholarship program. ',1,1,'2017-04-03 09:20:16','2017-04-03 09:20:16','gilman-and-kshitij','2017-04-03 00:00:00');
/*!40000 ALTER TABLE `awards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `short_description` text COLLATE utf8_unicode_ci,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `published_on` date DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metatitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metakeyword` text COLLATE utf8_unicode_ci,
  `metadescription` text COLLATE utf8_unicode_ci,
  `featured` tinyint(1) DEFAULT NULL,
  `counter` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogs`
--

LOCK TABLES `blogs` WRITE;
/*!40000 ALTER TABLE `blogs` DISABLE KEYS */;
INSERT INTO `blogs` VALUES (1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tum ille timide vel potius verecunde: Facio, inquit. Nam memini etiam quae nolo, oblivisci non possum Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tum ille timide vel potius verecunde: Facio, inquit. Nam memini etiam quae nolo, oblivisci non possum','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tum ille timide vel potius verecunde: Facio, inquit. Nam memini etiam quae nolo, oblivisci non possum Lorem ipsum dolor sit amet','lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit',1,'2017-02-17','uncategorized',NULL,NULL,NULL,1,0,'2017-02-17 11:28:57','2017-02-17 11:28:57');
/*!40000 ALTER TABLE `blogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `careers`
--

DROP TABLE IF EXISTS `careers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `careers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume_file_size` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `careers`
--

LOCK TABLES `careers` WRITE;
/*!40000 ALTER TABLE `careers` DISABLE KEYS */;
INSERT INTO `careers` VALUES (1,'sonu','sonupajai22@gmail.com','9898989898','Amura_Attendance_Policy.pdf','application/pdf',72283,'2017-04-04 14:40:45','2017-04-04 14:40:45'),(2,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:02','2017-04-06 20:12:02'),(3,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:07','2017-04-06 20:12:07'),(4,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:12','2017-04-06 20:12:12'),(5,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:16','2017-04-06 20:12:16'),(6,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:20','2017-04-06 20:12:20'),(7,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:24','2017-04-06 20:12:24'),(8,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:29','2017-04-06 20:12:29'),(9,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:33','2017-04-06 20:12:33'),(10,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:38','2017-04-06 20:12:38'),(11,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:39','2017-04-06 20:12:39'),(12,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:42','2017-04-06 20:12:42'),(13,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:44','2017-04-06 20:12:44'),(14,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:48','2017-04-06 20:12:48'),(15,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:49','2017-04-06 20:12:49'),(16,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:52','2017-04-06 20:12:52'),(17,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:53','2017-04-06 20:12:53'),(18,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:57','2017-04-06 20:12:57'),(19,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:12:58','2017-04-06 20:12:58'),(20,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:13:02','2017-04-06 20:13:02'),(21,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:13:03','2017-04-06 20:13:03'),(22,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:13:07','2017-04-06 20:13:07'),(23,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:13:08','2017-04-06 20:13:08'),(24,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:13:09','2017-04-06 20:13:09'),(25,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:13:11','2017-04-06 20:13:11'),(26,'hridja','test@amuratech.com','7756873800','1._Home.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',65055,'2017-04-06 20:13:12','2017-04-06 20:13:12'),(27,'sonu','sonupajai22@gmail.com','9898989898','code_standards.txt','text/plain',18285,'2017-04-07 06:35:32','2017-04-07 06:35:32'),(28,'sonu','sonupajai22@gmail.com','9898989898','code_standards.txt','text/plain',18285,'2017-04-07 06:36:04','2017-04-07 06:36:04'),(29,'atul','atul.saini@amuratech.com','8983477765','imp.docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document',16081,'2017-04-07 06:47:14','2017-04-07 06:47:14'),(30,'sonu','sonupajai22@gmail.com','9898989898','code_standards.txt','text/plain',18285,'2017-04-07 09:28:40','2017-04-07 09:28:40'),(31,'sonu','sonupajai22@gmail.com','9898989898','code_standards.txt','text/plain',18285,'2017-04-07 10:06:52','2017-04-07 10:06:52'),(32,'Piyush Sethi','piyush@amuratech.com','8878697280','Untitled_Document_1.txt','text/plain',13,'2017-04-25 12:59:09','2017-04-25 12:59:09');
/*!40000 ALTER TABLE `careers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `category_group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_group_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinic_registrations`
--

DROP TABLE IF EXISTS `clinic_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinic_registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobileno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pincode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `school_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `standard` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `food_preference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_mobileno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_relationship` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adult_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_terms` tinyint(1) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `order_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tracking_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `back_ref_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `failure_message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `data_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_size` int(11) DEFAULT NULL,
  `youth_kit_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount_pay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinic_registrations`
--

LOCK TABLES `clinic_registrations` WRITE;
/*!40000 ALTER TABLE `clinic_registrations` DISABLE KEYS */;
INSERT INTO `clinic_registrations` VALUES (1,'sunil',NULL,NULL,'Bibwewadi-Katraj','pune','411046','maharashtra','Male','2017-02-01 00:00:00','sdds','sdf','Veg','sdf','8551018446','sunil@amuratech.com','ssd','l',NULL,673358546,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-02-20 12:03:25','2017-02-20 12:03:25','ccavenu.png','image/png',176491,'l','200000'),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-03 06:35:40','2017-03-03 06:35:40',NULL,NULL,NULL,NULL,NULL),(3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-03 10:16:05','2017-03-03 10:16:05',NULL,NULL,NULL,NULL,NULL),(4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-03 10:16:15','2017-03-03 10:16:15',NULL,NULL,NULL,NULL,NULL),(5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-03 10:16:18','2017-03-03 10:16:18',NULL,NULL,NULL,NULL,NULL),(6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-03 10:16:18','2017-03-03 10:16:18',NULL,NULL,NULL,NULL,NULL),(7,'test',NULL,NULL,'test','test','411035','test','Male','2017-03-07 00:00:00','test','','Non-Veg','test','1234567890','test@gmail.com','test',NULL,NULL,789333095,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-07 07:00:29','2017-03-07 07:00:29','index.png','image/png',3142,'s','200000');
/*!40000 ALTER TABLE `clinic_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `blog_id` int(11) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mailer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campaign` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profession` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_area` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `organization` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enquiry_about` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entity_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entity_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `query` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hidden_data` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpfl_registration_news`
--

DROP TABLE IF EXISTS `cpfl_registration_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpfl_registration_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `school_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `standard` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_size` int(11) DEFAULT NULL,
  `youth_kit_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adult_kit_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount_pay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tracking_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_ref_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `failure_message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpfl_registration_news`
--

LOCK TABLES `cpfl_registration_news` WRITE;
/*!40000 ALTER TABLE `cpfl_registration_news` DISABLE KEYS */;
INSERT INTO `cpfl_registration_news` VALUES (1,'sunil','Male','2017-02-18 00:00:00','s','s','8551018446','sunil@amuratech.com','Maple.jpg','image/jpeg',98761,'s','m','200000','2017-02-17 06:37:19','2017-02-17 06:37:19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'sunil','Male','2017-02-21 00:00:00','ss','s','8551018446','sunil@amuratech.com','2_thumbnail_16Feb17.jpg','image/jpeg',42104,'s','l','200000','2017-02-17 07:02:10','2017-02-17 07:02:10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'sunil','Male','2017-02-23 00:00:00','s','s','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 07:36:53','2017-02-17 07:36:53',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'sunil','Male','2017-02-09 00:00:00','s','s','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 09:18:29','2017-02-17 09:18:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'sunil','Male','2017-02-21 00:00:00','ss','ss','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 09:27:44','2017-02-17 09:27:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'sunil','Male','2017-02-22 00:00:00','s','sd','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 09:43:05','2017-02-17 09:43:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'sunil','Male','2017-02-22 00:00:00','s','s','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','l','200000','2017-02-17 11:08:07','2017-02-17 11:08:07',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'sunil','Male','2017-02-16 00:00:00','ss','ss','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 11:31:00','2017-02-17 11:31:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'sunil','Male','2017-02-10 00:00:00','ss','sss','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 12:24:00','2017-02-17 12:24:00','398292615',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,'sunil','Male','2017-02-22 00:00:00','ss','dd','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 12:30:40','2017-02-17 12:37:44','829586691','ABC','12345678',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'sunil','Male','2017-02-14 00:00:00','ss','s','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 12:51:24','2017-02-17 12:51:52','665328327','Success','306003049565','1487335557410','','s','Pune','Maharashtrass','411025','India'),(12,'ss','Male','2017-02-09 00:00:00','ss','ss','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 12:54:25','2017-02-17 12:54:50','61074865','Aborted','306003049575','null','','','','','','India'),(13,'sunil','Male','2017-02-14 00:00:00','sss','ss','8551018446','sunil@amuratech.com','ccavenu.png','image/png',176491,'s','m','200000','2017-02-17 13:28:30','2017-02-17 13:29:07','394734602','Success','306003049629','1487337793855','','ss','ss','ss','411024','India');
/*!40000 ALTER TABLE `cpfl_registration_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpfl_registrations`
--

DROP TABLE IF EXISTS `cpfl_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpfl_registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `school_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_size` int(11) DEFAULT NULL,
  `kit_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `former_participant` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `former_participant_event` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `football_experience` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_problem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mediacal_app_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_year_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_dob` datetime DEFAULT NULL,
  `medical_gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_physician_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_contact_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_allergies` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_reaction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_reaaction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_hospitalization_occurred` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_allergy_medication` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_asthma` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_tetanus_injection` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_terms` tinyint(1) DEFAULT NULL,
  `medical_parent_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_print_nameapp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_relation_toapp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_father_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_father_mobileno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_father_emailid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_mother_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_mother_mobileno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_mother_emailid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_father_volunteer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_father_kit_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_mother_volunteer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_mother_kit_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emergency_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emergency_mobileno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emergency_relation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fee_structure` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `terms_condition` tinyint(1) DEFAULT NULL,
  `paren_consent` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpfl_registrations`
--

LOCK TABLES `cpfl_registrations` WRITE;
/*!40000 ALTER TABLE `cpfl_registrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpfl_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cryptos`
--

DROP TABLE IF EXISTS `cryptos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cryptos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cryptos`
--

LOCK TABLES `cryptos` WRITE;
/*!40000 ALTER TABLE `cryptos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cryptos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `published_date` date DEFAULT NULL,
  `publication` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disclaimer` text COLLATE utf8_unicode_ci,
  `info` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fcbescola_trials`
--

DROP TABLE IF EXISTS `fcbescola_trials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fcbescola_trials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `school_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `traning_venue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hear_about` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guardian_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fcbescola_trials`
--

LOCK TABLES `fcbescola_trials` WRITE;
/*!40000 ALTER TABLE `fcbescola_trials` DISABLE KEYS */;
/*!40000 ALTER TABLE `fcbescola_trials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_date` datetime DEFAULT NULL,
  `publication` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `featured` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (1,'Programa Elite - Gurgaon','','2013-01-21 10:38:00',NULL,'programa-elite-gurgaon',NULL,1,NULL,'2017-02-21 11:43:24','2017-02-21 11:43:24'),(2,'Programa Elite - Rohini','','2013-02-21 10:52:00',NULL,'programa-elite-rohini',NULL,1,NULL,'2017-02-21 11:45:39','2017-02-21 11:45:39'),(3,'Programa Elite - Asant Kunj','','2013-02-21 10:52:00',NULL,'programa-elite-asant-kunj',NULL,1,NULL,'2017-02-21 11:48:35','2017-02-21 11:48:35');
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metacontents`
--

DROP TABLE IF EXISTS `metacontents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metacontents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_titles` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_content` text COLLATE utf8_unicode_ci,
  `meta_text` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metacontents`
--

LOCK TABLES `metacontents` WRITE;
/*!40000 ALTER TABLE `metacontents` DISABLE KEYS */;
/*!40000 ALTER TABLE `metacontents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `published_date` date DEFAULT NULL,
  `publication` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disclaimer` text COLLATE utf8_unicode_ci,
  `info` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `doc_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doc_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doc_file_size` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'Media Heading','<p>test</p>','2017-01-01',NULL,'media-heading','test',NULL,'test',1,NULL,'2017-04-04 12:07:50','2017-04-04 12:07:50','Amura_Attendance_Policy_(1).pdf','application/pdf',72283),(3,'Five Goans To Live Barca Dream','<p>Timesofindia.com</p>',NULL,NULL,'five-goans-to-live-barca-dream','',NULL,'',1,NULL,'2017-04-21 11:30:03','2017-04-21 11:30:03','Untitled.jpg','image/jpeg',175569);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tagline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `overview` text COLLATE utf8_unicode_ci,
  `metakeywords` text COLLATE utf8_unicode_ci,
  `metadescription` text COLLATE utf8_unicode_ci,
  `metatitle` text COLLATE utf8_unicode_ci,
  `show_in_header` tinyint(1) DEFAULT NULL,
  `show_on_site` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `press_releases`
--

DROP TABLE IF EXISTS `press_releases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `press_releases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `short_description` text COLLATE utf8_unicode_ci,
  `featured` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `press_releases`
--

LOCK TABLES `press_releases` WRITE;
/*!40000 ALTER TABLE `press_releases` DISABLE KEYS */;
INSERT INTO `press_releases` VALUES (1,'Five Goans To Live Barca Dream','five-goans-to-live-barca-dream','','',1,1,'2017-04-04 12:11:09','2017-04-21 12:29:31','http://timesofindia.indiatimes.com/city/goa/Five-Goans-to-live-Barca-dream/articleshow/18622592.cms'),(2,'Five Goa boys selected for Barcelona journey','five-goa-boys-selected-for-barcelona-journey','','',1,1,'2017-04-21 12:33:28','2017-04-21 12:33:28','http://www.freepressjournal.in/sporty-bytes/'),(3,'Five Goa boys selected for Barcelona journey','five-goa-boys-selected-for-barcelona-journey--2','','',1,1,'2017-04-21 12:45:20','2017-04-21 12:45:20','http://www.business-standard.com/article/pti-stories/five-goa-boys-selected-for-barcelona-tournament-113022100799_1.html'),(4,'Five Goa boys selected for Barcelona journey','five-goa-boys-selected-for-barcelona-journey--3','','',1,1,'2017-04-21 12:46:12','2017-04-21 12:46:12','https://sports.ndtv.com/football/five-goa-boys-selected-for-barcelona-tournament-1540487'),(5,'Five Goa boys selected for Barcelona journey','five-goa-boys-selected-for-barcelona-journey--4','','',1,1,'2017-04-21 12:47:09','2017-04-21 12:47:09','http://zeenews.india.com/sports/football/five-goa-boys-selected-for-barcelona-tournament_756839.html');
/*!40000 ALTER TABLE `press_releases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selldocrmid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tagline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `started_at` date DEFAULT NULL,
  `completion_date` date DEFAULT NULL,
  `project_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_selling_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `availability` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clients` text COLLATE utf8_unicode_ci,
  `towers` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `area` int(11) DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `map_description` text COLLATE utf8_unicode_ci,
  `zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `neighbourhood` text COLLATE utf8_unicode_ci,
  `tags` text COLLATE utf8_unicode_ci,
  `meta_title` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `show_order` int(11) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `show_on_website` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `youth_kit_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount_pay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `redirects`
--

DROP TABLE IF EXISTS `redirects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `redirects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `old_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `redirects`
--

LOCK TABLES `redirects` WRITE;
/*!40000 ALTER TABLE `redirects` DISABLE KEYS */;
/*!40000 ALTER TABLE `redirects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resumes`
--

DROP TABLE IF EXISTS `resumes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resumes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_of_interest` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resume_file_size` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resumes`
--

LOCK TABLES `resumes` WRITE;
/*!40000 ALTER TABLE `resumes` DISABLE KEYS */;
/*!40000 ALTER TABLE `resumes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20130213093505'),('20130213114756'),('20130213114766'),('20130213114768'),('20130220121323'),('20130226073804'),('20131014111337'),('20131014144549'),('20131019092301'),('20150603061620'),('20150603061825'),('20150603061827'),('20150828073507'),('20150828073703'),('20150907055235'),('20160225064450'),('20170214103132'),('20170214111645'),('20170215070649'),('20170215135619'),('20170216095958'),('20170216120142'),('20170217100214'),('20170217135646'),('20170218073629'),('20170220114111'),('20170220114510'),('20170221093558'),('20170221094102'),('20170221103303'),('20170330140158'),('20170331120702'),('20170404072901'),('20170404113223'),('20170404132002');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `school_registrations`
--

DROP TABLE IF EXISTS `school_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_of_participation` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school_registrations`
--

LOCK TABLES `school_registrations` WRITE;
/*!40000 ALTER TABLE `school_registrations` DISABLE KEYS */;
INSERT INTO `school_registrations` VALUES (1,'MP','SSS','sonu@amuratech.com','3434343434',1,'2017-04-04 12:12:14','2017-04-04 12:12:14'),(2,'test school','delhi','test@amuratech.com','9876543210',1,'2017-04-06 20:22:15','2017-04-06 20:22:15'),(3,'atul','baner','atul.saini@amuratech.com','8984566549',3,'2017-04-07 07:19:12','2017-04-07 07:19:12'),(4,'fcb','bavdhan','atul.saini+1@amuratech.com','78767776767',1,'2017-04-07 07:19:49','2017-04-07 07:19:49'),(5,'atul','atul','atul.saini+camps@amuratech.com','8983477785',1,'2017-04-07 07:32:48','2017-04-07 07:32:48'),(6,'MP','SSS','sonu@amuratech.com','1212121212',1,'2017-04-07 09:40:04','2017-04-07 09:40:04'),(7,'MP','SSS','sonu@amuratech.com','1212121212',1,'2017-04-07 09:45:09','2017-04-07 09:45:09'),(8,'MP','SSS','sonu@amuratech.com','1212121212',1,'2017-04-07 09:45:40','2017-04-07 09:45:40'),(9,'MP','SSS','sonu@amuratech.com','1212121212',1,'2017-04-07 09:46:38','2017-04-07 09:46:38'),(10,'MP','test','sonu@amuratech.com','09898989898',1,'2017-04-20 09:54:20','2017-04-20 09:54:20'),(11,'','','','',NULL,'2017-04-28 13:41:07','2017-04-28 13:41:07'),(12,'DY Patil','Sinhgad','piyush@amuratech.com','8878697280',6,'2017-04-28 13:58:29','2017-04-28 13:58:29'),(13,'Patil','Sinhgad','piyush@amuratech.com','8878697280',3,'2017-04-28 14:03:50','2017-04-28 14:03:50');
/*!40000 ALTER TABLE `school_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_settings`
--

DROP TABLE IF EXISTS `site_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sitetitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_comments` tinyint(1) DEFAULT NULL,
  `xmlsitemap_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xmlsitemap_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xmlsitemap_file_size` int(11) DEFAULT NULL,
  `htmlsitemap_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `htmlsitemap_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `htmlsitemap_file_size` int(11) DEFAULT NULL,
  `googlemeta_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `googlemeta_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `googlemeta_file_size` int(11) DEFAULT NULL,
  `google_verification_code` text COLLATE utf8_unicode_ci,
  `google_analytics_code` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_settings`
--

LOCK TABLES `site_settings` WRITE;
/*!40000 ALTER TABLE `site_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_info` text COLLATE utf8_unicode_ci,
  `information` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `testimonial_text` text COLLATE utf8_unicode_ci,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_date` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiny_prints`
--

DROP TABLE IF EXISTS `tiny_prints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiny_prints` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiny_prints`
--

LOCK TABLES `tiny_prints` WRITE;
/*!40000 ALTER TABLE `tiny_prints` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiny_prints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiny_videos`
--

DROP TABLE IF EXISTS `tiny_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiny_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `original_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_file_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiny_videos`
--

LOCK TABLES `tiny_videos` WRITE;
/*!40000 ALTER TABLE `tiny_videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiny_videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tuples`
--

DROP TABLE IF EXISTS `tuples`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tuples` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `related_tuple_id` int(11) DEFAULT NULL,
  `related_tuple_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tuples`
--

LOCK TABLES `tuples` WRITE;
/*!40000 ALTER TABLE `tuples` DISABLE KEYS */;
/*!40000 ALTER TABLE `tuples` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin@amuratech.com','$2a$10$GA9/hWurLgOPrL5J2kVS6OS8HOHx0Wlwpwsj7jCT0svqGVGLmUSTq',NULL,NULL,NULL,31,'2017-04-29 06:32:44','2017-04-29 06:01:36','123.201.33.70','123.201.33.70','2017-02-17 11:28:57','2017-04-29 06:32:44');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `published_date` date DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` VALUES (1,'FCBCamp Pune November - News on DD Sahyadri','https://www.youtube.com/embed/eET612s9Afw',1,'2012-11-28','Conscient Football','2017-04-04 12:08:53','2017-04-21 12:43:01'),(2,'News snippets of FCBEscola Delhi','https://www.youtube.com/embed/9vdjpA-hzCQ',1,'2012-05-30','Conscient Football','2017-04-21 11:37:30','2017-04-21 12:43:53');
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-29 10:18:16
