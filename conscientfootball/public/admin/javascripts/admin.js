// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults
// application.js
function remove_fields(link) {
 jQuery(link).previous("input[type=hidden]").value = "1";
 jQuery(link).up(".fields").hide();
}

function add_fields(link, association, content) {
 var new_id = new Date().getTime();
 var regexp = new RegExp("new_" + association, "g")
 jQuery(link).parent().get(0).insert({
   before: content.replace(regexp, new_id)
 });
}

// application_jquery.js
function remove_fields(link) {
 jQuery(link).closest(".fields").remove();
}

function add_fields_with_callback(link, association, content, callback) {
 var new_id = new Date().getTime();
 var regexp = new RegExp("new_" + association, "g")
 jQuery(link).parent().get(0).insert({
   before: content.replace(regexp, new_id)
 });
 eval(callback);
}
