// -------------------------
// Multiple File Upload
// -------------------------
function MultiSelector(list_target, asset_type, max) {
   max = typeof(max) != 'undefined' ? max : 100000;
  this.list_target = list_target;
  this.asset_type = asset_type;
  this.count = 0;
  this.id = 0;
  if( max ){
  	this.max = max;
  }else{
  	this.max = -1;
  };

  
  this.addElement = function( element, element_title, element_description){
  		if( element.tagName == 'INPUT' && element.type == 'file' ){
  				element.name = asset_type + '_attachment[file_' + (this.id) + ']';
				 element_title.name = asset_type + '_title[title_' + (this.id) + ']';
				 element_description.name = asset_type + '_description[description_' + (this.id) + ']';
  				this.id++;
  				element.multi_selector = this;
  				element.multi_selector.element_title = element_title;
  				element.multi_selector.element_description = element_description;
  				element.onchange = function(){
  					if(typeof(element_title) == 'undefined' || element_title.value == ""){
  						alert("Please enter a Display Name for the Asset.");
  						element.value = "";
  						return;
  					}
  					element.multi_selector.element_title.style.display = 'none';
  					element.multi_selector.element_description.style.display = 'none';
            
            var desc_span =  this.parentNode.id.replace("complex", "desc");
            var complex_span = this.parentNode.id;
            
          jQuery("#" + desc_span).before("<span id='span_" + asset_type + this.multi_selector.count + "_desc'><textarea rows='4' id='" + asset_type + '_data_description' + this.multi_selector.id + "' cols='40'></textarea></span>");
          
          jQuery("#" + complex_span).before("<span id='span_" + asset_type + this.multi_selector.count + "_complex'><input id='" + asset_type + '_data_title' + this.multi_selector.id + "' type='text' /><input id='" + asset_type + '_data' + this.multi_selector.id + "' type='file' /></span>");
          
          var new_element_description = document.getElementById(asset_type + '_data_description' + this.multi_selector.id);
          var new_element_title =  document.getElementById(asset_type + '_data_title' + this.multi_selector.id);
          var new_element  =  document.getElementById(asset_type + '_data' + this.multi_selector.id);
          
          
					this.multi_selector.addElement( new_element, new_element_title, new_element_description, asset_type);
					this.multi_selector.addListRow( this , this.multi_selector.element_title, this.multi_selector.element_description);
					this.style.display = 'none';
					//this.style.left = '-1000px';
				};

				if( this.max != -1 && this.count >= this.max ){
					element.disabled = true;
					element_title.disabled = true;
					element_description.disabled = true;
				}
				this.count++;
				this.current_element = element;
				this.current_element_title = element_title;
				this.current_element_description = element_description;
		} else {
			alert( 'Error: not a file input element' );
		};
	};
	this.addListRow = function( element , element_title, element_description){
		var new_row = document.createElement('li');
		var new_row_button = document.createElement( 'a' );
		new_row_button.setAttribute('id', 'remove');
		new_row_button.title = 'Remove This Attachment';
		new_row_button.href = '#';
		new_row_button.innerHTML = 'Remove';
		new_row.element = element;
		new_row_button.onclick= function(){
				this.parentNode.element.parentNode.removeChild( this.parentNode.element );
				this.parentNode.parentNode.removeChild( this.parentNode );
				this.parentNode.element.multi_selector.count--;
				this.parentNode.element.multi_selector.current_element.disabled = false;
				this.parentNode.element.multi_selector.current_element_title.disabled = false;
				this.parentNode.element.multi_selector.current_element_description.disabled = false;
				return false;
		};
		var html = element_title.value;
		var html = element_description.value;
		html += element.value.split('/')[element.value.split('/').length - 1];
    html += "<br />"
		new_row.innerHTML = html;
		new_row.appendChild( new_row_button );
		this.list_target.appendChild( new_row );
	};
}
