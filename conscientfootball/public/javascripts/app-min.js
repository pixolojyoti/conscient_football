$(document).ready(function() {
    function e(e, i, t) {
        for (var a = 0; a < e.length; a++) e[a].classList[t](i)
    }
    var i = $(window).height();
    $(".homepageWrapper,.pageLoader, .fcbWrapper, .faqWrapper, .campsregisterWrapper, .faqbgWrapper, .delhiWrapper, .mumbaiWrapper, .campScheduleWrapper, .upcomingEventsWrapper").height(i), setTimeout(function() {
        $(".pageLoader").fadeOut()
    }, 2e3), $(".menuWrapper .has-submenu").click(function() {
        $(".sub-menu").slideToggle()
    }), $(".open-popup-form").colorbox({
        inline: !0,
        maxWidth: "90%",
        transition: "fade",
        width: "600px",
        height: "360px"
    }), $(".google-map-popup").colorbox({
        inline: !0,
        maxWidth: "90%",
        width: "60%",
        height: "60%",
        transition: "fade"
    });
    var i = $(window).height(),
        t = i / 2;
    $(".cpfl-wrapper, .qatarWrapper, .full-height").height(i), $(".ourprogramWrapper, .ourprogramFirst, .ourprogramSecond, .ourprogramThird, .trainingFirst, .trainingSecond, .fcbescola-bg, .fcbclinics-bg, .half-height").height(t), $(".minHeight").css({
        "min-height": "700px"
    }), $(".highlights-tabs a").click(function() {
        var e = $(this).attr("rel");
        $(".highlights-content").hide(), $("#" + e).fadeIn(), $(".highlights-tabs a").removeClass("highlights-btn-active"), $(this).addClass("highlights-btn-active")
    }), $(".explore-btn").click(function() {
        $("html, body").animate({
            scrollTop: $("#programs").offset().top
        }, 1e3)
    }), $(".menuWrapper-mobile li a").click(function() {
        var e = $(this).attr("rel");
        $(".menuWrapper-mobile li a").removeClass("current"), $(this).addClass("current"), $("html, body").animate({
            scrollTop: $("#" + e).offset().top
        }, 1e3)
    }), $(".close-btn").click(function() {
        $(".coach-info-details").fadeOut()
    }), $(".escola-content a").click(function() {
        var e = $(this).attr("rel");
        $(".coach-info-details").hide(), $("#" + e).fadeIn()
    }), $(window).width() >= 700 ? $(".virtual-number").click(function() {
        "-300px" === $(".virtual-number").css("right") ? $(".virtual-number").css("right", "0px") : $(".virtual-number").css("right", "-300px")
    }) : $(".virtual-number").click(function() {
        "-275px" === $(".virtual-number").css("right") ? $(".virtual-number").css("right", "0px") : $(".virtual-number").css("right", "-275px")
    }), $(".map-buttons a").click(function() {
        var e = $(this).attr("rel"),
            i = $(this).attr("data-map");
        $(".map-buttons a").removeClass("active-map-button"), $(".map").hide(), $(".contactWrapper").hide(), $(this).addClass("active-map-button"), $("#" + e).fadeIn(), $("#" + i).fadeIn()
    }), $(".mobile-menu").click(function() {
        $(".menuWrapper-mobile").slideToggle()
    }), $(".video-popup").colorbox({
        iframe: !0,
        innerWidth: 640,
        innerHeight: 390,
        maxWidth: "90%",
        maxHeight: "90%"
    });
    var a = $(window).height(),
        t = a / 2;
    $(".homePageSecondTop,.homePageSecondBottom,.homePageThirdTop").height(t), $(".homePageFirst, .homePageSecond, .homePageThird, .window-height").css("height", a), $(".homePageThirdTop").css({
        top: t
    });
     
    $(".bxslider").bxSlider({
        auto: !1,
        pager: !0,
        controls: !0,
        pause: 3e3,
        nextSelector: "#home-slider-next",
        prevSelector: "#home-slider-prev",
        nextText: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        prevText: '<i class="fa fa-angle-left" aria-hidden="true"></i>'
    });
    var i = $(window).height();
    $(".slide").height(i), $("a.scroll-btn").click(function() {
        $("html, body").animate({
            scrollTop: $("#section-one").offset().top
        }, 1e3)
    }), $(".gallery-slider").bxSlider({
        auto: !0,
        pager: !1,
        controls: !1,
        pause: 3e3,
        mode: "fade"
    }), $(".media-tabs li a").click(function(e) {
        var i = $(this).attr("rel");
        $(".albumWrapper").hide(), $(".media-tabs li a").removeClass("active-media"), $(this).addClass("active-media"), $("#" + i).fadeIn()
    }), $(".group3").colorbox({
        rel: "group3",
        transition: "fade",
        height: "80%"
    });
    for (var r = document.getElementsByClassName("accordion"), o = document.getElementsByClassName("panel"), n = 0; n < r.length; n++) r[n].onclick = function() {
        var i = !this.classList.contains("active");
        e(r, "active", "remove"), e(o, "show", "remove"), i && (this.classList.toggle("active"), this.nextElementSibling.classList.toggle("show"))
    }
});