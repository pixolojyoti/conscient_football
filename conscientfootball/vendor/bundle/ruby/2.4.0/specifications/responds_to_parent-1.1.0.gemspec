# -*- encoding: utf-8 -*-
# stub: responds_to_parent 1.1.0 ruby lib

Gem::Specification.new do |s|
  s.name = "responds_to_parent".freeze
  s.version = "1.1.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Mark Catley".freeze]
  s.date = "2012-05-09"
  s.homepage = "http://github.com/markcatley/responds_to_parent".freeze
  s.rubygems_version = "2.7.8".freeze
  s.summary = "[Rails] Adds 'responds_to_parent' to your controller torespond to the parent document of your page.Make Ajaxy file uploads by posting the form to a hiddeniframe, and respond with RJS to the parent window.".freeze

  s.installed_by_version = "2.7.8" if s.respond_to? :installed_by_version
end
