require 'test_helper'

class CryptosControllerTest < ActionController::TestCase
  setup do
    @crypto = cryptos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cryptos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create crypto" do
    assert_difference('Crypto.count') do
      post :create, :crypto => @crypto.attributes
    end

    assert_redirected_to crypto_path(assigns(:crypto))
  end

  test "should show crypto" do
    get :show, :id => @crypto.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @crypto.to_param
    assert_response :success
  end

  test "should update crypto" do
    put :update, :id => @crypto.to_param, :crypto => @crypto.attributes
    assert_redirected_to crypto_path(assigns(:crypto))
  end

  test "should destroy crypto" do
    assert_difference('Crypto.count', -1) do
      delete :destroy, :id => @crypto.to_param
    end

    assert_redirected_to cryptos_path
  end
end
