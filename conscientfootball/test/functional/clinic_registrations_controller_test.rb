require 'test_helper'

class ClinicRegistrationsControllerTest < ActionController::TestCase
  setup do
    @clinic_registration = clinic_registrations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:clinic_registrations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create clinic_registration" do
    assert_difference('ClinicRegistration.count') do
      post :create, :clinic_registration => @clinic_registration.attributes
    end

    assert_redirected_to clinic_registration_path(assigns(:clinic_registration))
  end

  test "should show clinic_registration" do
    get :show, :id => @clinic_registration.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @clinic_registration.to_param
    assert_response :success
  end

  test "should update clinic_registration" do
    put :update, :id => @clinic_registration.to_param, :clinic_registration => @clinic_registration.attributes
    assert_redirected_to clinic_registration_path(assigns(:clinic_registration))
  end

  test "should destroy clinic_registration" do
    assert_difference('ClinicRegistration.count', -1) do
      delete :destroy, :id => @clinic_registration.to_param
    end

    assert_redirected_to clinic_registrations_path
  end
end
