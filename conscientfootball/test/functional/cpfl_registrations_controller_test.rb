require 'test_helper'

class CpflRegistrationsControllerTest < ActionController::TestCase
  setup do
    @cpfl_registration = cpfl_registrations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cpfl_registrations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cpfl_registration" do
    assert_difference('CpflRegistration.count') do
      post :create, :cpfl_registration => @cpfl_registration.attributes
    end

    assert_redirected_to cpfl_registration_path(assigns(:cpfl_registration))
  end

  test "should show cpfl_registration" do
    get :show, :id => @cpfl_registration.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @cpfl_registration.to_param
    assert_response :success
  end

  test "should update cpfl_registration" do
    put :update, :id => @cpfl_registration.to_param, :cpfl_registration => @cpfl_registration.attributes
    assert_redirected_to cpfl_registration_path(assigns(:cpfl_registration))
  end

  test "should destroy cpfl_registration" do
    assert_difference('CpflRegistration.count', -1) do
      delete :destroy, :id => @cpfl_registration.to_param
    end

    assert_redirected_to cpfl_registrations_path
  end
end
