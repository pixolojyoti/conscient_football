require 'test_helper'

class SchoolRegistrationsControllerTest < ActionController::TestCase
  setup do
    @school_registration = school_registrations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:school_registrations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create school_registration" do
    assert_difference('SchoolRegistration.count') do
      post :create, :school_registration => @school_registration.attributes
    end

    assert_redirected_to school_registration_path(assigns(:school_registration))
  end

  test "should show school_registration" do
    get :show, :id => @school_registration.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @school_registration.to_param
    assert_response :success
  end

  test "should update school_registration" do
    put :update, :id => @school_registration.to_param, :school_registration => @school_registration.attributes
    assert_redirected_to school_registration_path(assigns(:school_registration))
  end

  test "should destroy school_registration" do
    assert_difference('SchoolRegistration.count', -1) do
      delete :destroy, :id => @school_registration.to_param
    end

    assert_redirected_to school_registrations_path
  end
end
