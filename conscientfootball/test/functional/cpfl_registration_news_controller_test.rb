require 'test_helper'

class CpflRegistrationNewsControllerTest < ActionController::TestCase
  setup do
    @cpfl_registration_news = cpfl_registration_news(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cpfl_registration_news)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cpfl_registration_news" do
    assert_difference('CpflRegistrationNew.count') do
      post :create, :cpfl_registration_news => @cpfl_registration_news.attributes
    end

    assert_redirected_to cpfl_registration_news_path(assigns(:cpfl_registration_news))
  end

  test "should show cpfl_registration_news" do
    get :show, :id => @cpfl_registration_news.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @cpfl_registration_news.to_param
    assert_response :success
  end

  test "should update cpfl_registration_news" do
    put :update, :id => @cpfl_registration_news.to_param, :cpfl_registration_news => @cpfl_registration_news.attributes
    assert_redirected_to cpfl_registration_news_path(assigns(:cpfl_registration_news))
  end

  test "should destroy cpfl_registration_news" do
    assert_difference('CpflRegistrationNew.count', -1) do
      delete :destroy, :id => @cpfl_registration_news.to_param
    end

    assert_redirected_to cpfl_registration_news_path
  end
end
