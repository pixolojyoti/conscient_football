require 'test_helper'

class FacebookContactsControllerTest < ActionController::TestCase
  setup do
    @facebook_contact = facebook_contacts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:facebook_contacts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create facebook_contact" do
    assert_difference('FacebookContact.count') do
      post :create, :facebook_contact => @facebook_contact.attributes
    end

    assert_redirected_to facebook_contact_path(assigns(:facebook_contact))
  end

  test "should show facebook_contact" do
    get :show, :id => @facebook_contact.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @facebook_contact.to_param
    assert_response :success
  end

  test "should update facebook_contact" do
    put :update, :id => @facebook_contact.to_param, :facebook_contact => @facebook_contact.attributes
    assert_redirected_to facebook_contact_path(assigns(:facebook_contact))
  end

  test "should destroy facebook_contact" do
    assert_difference('FacebookContact.count', -1) do
      delete :destroy, :id => @facebook_contact.to_param
    end

    assert_redirected_to facebook_contacts_path
  end
end
