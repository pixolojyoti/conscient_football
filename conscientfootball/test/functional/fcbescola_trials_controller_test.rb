require 'test_helper'

class FcbescolaTrialsControllerTest < ActionController::TestCase
  setup do
    @fcbescola_trial = fcbescola_trials(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fcbescola_trials)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fcbescola_trial" do
    assert_difference('FcbescolaTrial.count') do
      post :create, :fcbescola_trial => @fcbescola_trial.attributes
    end

    assert_redirected_to fcbescola_trial_path(assigns(:fcbescola_trial))
  end

  test "should show fcbescola_trial" do
    get :show, :id => @fcbescola_trial.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @fcbescola_trial.to_param
    assert_response :success
  end

  test "should update fcbescola_trial" do
    put :update, :id => @fcbescola_trial.to_param, :fcbescola_trial => @fcbescola_trial.attributes
    assert_redirected_to fcbescola_trial_path(assigns(:fcbescola_trial))
  end

  test "should destroy fcbescola_trial" do
    assert_difference('FcbescolaTrial.count', -1) do
      delete :destroy, :id => @fcbescola_trial.to_param
    end

    assert_redirected_to fcbescola_trials_path
  end
end
