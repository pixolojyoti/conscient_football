class AddColumnToPressRelease < ActiveRecord::Migration
  def self.up
    add_column :press_releases, :show_order, :integer
  end

  def self.down
    remove_column :press_releases, :show_order
  end
end
