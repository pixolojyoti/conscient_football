class AddColumnToNews < ActiveRecord::Migration
  def self.up
    add_column :news, :show_order, :integer
  end

  def self.down
    remove_column :news, :show_order
  end
end
