class AddColsToNews < ActiveRecord::Migration
 def self.up
  	   add_column :news, :doc_file_name, :string
  	   add_column :news, :doc_content_type, :string
  	   add_column :news, :doc_file_size, :integer
  end

  def self.down

  	remove_column :news, :doc_file_name
	remove_column :news, :doc_content_type
	remove_column :news, :doc_file_size
end
end
