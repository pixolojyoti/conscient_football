class CreatePages < ActiveRecord::Migration
  def self.up
    create_table :pages do |t|
      t.integer :parent_id
      t.string :title
      t.string :slug
      t.string :tagline
      t.text :overview
      t.text :metakeywords
      t.text :metadescription
      t.text :metatitle
      t.boolean :show_in_header
      t.boolean :show_on_site

      t.timestamps
    end
  end

  def self.down
    drop_table :pages
  end
end
