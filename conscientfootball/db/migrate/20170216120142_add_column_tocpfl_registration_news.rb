class AddColumnTocpflRegistrationNews < ActiveRecord::Migration
  def self.up
  add_column :cpfl_registration_news, :order_id, :string
  end

  def self.down
  remove_column :cpfl_registration_news, :order_id
  end
end
