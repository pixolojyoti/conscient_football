class CreateContacts < ActiveRecord::Migration
  def self.up
    create_table :contacts do |t|
      t.string :name
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :city
      t.string :state
      t.string :country
      t.string :pin
      t.string :area_code
      t.string :phone
      t.string :mobile
      t.string :fax
      t.string :email
      t.string :project
      t.string :mailer
      t.string :campaign
      t.string :occupation
      t.string :profession
      t.string :service_area
      t.string :project
      t.string :website
      t.string :organization
      t.string :designation
      t.string :enquiry_about
      t.string :entity_type
      t.string :entity_id
      t.string :page_url
      t.string :query
      t.string :hidden_data
      t.string :department
      t.text :comments
 
      t.timestamps
    end
  end

  def self.down
    drop_table :contacts
  end
end
