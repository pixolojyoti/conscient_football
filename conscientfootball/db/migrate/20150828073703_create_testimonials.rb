class CreateTestimonials < ActiveRecord::Migration
  def self.up
    create_table :testimonials do |t|
      t.string :name
      t.text :testimonial_text
      t.string :designation
      t.date :published_date
      t.boolean :status

      t.timestamps
    end
  end

  def self.down
    drop_table :testimonials
  end
end
