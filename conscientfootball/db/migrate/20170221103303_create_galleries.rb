class CreateGalleries < ActiveRecord::Migration
  def self.up
    create_table :galleries do |t|
      t.string :title
      t.string :description
      t.datetime :published_date
      t.string :publication
      t.string :slug
      t.string :info
      t.boolean :status
      t.string :featured

      t.timestamps
    end
  end

  def self.down
    drop_table :galleries
  end
end
