class CreateFcbescolaTrials < ActiveRecord::Migration
  def self.up
    create_table :fcbescola_trials do |t|
      t.string :first_name
      t.string :last_name
      t.datetime :dob
      t.string :school_name
      t.string :home_phone
      t.string :mobile_no
      t.string :email
      t.string :traning_venue
      t.string :hear_about
      t.string :guardian_first_name
      t.string :guardian_last_name
      t.string :guardian_mobile
      t.string :guardian_email

      t.timestamps
    end
  end

  def self.down
    drop_table :fcbescola_trials
  end
end
