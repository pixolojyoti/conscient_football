class AddColoumnToSchoolRegistrations < ActiveRecord::Migration
  def self.up
    add_column :school_registrations, :enquire_type, :string
  end

  def self.down
    remove_column :school_registrations, :enquire_type
  end
end
