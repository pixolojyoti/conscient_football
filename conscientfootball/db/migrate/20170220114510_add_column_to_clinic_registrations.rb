class AddColumnToClinicRegistrations < ActiveRecord::Migration
  def self.up
    add_column :clinic_registrations, :youth_kit_size, :string
    add_column :clinic_registrations, :amount_pay, :string
  end

  def self.down
    remove_column :clinic_registrations, :amount_pay
    remove_column :clinic_registrations, :youth_kit_size
  end
end
