class AddColumnToCpflRegistrationNew < ActiveRecord::Migration
  def self.up
    add_column :cpfl_registration_news, :order_status, :string
    add_column :cpfl_registration_news, :tracking_id, :string
    add_column :cpfl_registration_news, :bank_ref_no, :string
    add_column :cpfl_registration_news, :failure_message, :string
    add_column :cpfl_registration_news, :billing_address, :string
    add_column :cpfl_registration_news, :billing_city, :string
    add_column :cpfl_registration_news, :billing_state, :string
    add_column :cpfl_registration_news, :billing_zip, :string
    add_column :cpfl_registration_news, :billing_country, :string
  end

  def self.down
    remove_column :cpfl_registration_news, :billing_country
    remove_column :cpfl_registration_news, :billing_zip
    remove_column :cpfl_registration_news, :billing_state
    remove_column :cpfl_registration_news, :billing_city
    remove_column :cpfl_registration_news, :billing_address
    remove_column :cpfl_registration_news, :failure_message
    remove_column :cpfl_registration_news, :bank_ref_no
    remove_column :cpfl_registration_news, :tracking_id
    remove_column :cpfl_registration_news, :order_status
  end
end
