class CreateTuples < ActiveRecord::Migration
  def self.up
    create_table :tuples do |t|
      t.string :key
      t.text :value
      t.integer :related_tuple_id
      t.string :related_tuple_type

      t.timestamps
    end
  end

  def self.down
    drop_table :tuples
  end
end
