class CreateCpflRegistrationNews < ActiveRecord::Migration
  def self.up
    create_table :cpfl_registration_news do |t|
      t.string :name
      t.string :gender
      t.datetime :dob
      t.string :school_name
      t.string :standard
      t.string :mobile_no
      t.string :email
      t.string :data_file_name
      t.string :data_content_type
      t.integer :data_file_size
      t.string :youth_kit_size
      t.string :adult_kit_size
      t.string :amount_pay

      t.timestamps
    end
  end

  def self.down
    drop_table :cpfl_registration_news
  end
end
