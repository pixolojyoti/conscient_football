class CreateEvents < ActiveRecord::Migration
  def self.up
    create_table :events do |t|
      t.string :title
      t.text :description
      t.date :published_date
      t.string :publication
      t.string :slug
      t.string :author
      t.text :disclaimer
      t.text :info
      t.boolean :status
      t.boolean :featured

      t.timestamps
    end
  end

  def self.down
    drop_table :events
  end
end
