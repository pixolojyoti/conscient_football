class CreateVideos < ActiveRecord::Migration
  def self.up
    create_table :videos do |t|
      t.string :title
      t.text :url
      t.boolean :status
      t.date :published_date
      t.string :author

      t.timestamps
    end
  end

  def self.down
    drop_table :videos
  end
end
