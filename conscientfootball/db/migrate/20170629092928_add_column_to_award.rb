class AddColumnToAward < ActiveRecord::Migration
  def self.up
    add_column :awards, :show_order, :integer
  end

  def self.down
    remove_column :awards, :show_order
  end
end
