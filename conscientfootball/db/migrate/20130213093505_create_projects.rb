class CreateProjects < ActiveRecord::Migration
  def self.up
    create_table :projects do |t|
      t.string :name
      t.string :selldocrmid
      t.string :slug
      t.string :tagline
      t.string :short_description
      t.text :description
      t.date :started_at
      t.date :completion_date
      t.string :project_type
      t.string :project_status 
      t.string :project_category
      t.string :project_selling_type
      t.string :project_size
      t.string :availability
      t.text :clients
      t.string :towers
      t.text :address
      t.integer :area
      t.string :city
      t.string :state
      t.string :country
      t.string :latitude
      t.string :longitude
      t.text :map_description
      t.string :zip
      t.text :neighbourhood
      t.text :tags
      t.text :meta_title
      t.text :meta_description
      t.text :meta_keywords
      t.integer :show_order
      t.boolean :featured
      t.boolean :show_on_website
      t.timestamps
    end
  end

  def self.down
    drop_table :projects
  end
end
