class AddcategoryToTestimonial < ActiveRecord::Migration
  def self.up
add_column :testimonials, :category, :string
end
  def self.down
  	drop_column :testimonials, :category
  end
end
