class AddPreferredCenterToSchoolRegistrations < ActiveRecord::Migration
  def self.up
    add_column :school_registrations, :preferred_center, :string
  end

  def self.down
    remove_column :school_registrations, :preferred_center
  end
end
