class CreateFacilities < ActiveRecord::Migration
  def self.up
    create_table :facilities do |t|
      t.string :name
      t.string :mobile
      t.string :email
      t.string :phone
      t.string :location
      t.string :sport
      t.string :venue
      t.string :time
      t.text :comment

      t.timestamps
    end
  end

  def self.down
    drop_table :facilities
  end
end
