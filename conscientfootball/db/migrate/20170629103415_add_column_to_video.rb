class AddColumnToVideo < ActiveRecord::Migration
  def self.up
    add_column :videos, :show_order, :integer
  end

  def self.down
    remove_column :videos, :show_order
  end
end
