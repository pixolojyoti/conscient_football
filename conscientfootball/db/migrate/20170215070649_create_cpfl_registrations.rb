class CreateCpflRegistrations < ActiveRecord::Migration
  def self.up
    create_table :cpfl_registrations do |t|
      t.string :name
      t.string :gender
      t.datetime :dob
      t.string :school_name
      t.string :class
      t.string :mobile_no
      t.string :email
      t.string :data_file_name
      t.string :data_content_type
      t.integer :data_file_size
      t.string :kit_size
      t.string :former_participant
      t.string :former_participant_event
      t.string :football_experience
      t.string :medical_problem
      t.string :mediacal_app_name
      t.string :medical_year_class
      t.datetime :medical_dob
      t.string :medical_gender
      t.string :medical_address
      t.string :medical_physician_name
      t.string :medical_contact_no
      t.string :medical_allergies
      t.string :medical_reaction
      t.string :medical_reaaction
      t.string :medical_hospitalization_occurred
      t.string :medical_allergy_medication
      t.string :medical_asthma
      t.string :medical_tetanus_injection
      t.boolean :medical_terms
      t.string :medical_parent_name
      t.string :medical_print_nameapp
      t.string :medical_relation_toapp
      t.string :parent_father_name
      t.string :parent_father_mobileno
      t.string :parent_father_emailid
      t.string :parent_mother_name
      t.string :parent_mother_mobileno
      t.string :parent_mother_emailid
      t.string :parent_address
      t.string :parent_father_volunteer
      t.string :parent_father_kit_size
      t.string :parent_mother_volunteer
      t.string :parent_mother_kit_size
      t.string :emergency_name
      t.string :emergency_mobileno
      t.string :emergency_relation
      t.string :location
      t.string :fee_structure
      t.boolean :terms_condition
      t.boolean :paren_consent

      t.timestamps
    end
  end

  def self.down
    drop_table :cpfl_registrations
  end
end
