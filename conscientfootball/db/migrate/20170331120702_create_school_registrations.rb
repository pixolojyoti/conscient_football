class CreateSchoolRegistrations < ActiveRecord::Migration
  def self.up
    create_table :school_registrations do |t|
      t.string :school_name
      t.text :address
      t.string :email
      t.string :mobile
      t.integer :no_of_participation

      t.timestamps
    end
  end

  def self.down
    drop_table :school_registrations
  end
end
