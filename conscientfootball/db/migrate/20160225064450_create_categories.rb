class CreateCategories < ActiveRecord::Migration
  def self.up
    create_table :categories do |t|
      t.string :name
      t.text :description
      t.string :category_group
      t.string :category_type
      t.integer :category_group_id
      t.boolean :status

      t.timestamps
    end
  end

  def self.down
    drop_table :categories
  end
end
