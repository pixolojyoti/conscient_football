class CreateAwards < ActiveRecord::Migration
  def self.up
    create_table :awards do |t|
      t.string :title
      t.text :short_description
      t.text :description
      t.boolean :featured
      t.boolean :status

      t.timestamps
    end
  end

  def self.down
    drop_table :awards
  end
end
