class AddColumnToGalleries < ActiveRecord::Migration
  def self.up
    add_column :galleries, :published_year, :string
  end

  def self.down
    remove_column :galleries, :published_year
  end
end
