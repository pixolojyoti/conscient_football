class AddColumnToCreateFcbescolaTrials < ActiveRecord::Migration
  def self.up
    add_column :fcbescola_trials, :city, :string
  end

  def self.down
    remove_column :fcbescola_trials, :city
  end
end
