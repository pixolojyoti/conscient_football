class CreateTeams < ActiveRecord::Migration
  def self.up
    create_table :teams do |t|
      t.string :name
      t.string :designation
      t.text :short_info
      t.text :information
      t.boolean :status

      t.timestamps
    end
  end

  def self.down
    drop_table :teams
  end
end
