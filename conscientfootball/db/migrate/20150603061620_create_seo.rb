class CreateSeo < ActiveRecord::Migration
  def self.up
    
    create_table :redirects do |t|
      t.string :old_uri
      t.string :new_uri

      t.timestamps
    end
  
   create_table :blogs do |t|
      t.string :title
      t.text :description
      t.text :short_description
      t.string :slug
      t.boolean :status
      t.date :published_on
      t.string :category
      t.string :metatitle
      t.text :metakeyword
      t.text :metadescription
      t.boolean :featured
      t.integer :counter, :default => 0
      t.timestamps
    end
   
    create_table :metacontents do |t|
      t.string :url
      t.text :meta_titles
      t.text :meta_keywords
      t.text :meta_description
      t.text :meta_content
      t.text :meta_text

      t.timestamps
    end
   
   create_table :tiny_prints do |t|
	#(Includes the file with image content)
         t.string    :image_file_name          #Uploading file/image
         t.string    :image_file_size
         t.string    :image_content_type
         t.timestamps
    end
  
    create_table :tiny_videos do |t|
	#(Includes the file with original content)
         t.string    :original_file_name          #Uploading file/image
         t.string    :original_file_size
         t.string    :original_content_type
         t.timestamps
    end
   
   create_table :comments do |t|
      t.string :name
      t.string :email
      t.text :comment
      t.integer :blog_id
      t.boolean :approved

      t.timestamps
    end
   
   
    create_table :site_settings do |t|
	t.string :sitetitle
	t.string :phone
	t.string :email
        t.boolean :show_comments
	t.string :xmlsitemap_file_name
	t.string :xmlsitemap_content_type
	t.integer :xmlsitemap_file_size
	t.string :htmlsitemap_file_name
	t.string :htmlsitemap_content_type
	t.integer :htmlsitemap_file_size
	t.string :googlemeta_file_name
	t.string :googlemeta_content_type
	t.integer :googlemeta_file_size
	t.text :google_verification_code
	t.text :google_analytics_code

      t.timestamps
    end
   
   
   
  end

  def self.down
    drop_table :redirects
    drop_table :blogs
    drop_table :metacontents
    drop_table :tiny_videos
    drop_table :tiny_prints
    drop_table :site_settings
    
  end
end