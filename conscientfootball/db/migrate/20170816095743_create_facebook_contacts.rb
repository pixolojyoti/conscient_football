class CreateFacebookContacts < ActiveRecord::Migration
  def self.up
    create_table :facebook_contacts do |t|
      t.string :fullname
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :guardians_phone
      t.string :guardians_email
      t.string :gender
      t.string :date_of_birth
      t.string :city
      t.string :preferred_center
      t.string :school_name
      t.string :utm_source
      t.string :utm_campaign
      t.string :utm_medium
      t.string :utm_term
      t.string :utm_content
      t.string :srd
      t.string :project
      t.string :url
      t.string :device
      t.string :sibling_name
      t.string :sibling_number
      t.text :comments

      t.timestamps
    end
  end

  def self.down
    drop_table :facebook_contacts
  end
end
