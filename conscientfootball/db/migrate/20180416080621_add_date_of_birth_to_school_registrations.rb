class AddDateOfBirthToSchoolRegistrations < ActiveRecord::Migration
  def self.up
    add_column :school_registrations, :date_of_birth, :string
  end

  def self.down
    remove_column :school_registrations, :date_of_birth
  end
end
