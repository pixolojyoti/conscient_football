class AddColumnToProject < ActiveRecord::Migration
  def self.up
    add_column :projects, :youth_kit_size, :string
    add_column :projects, :amount_pay, :string
  end

  def self.down
    remove_column :projects, :amount_pay
    remove_column :projects, :youth_kit_size
  end
end
