class AddColumnToclinicRegistrations < ActiveRecord::Migration
  def self.up
  	add_column :clinic_registrations, :data_file_name, :string
  	add_column :clinic_registrations, :data_content_type, :string
  	add_column :clinic_registrations, :data_file_size, :integer
  end

  def self.down
  	remove_column :clinic_registrations, :data_file_name
  	remove_column :clinic_registrations, :data_content_type
  	remove_column :clinic_registrations, :data_file_size
  end
end