class CreateResumes < ActiveRecord::Migration
  def self.up
    create_table :resumes do |t|
      t.string :name
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :gender
      t.string :phone
      t.string :mobile
      t.text :address
      t.string :position
      t.string :area_of_interest
      t.string :resume_file_name
      t.string :resume_content_type
      t.integer :resume_file_size

      t.timestamps
    end
  end

  def self.down
    drop_table :resumes
  end
end
