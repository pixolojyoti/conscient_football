class CreateCareers < ActiveRecord::Migration
  def self.up
    create_table :careers do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :resume_file_name
      t.string :resume_content_type
      t.integer :resume_file_size
    
      t.timestamps
    end
  end

  def self.down
    drop_table :careers
  end
end
