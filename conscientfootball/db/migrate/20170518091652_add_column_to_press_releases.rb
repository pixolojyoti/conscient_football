class AddColumnToPressReleases < ActiveRecord::Migration
  def self.up
    add_column :press_releases, :published_date, :date
  end

  def self.down
    remove_column :press_releases, :published_date
  end
end
