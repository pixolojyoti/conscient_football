class AddColumnToAwards < ActiveRecord::Migration
  def self.up
    add_column :awards, :slug, :string
  end

  def self.down
    remove_column :awards, :slug
  end
end
