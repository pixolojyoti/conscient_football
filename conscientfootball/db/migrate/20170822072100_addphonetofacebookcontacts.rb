class Addphonetofacebookcontacts < ActiveRecord::Migration
  def self.up
  	   add_column :facebook_contacts, :phone, :string
  	   add_column :facebook_contacts, :form_id, :string
  end

  def self.down
  	   remove_column :remove_contacts, :phone
  	   remove_column :remove_contacts, :form_id
  end
end
