# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
   User.create(:email => 'admin@amuratech.com', :password => "admin@!23")
   Blog.create(:title => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', :description => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tum ille timide vel potius verecunde: Facio, inquit. Nam memini etiam quae nolo, oblivisci non possum Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tum ille timide vel potius verecunde: Facio, inquit. Nam memini etiam quae nolo, oblivisci non possum", :short_description => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tum ille timide vel potius verecunde: Facio, inquit. Nam memini etiam quae nolo, oblivisci non possum Lorem ipsum dolor sit amet",  :slug => "lorem-ipsum-dolor-sit-amet", :published_on => Date.today, :status => true, :featured => true ,:category => "uncategorized")

