# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20180416080630) do

  create_table "assets", :force => true do |t|
    t.string   "data_file_name"
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "group_id"
    t.string   "name"
    t.string   "alt_text"
    t.text     "description"
    t.integer  "asset_order"
    t.integer  "attachable_id"
    t.string   "attachable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "awards", :force => true do |t|
    t.string   "title"
    t.text     "short_description"
    t.text     "description"
    t.boolean  "featured"
    t.boolean  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.datetime "published_date"
    t.integer  "show_order"
  end

  create_table "blogs", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.text     "short_description"
    t.string   "slug"
    t.boolean  "status"
    t.date     "published_on"
    t.string   "category"
    t.string   "metatitle"
    t.text     "metakeyword"
    t.text     "metadescription"
    t.boolean  "featured"
    t.integer  "counter",           :default => 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "careers", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "resume_file_name"
    t.string   "resume_content_type"
    t.integer  "resume_file_size"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "category_group"
    t.string   "category_type"
    t.integer  "category_group_id"
    t.boolean  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clinic_registrations", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "mobileno"
    t.string   "address"
    t.string   "city"
    t.string   "pincode"
    t.string   "state"
    t.string   "gender"
    t.datetime "dob"
    t.string   "school_name"
    t.string   "standard"
    t.string   "food_preference"
    t.string   "guardian_name"
    t.string   "guardian_mobileno"
    t.string   "guardian_email"
    t.string   "guardian_relationship"
    t.string   "adult_size"
    t.boolean  "guardian_terms"
    t.integer  "order_id"
    t.string   "order_status"
    t.string   "tracking_id"
    t.string   "back_ref_no"
    t.string   "failure_message"
    t.string   "billing_address"
    t.string   "billing_city"
    t.string   "billing_state"
    t.string   "billing_zip"
    t.string   "billing_country"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "data_file_name"
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "youth_kit_size"
    t.string   "amount_pay"
  end

  create_table "comments", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.text     "comment"
    t.integer  "blog_id"
    t.boolean  "approved"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contacts", :force => true do |t|
    t.string   "name"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "pin"
    t.string   "area_code"
    t.string   "phone"
    t.string   "mobile"
    t.string   "fax"
    t.string   "email"
    t.string   "project"
    t.string   "mailer"
    t.string   "campaign"
    t.string   "occupation"
    t.string   "profession"
    t.string   "service_area"
    t.string   "website"
    t.string   "organization"
    t.string   "designation"
    t.string   "enquiry_about"
    t.string   "entity_type"
    t.string   "entity_id"
    t.string   "page_url"
    t.string   "query"
    t.string   "hidden_data"
    t.string   "department"
    t.text     "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cpfl_registration_news", :force => true do |t|
    t.string   "name"
    t.string   "gender"
    t.datetime "dob"
    t.string   "school_name"
    t.string   "standard"
    t.string   "mobile_no"
    t.string   "email"
    t.string   "data_file_name"
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "youth_kit_size"
    t.string   "adult_kit_size"
    t.string   "amount_pay"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "order_id"
    t.string   "order_status"
    t.string   "tracking_id"
    t.string   "bank_ref_no"
    t.string   "failure_message"
    t.string   "billing_address"
    t.string   "billing_city"
    t.string   "billing_state"
    t.string   "billing_zip"
    t.string   "billing_country"
  end

  create_table "cpfl_registrations", :force => true do |t|
    t.string   "name"
    t.string   "gender"
    t.datetime "dob"
    t.string   "school_name"
    t.string   "class"
    t.string   "mobile_no"
    t.string   "email"
    t.string   "data_file_name"
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "kit_size"
    t.string   "former_participant"
    t.string   "former_participant_event"
    t.string   "football_experience"
    t.string   "medical_problem"
    t.string   "mediacal_app_name"
    t.string   "medical_year_class"
    t.datetime "medical_dob"
    t.string   "medical_gender"
    t.string   "medical_address"
    t.string   "medical_physician_name"
    t.string   "medical_contact_no"
    t.string   "medical_allergies"
    t.string   "medical_reaction"
    t.string   "medical_reaaction"
    t.string   "medical_hospitalization_occurred"
    t.string   "medical_allergy_medication"
    t.string   "medical_asthma"
    t.string   "medical_tetanus_injection"
    t.boolean  "medical_terms"
    t.string   "medical_parent_name"
    t.string   "medical_print_nameapp"
    t.string   "medical_relation_toapp"
    t.string   "parent_father_name"
    t.string   "parent_father_mobileno"
    t.string   "parent_father_emailid"
    t.string   "parent_mother_name"
    t.string   "parent_mother_mobileno"
    t.string   "parent_mother_emailid"
    t.string   "parent_address"
    t.string   "parent_father_volunteer"
    t.string   "parent_father_kit_size"
    t.string   "parent_mother_volunteer"
    t.string   "parent_mother_kit_size"
    t.string   "emergency_name"
    t.string   "emergency_mobileno"
    t.string   "emergency_relation"
    t.string   "location"
    t.string   "fee_structure"
    t.boolean  "terms_condition"
    t.boolean  "paren_consent"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cryptos", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.date     "published_date"
    t.string   "publication"
    t.string   "slug"
    t.string   "author"
    t.text     "disclaimer"
    t.text     "info"
    t.boolean  "status"
    t.boolean  "featured"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "facebook_contacts", :force => true do |t|
    t.string   "fullname"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "guardians_phone"
    t.string   "guardians_email"
    t.string   "gender"
    t.string   "date_of_birth"
    t.string   "city"
    t.string   "preferred_center"
    t.string   "school_name"
    t.string   "utm_source"
    t.string   "utm_campaign"
    t.string   "utm_medium"
    t.string   "utm_term"
    t.string   "utm_content"
    t.string   "srd"
    t.string   "project"
    t.string   "url"
    t.string   "device"
    t.string   "sibling_name"
    t.string   "sibling_number"
    t.text     "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone"
    t.string   "form_id"
  end

  create_table "facilities", :force => true do |t|
    t.string   "name"
    t.string   "mobile"
    t.string   "email"
    t.string   "phone"
    t.string   "location"
    t.string   "sport"
    t.string   "venue"
    t.string   "time"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fcbescola_trials", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "dob"
    t.string   "school_name"
    t.string   "home_phone"
    t.string   "mobile_no"
    t.string   "email"
    t.string   "traning_venue"
    t.string   "hear_about"
    t.string   "guardian_first_name"
    t.string   "guardian_last_name"
    t.string   "guardian_mobile"
    t.string   "guardian_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "city"
  end

  create_table "galleries", :force => true do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "published_date"
    t.string   "publication"
    t.string   "slug"
    t.string   "info"
    t.boolean  "status"
    t.string   "featured"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "published_year"
  end

  create_table "metacontents", :force => true do |t|
    t.string   "url"
    t.text     "meta_titles"
    t.text     "meta_keywords"
    t.text     "meta_description"
    t.text     "meta_content"
    t.text     "meta_text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "news", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.date     "published_date"
    t.string   "publication"
    t.string   "slug"
    t.string   "author"
    t.text     "disclaimer"
    t.text     "info"
    t.boolean  "status"
    t.boolean  "featured"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "doc_file_name"
    t.string   "doc_content_type"
    t.integer  "doc_file_size"
    t.integer  "show_order"
  end

  create_table "pages", :force => true do |t|
    t.integer  "parent_id"
    t.string   "title"
    t.string   "slug"
    t.string   "tagline"
    t.text     "overview"
    t.text     "metakeywords"
    t.text     "metadescription"
    t.text     "metatitle"
    t.boolean  "show_in_header"
    t.boolean  "show_on_site"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "press_releases", :force => true do |t|
    t.string   "title"
    t.string   "slug"
    t.text     "description"
    t.text     "short_description"
    t.boolean  "featured"
    t.boolean  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "url"
    t.date     "published_date"
    t.integer  "show_order"
  end

  create_table "projects", :force => true do |t|
    t.string   "name"
    t.string   "selldocrmid"
    t.string   "slug"
    t.string   "tagline"
    t.string   "short_description"
    t.text     "description"
    t.date     "started_at"
    t.date     "completion_date"
    t.string   "project_type"
    t.string   "project_status"
    t.string   "project_category"
    t.string   "project_selling_type"
    t.string   "project_size"
    t.string   "availability"
    t.text     "clients"
    t.string   "towers"
    t.text     "address"
    t.integer  "area"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "latitude"
    t.string   "longitude"
    t.text     "map_description"
    t.string   "zip"
    t.text     "neighbourhood"
    t.text     "tags"
    t.text     "meta_title"
    t.text     "meta_description"
    t.text     "meta_keywords"
    t.integer  "show_order"
    t.boolean  "featured"
    t.boolean  "show_on_website"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "youth_kit_size"
    t.string   "amount_pay"
  end

  create_table "redirects", :force => true do |t|
    t.string   "old_uri"
    t.string   "new_uri"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "resumes", :force => true do |t|
    t.string   "name"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "gender"
    t.string   "phone"
    t.string   "mobile"
    t.text     "address"
    t.string   "position"
    t.string   "area_of_interest"
    t.string   "resume_file_name"
    t.string   "resume_content_type"
    t.integer  "resume_file_size"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "school_registrations", :force => true do |t|
    t.string   "school_name"
    t.text     "address"
    t.string   "email"
    t.string   "mobile"
    t.integer  "no_of_participation"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "enquire_type"
    t.string   "participant_name"
    t.string   "date_of_birth"
    t.string   "preferred_center"
  end

  create_table "site_settings", :force => true do |t|
    t.string   "sitetitle"
    t.string   "phone"
    t.string   "email"
    t.boolean  "show_comments"
    t.string   "xmlsitemap_file_name"
    t.string   "xmlsitemap_content_type"
    t.integer  "xmlsitemap_file_size"
    t.string   "htmlsitemap_file_name"
    t.string   "htmlsitemap_content_type"
    t.integer  "htmlsitemap_file_size"
    t.string   "googlemeta_file_name"
    t.string   "googlemeta_content_type"
    t.integer  "googlemeta_file_size"
    t.text     "google_verification_code"
    t.text     "google_analytics_code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "teams", :force => true do |t|
    t.string   "name"
    t.string   "designation"
    t.text     "short_info"
    t.text     "information"
    t.boolean  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "testimonials", :force => true do |t|
    t.string   "name"
    t.text     "testimonial_text"
    t.string   "designation"
    t.date     "published_date"
    t.boolean  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "category"
  end

  create_table "tiny_prints", :force => true do |t|
    t.string   "image_file_name"
    t.string   "image_file_size"
    t.string   "image_content_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tiny_videos", :force => true do |t|
    t.string   "original_file_name"
    t.string   "original_file_size"
    t.string   "original_content_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trial_registrations", :force => true do |t|
    t.string   "fullname"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "guardians_phone"
    t.string   "guardians_email"
    t.string   "gender"
    t.string   "date_of_birth"
    t.string   "city"
    t.string   "preferred_center"
    t.string   "school_name"
    t.string   "utm_source"
    t.string   "utm_campaign"
    t.string   "utm_medium"
    t.string   "utm_term"
    t.string   "utm_content"
    t.string   "srd"
    t.string   "project"
    t.string   "url"
    t.string   "device"
    t.string   "sibling_name"
    t.string   "sibling_number"
    t.text     "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tuples", :force => true do |t|
    t.string   "key"
    t.text     "value"
    t.integer  "related_tuple_id"
    t.string   "related_tuple_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "videos", :force => true do |t|
    t.string   "title"
    t.text     "url"
    t.boolean  "status"
    t.date     "published_date"
    t.string   "author"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "show_order"
  end

end
