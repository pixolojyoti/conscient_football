Conscientfootball::Application.routes.draw do
  resources :facilities

  resources :facebook_contacts

  resources :careers

  resources :school_registrations

  resources :videos

  resources :galleries

  resources :clinic_registrations

  resources :cpfl_registration_news

  resources :cpfl_registrations

  resources :fcbescola_trials

  resources :cryptos
  resources :trial_registrations
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end
  match "/users/sign_up",  :to => redirect{"/users/sign_in"}
  match "/users/password/new",  :to => redirect{"/users/sign_in"}

  devise_for :users
  resources :testimonials
  resources :teams
  resources :resumes
  resources :pages
  resources :awards
  resources :contacts
  resources :projects
  resources :news
  resources :events
  resources :press_releases
  resources :blogs
  resources :metacontents
  resources :redirects
  resources :comments
  resources :site_settings
  resources :assets
    resources :categories
  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end
  root :to => 'site#fcbescolanew', :constraints => lambda{|request| request.env['SERVER_NAME'].match('fcbescola.conscientfootball.com')}
  root :to => 'site#fcbclinics', :constraints => lambda{|request| request.env['SERVER_NAME'].match('fcbclinics.conscientfootball.com')}
  #match "/fcbescolanew" , :to => "site#fcbescolanew"
  match "/admin" , :to => "admin#index"
  match "/trial-registration" , :to => "site#registeration"

  # match "/our-program",  :to => redirect{|p, req| "/our-programs?#{req.params.to_query}"}
  # match "/Clinics_and_Camps",  :to => redirect{|p, req| "/football-clinic-and-camps?#{req.params.to_query}"}
  # match "/join_us",  :to => redirect{|p, req| "/join-us?#{req.params.to_query}"}
  # match "/Coaching",  :to => redirect{|p, req| "/football-coaching?#{req.params.to_query}"}
  # match "/Tournaments_and_Leagues",  :to => redirect{|p, req| "/football-tournaments-and-leagues?#{req.params.to_query}"}
  # match "/fcbescola-goals",  :to => redirect{|p, req| "/fcbescola-goal?#{req.params.to_query}"}


  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  match "/fcbescola-mumbai",  :to => redirect{|p, req| "/fcbescola-football-academy-mumbai?#{req.params.to_query}"}
  match "/fcbescola-delhi",  :to => redirect{|p, req| "/fcbescola-football-academy-delhi?#{req.params.to_query}"}
  root :to => "site#index"
  match "/about-us" , :to => "site#aboutus1"
  match "/our-programs" , :to => "site#ourprogram_new"
  match "/gallery" , :to => "site#gallery"
  match "/gallery-old" , :to => "site#gallery_old"
  match "/gallery-details/:year" , :to => "site#gallery_details"
  match "/gallery-details-new/:year" , :to => "site#gallery_details_new"
  match "/gallery_old/:id" => "site#gallery_old"
  match "/media" , :to => "site#media"
  match "/media/news" , :to => "site#news"
  match "/media/videos" , :to => "site#videos"
  match "/media/online-coverage" , :to => "site#press_release"
  match "/media/blogs" , :to => "site#blog_list"
  match "/contact-us" , :to => "site#contactus"
  match "/join-us" , :to => "site#career"
  match "/football-coaching" , :to => "site#training"
  match "/annual-calendar" , :to => "site#annual_calendar"
  match "/football-camps" , :to => "site#barcelona_clinic"
  match "/football-summer-camps" , :to => "site#summer_camps"
  match "/football-clinic-and-camps" , :to => "site#camps"
  match "/football-tournaments-and-leagues" , :to => "site#cpfl"
  match "/cpfl-form" , :to => "site#cpfl_form"
  match "/faq" , :to => "site#faq"
  match "/faq-overview" , :to => "site#faq_overview"
  match "/fcbse-venue" , :to => "site#fcbse_venue"
  #match "/football-camps" , :to => "site#football_camps"

match "/site/facility", :to => "site#facility"

  
  match "/gallery-details" , :to => "site#gallery_details"
  match "/gallery-details-new" , :to => "site#gallery_details_new"
  match "/how-we-train" , :to => "site#how_we_train"
  match "/league-table" , :to => "site#league_table"
  match "/our-program-training" , :to => "site#our_program_training"
  match "/overview-fcbescola" , :to => "site#overview_fcbescola"
  match "/rules-regulations" , :to => "site#rules_regulations"
  match "/schedule" , :to => "site#schedule"
  match "/training-program" , :to => "site#training_program"
  match "/view-center" , :to => "site#view_center"
  match "/why-join" , :to => "site#why_join"
  match "/camp-schedule" , :to => "site#camp_schedule"
  match "/upcoming-events" , :to => "site#upcoming_events"
  match "/upcoming-event-details/:id" => "site#upcoming-event-details"
  match "/aboutus1" , :to => "site#aboutus1"
  match "/success-stories" , :to => "site#success_stories"

  match "/facility-management" , :to => "site#mgmt"


  match "/success-stories-details/:id" => "site#success_stories_details"

  match "/about-barca-clinic" , :to => "site#about_barca_clinic"
  match "/terms-and-conditions" , :to => "site#terms_condition"
  match "/fcbescola-goal" , :to => "site#fcbescola_goals"
  match "/registeration" , :to => "site#registeration"
  match "/social-responsibility" , :to => "site#social_responsibility"
  match "/terms-condition-clinics" , :to => "site#terms_condition_clinics"
  match "/privacy-policy" , :to => "site#privacy_policy"
  match "/blog-detail/:id" , :to => "site#blog_details"

  match "/registration_data" , :to => "school_registrations#registration_data"

  match "/fcbescola" , :to => "fcbescola#index"
  match "/fcbescola/trial-registration" , :to => "fcbescola#registeration"


  match "/fcbescola/privacy-policy" , :to => "fcbescola#privacy_policy"
  match "/fcbescola/social-responsibility" , :to => "fcbescola#social_responsibility"
  match "/fcbescola/terms-and-conditions" , :to => "fcbescola#terms_condition"
  post "/regsubmit", :to => "fcbescola#regsubmit"
  post "/regsubmit_delhi", :to => "fcbescola#regsubmit_delhi"

  post "/regsubmit_dwarka", :to => "fcbescola#regsubmit_dwarka"


  match "/fcbescola-football-academy-mumbai" , :to => "fcbescola_mumbai#index"
  match "/fcbescola-mumbai/trial-registration" , :to => "fcbescola_mumbai#registeration"
  match "/fcbescola-mumbai/privacy-policy" , :to => "fcbescola_mumbai#privacy_policy"
  match "/fcbescola-mumbai/social-responsibility" , :to => "fcbescola_mumbai#social_responsibility"
  match "/fcbescola-mumbai/terms-and-conditions" , :to => "fcbescola_mumbai#terms_condition"
  match "/fcbescola-mumbai-juhu-center" , :to => "fcbescola_mumbai#fcbescola_juhu_center"
  match "/fcbescola-mumbai-matunga-center" , :to => "fcbescola_mumbai#fcbescola_matunga_center"
  match "/fcbescola-mumbai-andheri-center" , :to => "fcbescola_mumbai#fcbescola_andheri_center"
  match "/fcbescola-south-mumbai-center" , :to => "fcbescola_mumbai#fcbescola_south_mumbai_center"
  post "/reg_south_mumbai", :to => "fcbescola_mumbai#reg_south_mumbai"
  post "/reg_matunga", :to => "fcbescola_mumbai#reg_matunga"
  post "/reg_andheri", :to => "fcbescola_mumbai#reg_andheri"
  post "/reg_juhu", :to => "fcbescola_mumbai#reg_juhu"

  match "/fcbescola-football-academy-delhi" , :to => "fcbescola_delhi#index"
  match "/fcbescola-delhi/trial-registration" , :to => "fcbescola_delhi#registeration"

  match "/fcbescola-delhi-dwarka-center/trial-registration" , :to => "fcbescola_delhi#registration_dwarka"

  match "/fcbescola-delhi/privacy-policy" , :to => "fcbescola_delhi#privacy_policy"
  match "/fcbescola-delhi/social-responsibility" , :to => "fcbescola_delhi#social_responsibility"
  match "/fcbescola-delhi/terms-and-conditions" , :to => "fcbescola_delhi#terms_condition"

  match "/site/fb_lead" , :to => "site#fb_lead"

  match "/career",  :to => redirect{"/join-us"}
  match "/football-clinic-and-camps/enquiry.html",  :to => redirect{"/football-clinic-and-camps"}
  match "/camps",  :to => redirect{"/football-camps"}
  match "/football-clinic-and-camps/schedule20131112.html",  :to => redirect{"/football-clinic-and-camps"}
  match "/football-clinic-and-camps/index2013dec17.html",  :to => redirect{"/football-clinic-and-camps"}

  match "/football-clinic-and-camps/enquiry5dec2012bkp.html",  :to => redirect{"/football-clinic-and-camps"}
  match "/football-clinic-and-camps/pdf/the-tribune.pdf",  :to => redirect{"/football-clinic-and-camps"}
  match "/football-clinic-and-camps/about.html",  :to => redirect{"/football-clinic-and-camps"}
  match "/football-clinic-and-camps/index20140114.html",  :to => redirect{"/football-clinic-and-camps"}
  match "/football-clinic-and-camps/schedule.html",  :to => redirect{"/football-clinic-and-camps"}
  match "/football-clinic-and-camps/index23dec2013.html",  :to => redirect{"/football-clinic-and-camps"}
  match "/football-clinic-and-camps/schedule_26_june_2014.html",  :to => redirect{"/football-clinic-and-camps"}
  match "/football-clinic-and-camps/index_4_july_2014.html",  :to => redirect{"/football-clinic-and-camps"}
  match "/football-clinic-and-camps/images/",  :to => redirect{"/gallery-details/2016-17"}
  match "/football-clinic-and-camps/news.html",  :to => redirect{"/media"}
  match "/football-clinic-and-camps/pdf/fcbcamps-chandigarh.pdf",  :to => redirect{"/football-camps"}

  # See how all your routes lay out with "rake routes"

  #match "(*seo)", :to => "site#not_found", :status => 404

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
  match ':controller(/:action(/:id(.:format)))'

 match "(*seo)", :to => "site#not_found", :status => 404

end
