COUNTRIES_SUPPORTED = ["India"]
Max_Attachment_Size = 10.megabyte


PROJECT_MULTIPLE_ASSETS = ["overview","archtechture","interior","exterior","features","lifestyle", "specifications","amenities","plans","layout","awards_and_accolades", "gallery", "neighbourhood",  "thumbnail", "site_progress", "background", "amenities_and_specifications", "location" ]

PROJECT_ASSETS_MENU_ORDER = ["overview","features","specifications","amenities","plans","layout","gallery","location", "neighbourhood","lifestyle","interior","exterior","updates"]

WORDIFY = %w(zero one two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen)
#PROJECT_MULTIPLE_ASSETS = ["amenities & specifications"]
MEDIUM_MULTIPLE_ASSETS = ["inner_pages"]
RESUME_SINGULAR_ASSETS=["resume"]
BLOG_MULTIPLE_ASSETS = ["gallery"]
PAGE_MULTIPLE_ASSETS = ["overview"]
TESTIMONIAL_MULTIPLE_ASSETS = ["overview"]
TEAM_MULTIPLE_ASSETS = ["overview"]
PROJECT_CATEGORY = ["residential","commercial","hospitality", "retail", "social_infrastructure"]
PROJECT_TYPE = ["2bhk","3bhk","4bhk","5bhk", "Condominiums", "Penthouses"]
PROJECT_STATUS = ["upcoming","ongoing","completed"]
PROJECT_SELLING_TYPE = ["lease","sale"]

NEWS_SINGULAR_ASSETS = ["main_image"]
NEWS_MULTIPLE_ASSETS = ["gallery"]

CSR_SINGULAR_ASSETS = ["main_image"]
CSR_MULTIPLE_ASSETS = ["gallery", "csr"]


PAGE_SINGULAR_ASSETS = ["main_image"]
PAGE_MULTIPLE_ASSETS = ["gallery"]

EVENT_SINGULAR_ASSETS = ["main_image"]
EVENT_MULTIPLE_ASSETS = ["gallery", "event"]


AWARD_MULTIPLE_ASSETS = ["overview"]
PRESSRELEASE_MULTIPLE_ASSETS = ["overview"]
PRESS_MULTIPLE_ASSETS = ["overview"]


GALLERY_SINGULAR_ASSETS = ["main_image"]
GALLERY_MULTIPLE_ASSETS = ["gallery"]

EXHIBITION_MULTIPLE_ASSETS = ["gallery", "exhibition"]

MEDIUM_MULTIPLE_ASSETS = ["gallery", "media"]

ADVERTISEMENT_MULTIPLE_ASSETS = ["gallery", "advertisement"]

PRINTMEDIUM_MULTIPLE_ASSETS = ["gallery", "media"]

EVENT_SINGULAR_ASSETS = ["main_image"]

EVENT_MULTIPLE_ASSETS = ["gallery"]

SITE_TITLE = "Amura"
ADMIN = "administrator"
