# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20170811135135) do

  create_table "trial_registrations", :force => true do |t|
    t.string   "fullname"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "guardians_phone"
    t.string   "guardians_email"
    t.string   "gender"
    t.string   "date_of_birth"
    t.string   "city"
    t.string   "preferred_center"
    t.string   "school_name"
    t.string   "utm_source"
    t.string   "utm_campaign"
    t.string   "utm_medium"
    t.string   "utm_term"
    t.string   "utm_content"
    t.string   "srd"
    t.string   "project"
    t.string   "url"
    t.string   "device"
    t.string   "sibling_name"
    t.string   "sibling_number"
    t.text     "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
