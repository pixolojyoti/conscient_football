class ApplicationController < ActionController::Base
  protect_from_forgery
#  before_filter :set_cookies
  def set_cookies

    tmp = params[:utm_source]
    if cookies[:utm_source].blank?
      if tmp.present?
        cookies[:utm_source] = tmp
        
      else
        cookies[:utm_source] = "website"
      end
    end

    tmp2 = params[:utm_campaign]
    if cookies[:utm_campaign].blank? 
      if tmp2.present?
        cookies[:utm_campaign] =  tmp2
      else
        cookies[:utm_campaign] =  "organic"
      end
    end

    tmp3 = params[:utm_medium]
    if cookies[:utm_medium].blank? 
      if tmp3.present?
        cookies[:utm_medium] =  tmp3
      else
        cookies[:utm_medium] =  "website_organic"
      end
    end
    tmp4 = params[:srd]
    if cookies[:srd].blank?
      if tmp.present?
        cookies[:srd] = tmp
        
      end
    end

  end   
end
