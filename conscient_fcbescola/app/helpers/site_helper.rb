module SiteHelper

	def index
		"/"
	end

	def registeration
		"/trial-registration"
	end

	def url_social_responsibility
		"/social-responsibility"
	end	

	def url_careers
		"/careers"
	end
	
	def url_terms_condition
		"/terms-and-conditions"
	end	

	def url_privacy_policy
		"/privacy-policy"
	end	
end
