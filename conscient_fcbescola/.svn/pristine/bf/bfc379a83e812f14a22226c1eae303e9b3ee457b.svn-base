<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><%= @metatitles %></title>
	<meta name="keywords" content="<%= @metakeywords %>"/>
	<meta name="description" content="<%= @metadescription %>"/>
	<link rel="shortcut icon" href="favicon.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link rel="stylesheet" href="/stylesheets/colorbox.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		
<!--[if  IE ]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Roboto:300,400,700');

html, body { 
	border:0; margin:0; padding:0;
	font-family: 'Roboto', sans-serif; font-size: 15px; 
	line-height: 25px;
	color: #000;
	font-weight: 400;
}


a{
	border: 0;
	outline: 0;
}

.text-left{
	text-align: left;
}

.text-right{
	text-align: right;
}

.text-center{
	text-align: center;
}

::-webkit-scrollbar {
	width: 8px;
}

::-webkit-scrollbar-track {
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.8); 
	border-radius: 10px;
}

::-webkit-scrollbar-thumb {
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(247,192,52,0.8); 
	background:rgba(247,192,52,0.8);
}

.clear { 
	clear:both; 
}
.white{
	color: #fff !important;
}

.pull-left{
	float: left;
}

.pull-right{
	float: right;
}

*, *:after, *:before {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.section{
	width: 100%;
}

.container{
	max-width: 1200px;
	margin: 0 auto;
}

.logo{
	width: 9%;
	margin: 0 auto;
}

.virtual-number-mobile{
	display: none;
}

.header-wrapper{
	width: 100%;
	background: rgba(0,0,0,1);
	padding: 10px 20px;
	text-align: center;
	position: fixed;
    top: 0;
    left: 0;
    z-index: 99;
    
    -webkit-transform: translateZ(0);
	-moz-transform: translateZ(0);
	-o-transform: translateZ(0);
	transform: translateZ(0);
	
	-webkit-transition: all 0.2s ease-in-out;
	-moz-transition: all 0.2s ease-in-out;
	-o-transition: all 0.2s ease-in-out;
	transition: all 0.2s ease-in-out;
}

.header-wrapper-scrolled{
	width: 100%;
	background: rgba(0,0,0,0.8);
	padding: 10px 20px;
	text-align: center;
	position: fixed;
    top: 0;
    left: 0;
    z-index: 99;
    
    -webkit-transform: translateZ(0);
	-moz-transform: translateZ(0);
	-o-transform: translateZ(0);
	transform: translateZ(0);
	
	-webkit-transition: all 0.2s ease-in-out;
	-moz-transition: all 0.2s ease-in-out;
	-o-transition: all 0.2s ease-in-out;
	transition: all 0.2s ease-in-out;
}

.img-responsive{
	max-width: 100%;
}

.main-banner{
	background: url(/images/Registration-IMg.jpg) no-repeat center center;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    height: 100%;
    margin-top: 100px;
    
}

.relative{
	position: relative;
}

.absolute{
	position: absolute;
}

.banner-title{
	bottom: 20px; 
	width: 100%;
	text-align: center;
	font-size: 30px;
	font-weight: 700;
	text-transform: uppercase;
}

.yellow{
	color:#F9BF1E;	
}

.form-wrapper{
	width: 50%;
}

.details-wrapper{
	width: 50%;
	padding: 0px 20px;
}

.details-wrapper li{
	 list-style-type: none;
    padding: 0px 0px 15px 0px;
    font-size: 15px;
}

.details-wrapper ul{
	padding-left: 0px;
	margin:0;
}

.details-wrapper li img{
	display: inline-block;
	margin-right: 10px;
}

.details-wrapper li span{
	display: block;
	vertical-align: top;
	float: left;
}

.video-box{
	width: 95%;
    /*height: 325px;
    background-image: url(/images/Video-Thumb.jpg);
    background-repeat: no-repeat;
    background-size: cover;*/
}

.padding50{
	padding: 50px 0px;
}

.form-btn{
	color: #000;
	font-size: 16px;
	border: 1px solid #ecf0f1;
	padding: 10px 0px;
	text-decoration: none;
	text-align: center;
	display: inline-block;
	width: 50%;
}

.active-form-btn{
	background-color: #ecf0f1 !important;
}

.form-container{
    padding: 20px 40px;
    background-color: #ecf0f1;
}


.selldof .checkbox input[type=checkbox], .selldof .checkbox-inline input[type=checkbox], .selldof .radio input[type=radio], .selldof .radio-inline input[type=radio]{
	margin-top: 6px;
}

.selldof .title{
	display: none;
}

.footer{
	background: rgba(0,0,0,0.8);
    width: 100%;
    font-size: 12px;
}

.selldof .help-block{
	position: absolute !important;
    left: 20px !important;
    bottom: -17px !important;;
}

.selldof .bootstrap-datetimepicker-widget {
    top: 10px;
    left: 10px;
    position: absolute !important;
    background: #eee !important;
    z-index: 99 !important;
}

.title{
	font-size: 20px;
    font-weight: 700;
    margin-bottom: 25px;
}

.selldof .form-control, .sell_do_form_control{
	border-radius: 0px !important;

}

.selldof{
	font-family: 'Roboto', sans-serif !important; 
}

.selldof .btn, .sell_do_ctc_btn, .selldof .btn, .sell_do_verify_btn{
	border-radius: 0px !important;
	background-color: #f8c034 !important;
	color: #000 !important;
	border: 0 !important;
	text-transform: uppercase;
	padding: 8px 45px !important;
    font-size: 14px !important;
}

.barcalonaclinicHd {
    font-size: 24px;
    line-height: 36px;
    text-transform: uppercase;
    color: #000;
    position: relative;
    margin-bottom: 25px;
}

.virtual-number{
	    width: 345px;
    background-color: #f9c136;
    position: fixed;
    right: -300px;
    top: 10%;
    z-index: 999;
    padding: 10px;
    font-size: 16px;
    transition:0.6s;
    cursor: pointer;
}

.number {
    font-size: 25px;
    margin-right: 20px;
    margin-top: 12px;
    margin-left: 5px;
}

@media (max-width: 1024px){
.details-wrapper li{
		padding: 0px 0px 15px 0px;
	}

.details-wrapper li span{
	font-size: 14px;
}
/*
.video-box{
	height: 270px;
}*/

	/*.details-wrapper li img{
		display: none;
	}*/
}


@media (max-width: 786px){
.details-wrapper li{
		padding: 0px 0px 15px 0px;
	}

	/*.details-wrapper li img{
		display: none;
	}*/
/*
	.video-box{
		height: 200px;
	}
*/
	.main-banner{
		margin-top: 74px;
	}
}

@media (max-width: 700px){
	.form-wrapper, .details-wrapper{
		width: 100%;
	}

	.logo{
		width: 25%;
	}
	.details-wrapper ul{
		margin-top: 20px;
	}

	.details-wrapper li{
		padding: 5px 0px 5px 0px;
		font-size: 16px;
	}

	.virtual-number{
		display: none;
		
	}

	.virtual-number-mobile{
		display: block;
	    width: 100%;
	    position: fixed;
	    background: rgb(53, 53, 53);
	    right: 0;
	    bottom: 0px;
	    z-index: 999;
	    padding: 0px;
	    font-size: 20px;
	    text-align: center;
	    color: #fff;
	 }

	 .number-mobile{
	 	width: 100%;
    	font-size: 16px;
	 }

	 .main-banner{
	 	margin-top: 76px;
	 }

	 .form-container {
	    padding: 20px 20px;
	}
/*
	.video-box {
		height: 170px;
	}*/
/*
	.details-wrapper li img{
		display: none;
	}*/
}
.check-mark{
	background-color: #d9130e;
	border-radius: 100%;
	color: #fff;
	width: 26px;
	height: 26px;
	text-align: center;	
	margin-right: 5px;
}

.pointers{
	width: 84%;
}
.select2-container--default .select2-selection--single{
    border: 1px solid #cccccc !important;
    border-radius: 0px !important;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075) !important;
    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075) !important;
    -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s !important;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s !important;
    height: 32px !important;
}
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-93759358-1', 'auto');
ga('send', 'pageview');
ga('require', 'displayfeatures');
ga('require', 'ecommerce', 'ecommerce.js'); 
</script>
</head>
<body>
<div class="virtual-number">
		<div class="pull-left number" data-event-category="fcbescola-number-button" data-event-action="click" data-event-name="phone icon">
			<i class="fa fa-phone" aria-hidden="true"></i>
		</div>
		<div class="pull-left virtual-number-wrapper">
			<span>&nbsp;<!-- Delhi NCR: --> <span class="sell_do_virtual_numbers"></span></span> <br/>
		</div>
		<div class="clear"></div>
	</div>
	<div class="virtual-number-mobile">
		<div class="pull-left number-mobile  virtual-number-wrapper">
			<span>&nbsp;	<!-- Delhi NCR: --> <span class="sell_do_virtual_numbers"></span></span> <br/>
		</div>
		<div class="clear"></div>
	</div>
<div class="header-wrapper">
	<div class="container">
		<div class="logo">
			<a href="http://fcbescola.conscientfootball.com/"><img src="/images/conscient_football_logo.png" class="img-responsive"></a>
		</div>
	</div>
</div>
<div class="section main-banner slide relative">
	<div class="absolute banner-title yellow">
		Welcome to FCBEscola India
	</div>
</div>
<div class="section padding50">
	<div class="container">

		<div class="form-wrapper pull-right">
			<!-- <div class="form-title">
				<a href="javascript:;" class="form-btn active-form-btn" rel="minor">IF (MINOR)</a><a href="javascript:;" rel="parent" class="form-btn">IF (PARENTS)</a>
			</div> -->
			<div class="form-container" id="minor">
				<div class="barcalonaclinicHd">TRIAL REGISTRATION</div>
				<script src='//trkr.scdn1.secure.raxcdn.com/t/forms/573c82f7541385bf32000033/58b915ec3bb2f8073a000861.js' data-form-id='58b915ec3bb2f8073a000861'></script>
			</div>
			<!-- <div class="form-container" id="parent" style="display:none;">
				<script src='//trkr.scdn1.secure.raxcdn.com/t/forms/573c82f7541385bf32000033/58b915ec3bb2f8073a000861.js' data-form-id='58b915ec3bb2f8073a000861'></script>
			</div> -->
		</div>
		<div class="details-wrapper pull-left">
			<ul>
				<li><span class="check-mark"><i class="fa fa-check" aria-hidden="true"></i></span><span class="pointers">After School Training Programme</span>
				<div class="clear"></div>
				</li>
				<li><span class="check-mark"><i class="fa fa-check" aria-hidden="true"></i></span><span class="pointers">Training for Players between 5-17 years of Age</span>
					<div class="clear"></div>
				</li>
				<li><span class="check-mark"><i class="fa fa-check" aria-hidden="true"></i></span><span class="pointers">Technical Directors from FCB oversee all coaching.</span>
					<div class="clear"></div>
				</li>
				<li><span class="check-mark"><i class="fa fa-check" aria-hidden="true"></i></span><span class="pointers">Small Training groups (8-14 players)</span>
					<div class="clear"></div>
				</li>
				<li><span class="check-mark"><i class="fa fa-check" aria-hidden="true"></i></span><span class="pointers">Free Trial Session post which offer is sent to the player.</span>
					<div class="clear"></div>
				</li>
				<li><span class="check-mark"><i class="fa fa-check" aria-hidden="true"></i></span><span class="pointers">Internal League matches</span>
					<div class="clear"></div>
				</li>
				<li><span class="check-mark"><i class="fa fa-check" aria-hidden="true"></i></span><span class="pointers">Centers in Gurgaon, Delhi, Noida & Mumbai.</span>
					<div class="clear"></div>
				</li>
				<li><span class="check-mark"><i class="fa fa-check" aria-hidden="true"></i></span><span class="pointers">Players train under the same methodology learned by<br/> Lionel Messi, Andrés Iniesta, Sergio Busquets & Gerard Piqué, among others. </span>
					<div class="clear"></div>
				</li>
				<li><span class="check-mark"><i class="fa fa-check" aria-hidden="true"></i></span><span  class="pointers">Chance to represent FCBEscola in the annual EIT tournament in Barcelona, Spain.</span>
					<div class="clear"></div>
				</li>
			</ul>
			<a class="video-box" href="javascript:;" style="display:block">
				<iframe width="100%" height="315" src="https://www.youtube.com/embed/m7V87laf_IE" frameborder="0" allowfullscreen></iframe>
			</a>
			
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="footer text-center yellow">
	© Copyright 2017. All Rights Reserved
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/javascripts/jquery.colorbox.js"></script>	
<script src='//trkr.scdn1.secure.raxcdn.com/t/573c82f7541385bf32000033.js'></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TSG2874');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TSG2874"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->



<script type="text/javascript">
	$(document).ready(function(){
		var wht=$(window).height();
		var pageSectiionHeight=wht/2;
		$('.slide').height(pageSectiionHeight);

		$(".form-title a").click(function(){
			$(".form-container").hide();
			var link = $(this).attr("rel");
			$("a.form-btn").removeClass("active-form-btn");
			$(this).addClass("active-form-btn");
			$("#"+link).fadeIn();

		})

		$(".video-popup").colorbox({iframe:true, innerWidth:640, innerHeight:390,  maxWidth:'95%',maxHeight:'50%'});

		$(window).scroll(function() {    
		    var scroll = $(window).scrollTop();
		     //>=, not <=
		    if (scroll >= 50) {
		        //clearHeader, not clearheader - caps H
		        $(".header-wrapper").addClass("header-wrapper-scrolled");
		    }
		    else{
		    	$(".header-wrapper").removeClass("header-wrapper-scrolled");	
		    }
		});


	    $(".virtual-number").click(function(){
	    	if($(".virtual-number").css("right") === "-300px")
	    	{
	    		$(".virtual-number").css("right", "0px");	
	    	}
	    	else
	    	{
	    		$(".virtual-number").css("right", "-300px");
	    	}
	    	
	    })
	})
</script>
<script>
$(document).ready(function(){
$(document).on('change', 'select[name="sell_do[form][address][city]"]', function(){
	//console.log('ss');	
	if($('select[name="sell_do[form][address][city]"] option:selected' ).val()=='Mumbai'){
		$('select[name="sell_do[form][lead][project_id]' ).html('<option value="">Select</option><option value="573e9c803bb2f8a95d00002e">Andheri East Center</option><option value="573e9c375413853d070000e8">Matunga Center</option><option value="59703f94541385984000029e">South Mumbai</option>');
		
	}else if($('select[name="sell_do[form][address][city]"] option:selected' ).val()=='Delhi'){
		$('select[name="sell_do[form][lead][project_id]' ).html('<option value="">Select</option><option value="573e9be0a7a0396c6f00011e">Noida Center</option><option value="573e99825413856c68000146">South Delhi Center</option><option value="573e9a64a7a0396c6f0000ef">Gurgaon Center</option><option value="573e99fd3bb2f81091000095">West Delhi Center</option>');
		
	}else{
		$('select[name="sell_do[form][lead][project_id]' ).html('<option value="">Select</option><option value="573e9be0a7a0396c6f00011e">Noida Center</option><option value="573e99825413856c68000146">South Delhi Center</option><option value="573e9a64a7a0396c6f0000ef">Gurgaon Center</option><option value="573e99fd3bb2f81091000095">West Delhi Center</option><option value="573e9c803bb2f8a95d00002e">Andheri East Center</option><option value="573e9c375413853d070000e8">Matunga Center</option><option value="59703f94541385984000029e">South Mumbai</option>');
			
	}
});

var _selldo = [];
/*_selldo.push({project_id : '5867b98ea7a039f1e6000246'});*/
window.sell_do_form_successfully_submitted = function(data, event){
	console.log(data);

    try{
      dataLayer.push({
         'event' : 'selldo_form_submitted'
      });
    }catch(err){}
    
    /*window.location.href = "http://aoeminente.amura.in/images/Eminente-Brochure.pdf";*/
    /*if(digitNumber!=0){
    	$("#open-pdf")[0].click();
    }*/

}
  window.sell_do_form_rendered = function(){
  	var contains = function(needle) {
    // Per spec, the way to identify NaN is that it is not equal to itself
    var findNaN = needle !== needle;
    var indexOf;

    if(!findNaN && typeof Array.prototype.indexOf === 'function') {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function(needle) {
            var i = -1, index = -1;

            for(i = 0; i < this.length; i++) {
                var item = this[i];

                if((findNaN && item !== item) || item === needle) {
                    index = i;
                    break;
                }
            }

            return index;
        };
    }

    return indexOf.call(this, needle) > -1;
};

       $('[name="sell_do[form][custom][school_name]"]').replaceWith('<select data-required="true" data-validator="string" class="form-control" name="sell_do[form][custom][school_name]"></select>');
       
       	var school_array=["","Alliance World School, Noida","American School","Amity International School, Noida","Amity International School, Noida, Noida","Amity International, Gurgaon","Amity International, Pushp Vihar","Amity International, Saket","Amity Pushpvihar","Amity Saket","Amity University, Noida","Amrita Vidyalayam","Apeejay School, Kharghar","Apeejay School, Noida","Apeejay School, PitamPura","Apeejay School, Sheikh Sarai","Apeejay, Noida","Aravali International","Army Public School, Noida","Arya Vidya Mandir, Santacruz","Arya Vidyamandir, Juhu","ASB, Kurla","Ascend International School, BKC","Ascend International, Bandra","Assisi Convent School, Noida","Aster Public School, Noida","Atomic Energy Junior College, Chembur","Avalon Heights International School, Vashi","B D Somani International School, Cuff Parade","B.C.Model School, FBD","Bal Bharati Public School,Â Dwarka","Bal Bharati Public School, Noida","Bal Bharti, pitampura","Bal Bharti, Rohini","Balbharti Public School, Kharghar","Balmohan Vidya Mandir, Dadar","Bb Petit Girls School, Bandra","BD Somani School, Cuff Parade","Bharat College, Prabhadevi","Bhavans College, Andheri","Billabong High International , Santacruz","Billabong High International School, Noida","Billabong International, Malad","Birla Vidya Niketan","Blue Bells Model School, Gurgaon","Bombay Cambridge, Andheri","Bombay International School, Malabar Hill","Bombay Scottish, Mahim","Bombay Scottish, Powai","C C A School, Gurgaon","C.N.M.S, Vile Parle","C.P.Goenka, Juhu","Cambridge , Kandivili","Cambridge Noida, Noida","Cambridge, Noida","Campion School, Fort","Canossa High School, Andheri","Canterbury Public School, Gurgaon","Cathedral & John Connon School, Fort","Central Railway School, Kalyan","Childrens Academy, Malad","City International, Oshiwara","City International, Pune","City Public School, Noida","Claras College, Versova","CNMS, Vile Parle","CP Goenka International School, Juhu","D A V Public School, Gurgaon","D Y Patil International, Nerul","D.A.V Public School, Thane","D.A.V. School, Noida","D.G Ruparel College, Mahim","D.P.S , Nerul","D.P.Y.A High School, Dadar","Delhi Public School, Dwarka","Delhi Public School, Gautam Buddh Nagar, Noida","Delhi Public School, Noida","Delhi Public School, Noida, Noida","Delhi Public School, Rohini","Deutsche Schule, New Delhi","Dharumavantha school","Dhirubhai Ambani International School, BKC","Dominic Savio High School, Andheri","Don Bosco Borivali, Borivali","Don Bosco High School, Matunga","Don Bosco International School, Matunga","Don Bosco School","DPS Gautam Buddh Nagar, Noida","DPS Indirapuram, Noida","DPS Intl, R K Puram","DPS Intl, Saket","DPS Noida, Noida","DPS R K Puram","DPS School, Gurgaon","DPS Vasant Vihar","DPS, Vasant Kunj","Dr.Pillai Global Academy, Borivali","Dr.Sarvepalli Vidyalaya, Malad","DY Patil International, Worli","Ecole Mondiale World School, Juhu","Edubridge International School, Grant road","Euro International, Airoli","EuroSchool","Excelsior American School, Gurgaon","Excelssior","Father Agnel School","Fortune World School, Noida","G.D.GOENKA Public School, Noida","G.D.Goenka, Gurgaon","Ganga International Delhi, Delhi","Garodia Intl, Ghatkoper","Gd goenka , Paschim Vihar","GD Goenka Public School, VK","GD goenka rohini","Genesis Global School, Noida","Ghanshyam Das Jalan, Malad","GICL, Ghatkoper","Global Indian International School, Noida","Gokuldam high school, Goregaon","Gokuldham high school, Goregaon","Gokuldham High School, Malad","Goodley public school","Greenfingers Global School , Kharghar","Greenfingers Global School, Kharghar","Gregorios High School, Chembur","Gundecha Education Academy, Kandivili","Guru Harkrishan Public School, VV","Gyan Bharati School, Saket","Gyan Kendra High School, Andheri","Hansraj Jr.College, Andheri","Hansraj Morarji, Andheri","Harmony International School, Kharghar","Heritage Vasant Kunj","HFS Powai, Powai","Hiranandani School, Powai","Holy Family, Andheri","HR College, Churchgate","HVPS International, Andheri","ibambini preschool, Gurgaon","IES Junior College, Bandra","IES Manik Vidya Mandir , Bandra","IES Orion, Dadar","Indraprastha Global School, Noida","Indraprastha International School, Dwarka","Indus World School, Gurgaon","Intellitots Early Learning Centre, Gurgaon","Iraq School","ITL public school, Dwarka","J.B Petit High School, Fort","J.P.International School, Noida","Jai Hind College , Fort","Jai Hind College, Fort","Jain Happy School","Jamnabai Narsee International School, Juhu","Jamnabai Narsee School, Juhu","Jamnabhai Narsee School, Juhu","Jankidevi Public School, Andheri","Japanese School","JBCN International, Parel","Jesus and Mary Convent School, Noida","JNS, Juhu","K R Mangalam World School, GK 2","K V Gole Market","K.C College , Fort","K.M Agarwal College, Kalyan","Kalka P S","Kamrajar Junior college, Dharavi","Kendriya Vidyalaya, Powai","Kenia & Anchor English School, Chinchpokli","Kerala School, R K Puram","Khaitan Public School, Noida","Khalsa College, Matunga","Kidzee Play School, Noida","KJ Somiaya College, Ghatkoper","KLAY Gurgaon, Gurgaon","Kohinoor International School, Kurla","Kothari International School, Noida","Kunskapsskolan Eduventures, Gurgaon","KV Andrews Ganj","KVIIT Powai, Powai","L.R.Tiwari College, Mira Road","Lakshadham High School, Goregaon","LANCER INTERNATIONAL SCHOOL, Gurgaon","Lilavati Podar ICSE, Santacruz","Lilavati Poddar ISC, Santacruz","Lilavati Poddar, Santacruz","Lilavatibai Senior School, Santacruz","Little Angel High School, Sion","Little Scholar Play School, Noida","Lokhandanwala Foundation School , Kandivili","Lord Jesus Public School, Gurgaon","Lotus Valley Intl School","Lotus Valley International School, Gurgaon","Lotus Valley, Noida","LPS, Hauz Khas","Mainadevi bajaj International, Malad","Maj. T C Convent school","Manav Rachna Intl University","Manav Rachna International School, Noida","Maneckji Cooper, Juhu","Manek Vidya Mandir, Bandra","MATA GUJRI PUBLIC SCHOOL","MatriKiran High School, Gurgaon","MatriKiran Junior School, Gurgaon","Maxfort , Rohini","Maxfort school dwarka","Maxfort school Pachim Vihar","Mayoor School, Noida","Mirambika","MMK College, Bandra","MNR School Excellence, Panvel","Model College , Dombivali","Modern Barakhamba","Modern Juniors School, Humayun Rd","Modern Montessori International, Noida","Modern Public School, Shalimar BaghÂ â€ƒ","Modern School, Noida","Modern Vasant Vihar","Montfort School , Ashok Vihar","Mothers International School","Mothers Pride, Noida","Mount Carmel","Mount Carmel School","MPSTME, Vile Parle","Muljibai Mehta International School, Virar","Nehru International Public School, Noida","NES International School, Malabar Hill","New Cambridge, Peddar road","New English College, Kalyan","New Horizon Public School, Airoli","New Horizon Public School, Kharghar","New Horizon Public School, Panvel","Nirmala College","Nirmala College, Kandivili","No school","Noida Public School, Noida","North Point, Koper Khairane","NSS Hill Spring International School, Tardeo","O.L.P.S , Chembur","Oberoi International","Oberoi International , Goregaon","Oberoi International School, Goregoan","OLGC High School, Sion","Our Lady of Health, Andheri","Our Lady of Nazareth, Bhayander","Our Lady Of Salvation High School, Dadar","P.G Garodia School, Ghatkoper","Pace Jr College, Powai","Pallavan, Gurgaon","Parle Tilak Vidyalaya, Vile Parle","Pathways Noida","Pathways School Noida, Noida","Pathways School, Noida","Pathways, GGN","Pawar Public School, Bhandup","Pawar Public school, Chandivili","Podar International School, Bandra","Podar International School, Powai","Podar International, Kalyan","Podar International, Khar","Podar International, Powai","Podar International, Santacruz","Podar School (ICSE), Powai","Podar World College, Juhu","Pumpkin House, Gurgaon","R.N.Poddar, Santacruz","Rabindranath World School, Gurgaon","Raghav Global School, Noida","Rajiv Gandhi IT, Versova","Ramaeesh International School, Noida","Ramagya School, Noida","Rawal International School","RBK School, Mira Road","RD National, Bandra","Reliance Foudation School, Koper Khairane","Ridge Valley School, Gurgaon","RIMS International, Andheri","Rithumbara College, Andheri","Rizvi Springfield High School, Khar","RN Podar , Santacruz","RN Podar School, Santacruz","Rockwood Public School, Noida","Rockwoord Public School, Noida","Rustomjee Cambridge International School, Thane","Rustomjee International, Dahisar","Ryan  international school rohini ","Ryan International School, Chembur","Ryan International School, Noida","Ryan International School, VK","Ryan International, Goregaon","Ryan International, Malad","Ryan International, Noida","S.M.Shetty International School, Powai","S.M.Shetty, Powai","Saboo Siddik College Of Engineering, Mumbai Central","Saket College, Kalyan","Salwan Public School, Gurgaon","Sanskar Play School, Noida","Sanskriti School","Sapphire International School, Noida","Sapphire School, Noida","Saraswati Jr College, Thane","sardar patel vidyalaya","Sathaye College, Vile Parle","Scottish High School, Gurgaon","Seth Praful Patel Public School, Kalyan","Shalom Hills International School, Gurgaon","Shemrock Galaxy, Noida","Sherwood Convent School, Gurgaon","Shikshantar School, Gurgaon","Shishuvan","Shiv Nadar School, Noida","Shiv Nadar, GGN","Shiv Nadar, Noida","Shree Swaminarayan Gurukul School, Gandhinagar","Shriram Millenium, Noida","Shriram Millennium School, Noida","Shriram School, Moulsari","SIES College, Sion","Singapore school, Mira road","Somaya College, Vidyavihar","Somerville Noida, Noida","Somerville School, Noida","Somerville, Noida","Sommerville, Vasant Kunj","Springdales Public School, Pusa Road","Springdales School, Dhaula kuan","Springdales School, Pusa Rd","Sri Venkateshwar International School, Dwarka","ST COLUMBAS","St Francis Indirapuram","St Pauls, Hauz Khas","St. Andrews College, Bandra","St. Angels School, Gurgaon","St. Francis Indirapuram, Noida","St. Francis School (ICSE), Borivali","St. Gregorios School, Chembur","St. Josephs Sr. Sec. School, Noida","St. Marys ICSE, Koper Khairane","St. Marys ICSE, Mazagoan","St. Stanislaus High School, Bandra","St. Theresas High School, Bandra","St. Xaviers College Of Arts & Science, CST","St.Annes Jr.College, Malad","St.John High School, Thane","St.Joseph High School , Vikroli","St.Marys ICSE, Mazgaon","St.Stanislaus, Bandra","St.Xaviers High School, Goregaon","Step by Step School, Noida","Step by Step World School, Noida","Summer Fields School","Suncity School, Gurgaon","Sungrace High School, Ghatkoper","SUNRISEVILLE SCHOOL, Noida","SVIS, Kandivili","SVKM, Vile Parle","Swami Vivekanand International School , Kandivili","Tagore International, VV","Thakur College Of Science And Commerce, Kandivili","Thakur Public School, Kandivili","Thakur Vidya mandir, Kandivili","The Ardee School - Junior School, Gurgaon","The Bishops Co-Ed School , Pune","The British School","The HDFC School - Primary SchoolÂ , Gurgaon","The Heritage School, Gurgaon","The Khaitan School, Noida","The Maurya School, Gurgaon","The Millennium School, Noida","The Paras World School, Gurgaon","The Shri Ram Early Years, Gurgaon","The Shri Ram School Aravali","The Shri Ram School, Gurgaon","The Shriram Millennium School, Noida","The Shriram School, Vasant Vihar","The Somaiya School , Ghatkoper","Tridha, Andheri","Udayanchal High School, Vikroli","Universal School, Ghodbunder","Uttrakhand Public School, Noida","V.G.Vaze, Mulund","V.P.M Briol English School, Mulund","Vanasthali Public School, Noida","Vartak College, Vasai","Vasant Valley","Vega Schools, Gurgaon","Venkateshwar International School","Vibgyor High, Goregaon","Vibgyor High, Malad","Vidyanidhi College, Juhu","Vidyavikasani, Vasai","Vishwa Bharati Public School, Noida","Vissanji Academy, Andheri","VVW Arya Vidya mandir, Sion","Witty International, Malad","Yo Poppins Play School & Day Care, Noida"];
			fLen = school_array.length;
			var text='';
			for (i = 0; i < fLen; i++) {
			   if(school_array[i] == ""){
					text += '<option value="">Select School</option>';
				}
				else{
					text += '<option value="'+school_array[i]+'">' + school_array[i] + '</option>';
				}
			}
			$('select[name="sell_do[form][custom][school_name]"]').html(text);
			$('select[name="sell_do[form][custom][school_name]"]').select2({tags:true});
			/*$("select[name='sell_do[form][custom][school_name]']").parent().parent().parent().after("<div style='font-size:11px;color:#000;line-height:14px;'>Incase your School name is missing, input your school name in the box and press enter.</span></div></div>");*/
			// $("select[name='sell_do[form][custom][school_name]']").parent().parent().parent().after("<div class='selldof_field-container selldof_col-xs-12 selldof_col-sm-6 selldof_col-md-6 selldof_col-lg-6'><div style='font-size:11px;color:#000;line-height:14px;position: absolute;top: -16px;'>Incase your School name is missing, input your school name in the box and press enter.</div></div>");

			$("select[name='sell_do[form][custom][school_name]']").parent().append('<div style="font-size:11px;color:#000;line-height:14px;">Incase your School name is missing, input your school name in the box and press enter.</div>');

			$(".selldo_datepicker").parent().remove();
			
			$("[name='sell_do[form][lead][sex]']").parent().parent().parent().after("<div class='selldof_field-container selldof_col-xs-12 selldof_col-sm-6 selldof_col-md-6 selldof_col-lg-6'><div class='form-group'><label style='display:block'>Date of Birth</label><input type='text' data-required='true' data-validator='string'  id='Date' value='' class='textbox datepicker form-control' placeholder='Select Date' readonly><div style='font-size:11px;color:#000;line-height:14px;'>Children between 5 - 17 years of age can only apply.</span></div></div>");
			$(".datepicker").datepicker({yearRange: '1999:2012',changeYear: true,changeMonth: true,defaultDate: '01/01/2012'});
			jQuery("[name='sell_do[form][lead][email]']").after("<input type='hidden' name='sell_do[form][custom][birth_date]'>");
			$("#Date").change(function() {
				jQuery("[name='sell_do[form][custom][birth_date]']").val($("#Date").val());	
			});
			$("[name='sell_do[form][custom][other_school_name]']").parent().parent().hide();


			html = '<div class="selldof_field-container selldof_col-xs-12 selldof_col-sm-12 selldof_col-md-12 selldof_col-lg-12" style="margin-top:20px;">\
					<label>\
						Have a sibling who is also interested? <span class="show-siblings" style="text-decoration:underline;">Click Here</span>\
					</label>\
				</div>';

			$("[name='sell_do[form][custom][sibling_name]']").parents(".selldof_row").prepend(html);

			$("[name='sell_do[form][custom][sibling_name]']").parent().parent().hide();
			$("[name='sell_do[form][custom][sibling_number]']").parent().parent().hide();

			$(".show-siblings").on("click", function(){
				$("[name='sell_do[form][custom][sibling_name]']").parent().parent().toggle();
				$("[name='sell_do[form][custom][sibling_number]']").parent().parent().toggle();
			});
		
  };
});

</script>
<script type="text/javascript">
$(document).ready(function(){
	 if($(window).width()<=700){
	  x = setInterval(function(){
	      if($(".sell_do_virtual_numbers").html().indexOf("+91 11 6661 9488") != -1){
	        clearInterval(x);
	        $(".virtual-number-wrapper").html("<span>Delhi NCR: <span>+91 11 6661 9488</span></span><br/><span>Mumbai: <span>+91 22 6173 9769 </span></span> <br/>");
	      }
	  }, 100);
}else{
x = setInterval(function(){
if($(".sell_do_virtual_numbers").html() == "+91 11 6661 9488"){
	clearInterval(x);
	$(".virtual-number-wrapper").html("<span>Delhi NCR: <span>+91 11 6661 9488</span></span><br/><span>Mumbai: <span>+91 22 6173 9769 </span></span> <br/>");
	}
}, 100);
}	

});
</script>
</body>
</html>
