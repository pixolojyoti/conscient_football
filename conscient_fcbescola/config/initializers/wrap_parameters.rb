ActiveSupport.on_load(:action_controller) do
  include ActionController::ParamsWrapper
  wrap_parameters format: [:json] if respond_to?(:wrap_parameters)
end
