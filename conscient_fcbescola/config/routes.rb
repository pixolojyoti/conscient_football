#ConscientFcbescola::Application.routes.draw do
#  resources :trial_registrations
Rails.application.routes.draw do

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
#  match '/', :to => redirect{|p, req| "http://conscientfootball.com/fcbescola?#{req.params.to_query}"}  ,:via => [:get, :post]
#  match '/trial-registration', :to => redirect{|p, req| "http://conscientfootball.com/fcbescola/trial-registration?#{req.params.to_query}"} ,:via => [:get, :post]
#  match '/privacy-policy', :to => redirect{|p, req| "http://conscientfootball.com/privacy-policy?#{req.params.to_query}"}   ,:via => [:get, :post]
#  match '/social-responsibility', :to => redirect{|p, req| "http://conscientfootball.com/social-responsibility?#{req.params.to_query}"} ,:via => [:get, :post]
#  match '/terms-and-conditions', :to => redirect{|p, req| "http://conscientfootball.com/terms-and-conditions?#{req.params.to_query}"}   ,:via => [:get, :post]
#  match '/careers', :to => redirect{|p, req| "http://conscientfootball.com/careers?#{req.params.to_query}"} ,:via => [:get, :post]
#  match '/regsubmit', :to => redirect{|p, req| "http://conscientfootball.com/fcbescola/?#{req.params.to_query}"}    ,:via => [:get, :post]          

  root "site#index"
    
 get 'mumbai'=>'mumbairegistrations#index'
#  match "/" , :to => "site#index"   , :via => [:get, :post]
#  match "/trial-registration" , :to => "site#registeration" , :via => [:get, :post]
#  match "/privacy-policy" , :to => "site#privacy_policy"    , :via => [:get, :post]
#  match "/social-responsibility" , :to => "site#social_responsibility"  , :via => [:get, :post]
#  match "/terms-and-conditions" , :to => "site#terms_condition" , :via => [:get, :post]
#  match "/careers" , :to => "site#career"   , :via => [:get, :post]
#  match "/regsubmit", :to => "site#regsubmit"   , :via => [:get, :post]
  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
