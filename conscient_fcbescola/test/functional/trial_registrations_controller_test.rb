require 'test_helper'

class TrialRegistrationsControllerTest < ActionController::TestCase
  setup do
    @trial_registration = trial_registrations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:trial_registrations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create trial_registration" do
    assert_difference('TrialRegistration.count') do
      post :create, :trial_registration => @trial_registration.attributes
    end

    assert_redirected_to trial_registration_path(assigns(:trial_registration))
  end

  test "should show trial_registration" do
    get :show, :id => @trial_registration.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @trial_registration.to_param
    assert_response :success
  end

  test "should update trial_registration" do
    put :update, :id => @trial_registration.to_param, :trial_registration => @trial_registration.attributes
    assert_redirected_to trial_registration_path(assigns(:trial_registration))
  end

  test "should destroy trial_registration" do
    assert_difference('TrialRegistration.count', -1) do
      delete :destroy, :id => @trial_registration.to_param
    end

    assert_redirected_to trial_registrations_path
  end
end
