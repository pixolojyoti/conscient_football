# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20170220114510) do

  create_table "assets", :force => true do |t|
    t.string   "data_file_name"
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "group_id"
    t.string   "name"
    t.string   "alt_text"
    t.text     "description"
    t.integer  "asset_order"
    t.integer  "attachable_id"
    t.string   "attachable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clinic_registrations", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "mobileno"
    t.string   "address"
    t.string   "city"
    t.string   "pincode"
    t.string   "state"
    t.string   "gender"
    t.datetime "dob"
    t.string   "school_name"
    t.string   "standard"
    t.string   "food_preference"
    t.string   "guardian_name"
    t.string   "guardian_mobileno"
    t.string   "guardian_email"
    t.string   "guardian_relationship"
    t.string   "adult_size"
    t.boolean  "guardian_terms"
    t.integer  "order_id"
    t.string   "order_status"
    t.string   "tracking_id"
    t.string   "back_ref_no"
    t.string   "failure_message"
    t.string   "billing_address"
    t.string   "billing_city"
    t.string   "billing_state"
    t.string   "billing_zip"
    t.string   "billing_country"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "data_file_name"
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "youth_kit_size"
    t.string   "amount_pay"
  end

  create_table "contacts", :force => true do |t|
    t.string   "name"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "pin"
    t.string   "area_code"
    t.string   "phone"
    t.string   "mobile"
    t.string   "fax"
    t.string   "email"
    t.string   "project"
    t.string   "mailer"
    t.string   "campaign"
    t.string   "occupation"
    t.string   "profession"
    t.string   "service_area"
    t.string   "website"
    t.string   "organization"
    t.string   "designation"
    t.string   "enquiry_about"
    t.string   "entity_type"
    t.string   "entity_id"
    t.string   "page_url"
    t.string   "query"
    t.string   "hidden_data"
    t.string   "department"
    t.text     "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
