$(document).ready(function(){
	
	var aChildren = $(".menuWrapper a"); // find the a children of the list items
	var gap = 0; //Navigation height
	var aArray = []; // create the empty aArray
	for (var i=0; i < aChildren.length; i++) {    
		var aChild = aChildren[i];
		if (!$(aChild).hasClass('extLink')) {
			if ($(aChild).attr('rel')) {
				var ahref = $(aChild).attr('rel');
				aArray.push(ahref);
			}
		}
	}
	
	//On Scroll - Add class active to active tab
	$(window).scroll(function(){
		var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
		var windowHeight = $(window).height(); // get the height of the window
		var docHeight = $(document).height();
		for(i=0;i<aArray.length;i++){
			var theID = aArray[i];
			var divPos = $("#"+theID).offset().top; // get the offset of the div from the top of page
			var divHeight = $("#"+theID).outerHeight(); // get the height of the div in question
			if (windowPos >= (divPos - gap) && windowPos < ((divPos - gap) + divHeight)) {
				$("a[rel='" + theID + "']").addClass("active");
			} else {
				$("a[rel='" + theID + "']").removeClass("active");
			}
		}	
		
		//If document has scrolled to the end. Add active class to the last navigation menu
		if(windowPos + windowHeight == docHeight) {
			if (!$(".menuWrapper a:last-child").hasClass("active")) {
				var navActiveCurrent = $(".active").attr("rel");
				$(".menuWrapper a").removeClass("active");
				$(".menuWrapper a:last-child").addClass("active");
			}
		}
		
	});
	
	//On Click
	$('.menuWrapper a').on("click", function(){
		if(!$(this).hasClass('extLink')) {
			var href = $(this).attr("rel");
			var gap = -5; //Navigation height
	
			$('html,body').animate({
				scrollTop: $("#"+href).offset().top - gap
			}, 1000);
		}
	});
});