class CreateRegistrations < ActiveRecord::Migration
  def self.up
    create_table :registrations do |t|
      t.string :name
      t.string :gender
      t.date :dob
      t.string :email
      t.string :phone
      t.string :school
      t.string :standard
      t.string :kit_size
      t.boolean :terms_and_constions
      t.integer :amount
      t.string :order_id
      t.string   :photo_file_name
      t.string  :photo_content_type
      t.integer   :photo_file_size

      t.timestamps
    end
  end

  def self.down
    drop_table :registrations
  end
end
