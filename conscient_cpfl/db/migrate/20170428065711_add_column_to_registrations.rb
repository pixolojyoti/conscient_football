class AddColumnToRegistrations < ActiveRecord::Migration
  def self.up
    add_column :registrations, :order_status, :string
    add_column :registrations, :tracking_id, :string
    add_column :registrations, :bank_ref_no, :string
    add_column :registrations, :failure_message, :string
  end

  def self.down
    remove_column :registrations, :failure_message
    remove_column :registrations, :bank_ref_no
    remove_column :registrations, :tracking_id
    remove_column :registrations, :order_status
  end
end
