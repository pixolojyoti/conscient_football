class CreateCryptos < ActiveRecord::Migration
  def self.up
    create_table :cryptos do |t|
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :cryptos
  end
end
