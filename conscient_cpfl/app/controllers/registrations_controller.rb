class RegistrationsController < ApplicationController
   layout "admin"
   before_filter :authenticate_user!
  def index
    @registrations=Registration.order("created_at desc").paginate(:page => params[:page], :per_page => 10)
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @registrations }
    end
  end

  # GET /registrations/1
  # GET /registrations/1.xml
  def show
    @registration = Registration.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @registration }
    end
  end

  # GET /registrations/new
  # GET /registrations/new.xml
  def new
    @registration = Registration.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @registration }
    end
  end

  # GET /registrations/1/edit
  def edit
    @registration = Registration.find(params[:id])
  end

  # POST /registrations
  # POST /registrations.xml
  def create
    @registration = Registration.new(params[:registration])

    respond_to do |format|
      if @registration.save
        format.html { redirect_to(@registration, :notice => 'Registration was successfully created.') }
        format.xml  { render :xml => @registration, :status => :created, :location => @registration }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @registration.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /registrations/1
  # PUT /registrations/1.xml
  def update
    @registration = Registration.find(params[:id])

    respond_to do |format|
      if @registration.update_attributes(params[:registration])
        format.html { redirect_to(@registration, :notice => 'Registration was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @registration.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /registrations/1
  # DELETE /registrations/1.xml
  def destroy
    @registration = Registration.find(params[:id])
    @registration.destroy

    respond_to do |format|
      format.html { redirect_to(registrations_url) }
      format.xml  { head :ok }
    end
  end

  def registration_data
    require "csv"
    file = "#{Rails.root}/registration_data.csv"
    scope = Registration.where("created_at >= ? AND created_at <= ?", params[:from_date],params[:to_date].to_date+ 1.days).order("created_at DESC")
    @registration=scope.all
    CSV.open(file, "wb") do |row|
      row << ["Date", "Name", "Gender", "DOB", "Phone", "Email", "School", "Standard", "Kit Size","Order ID"]
      @registration.each_with_index do |n, index|
        row << [n.created_at.to_s(:notification), n.name, n.gender, n.dob, n.phone, n.email, n.school, n.standard, n.kit_size, n.order_id]
      end
    end
    File.open(file, 'r') do |f|
      send_data f.read, :type => "text/csv", :filename => "registration_data.csv"
    end

   
  end
end
