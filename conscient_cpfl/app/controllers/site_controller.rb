class SiteController < ApplicationController
	def index
	end

	def registeration
		render :layout => false
	end

  def careers
  end

  def social_responsiblity
    
  end

  def terms_condition

  end

  def privacy_policy
    
  end
  def create_registration
    @registration = Registration.new(params[:registration])
    @registration.order_status="Pending"
    if @registration.save
      FormNotifier.notify_registration_admin(@registration, "Registration  Successful").deliver       
      FormNotifier.notify_registration_end_user(@registration, "Registration  Successful").deliver
      render :json => {:status => "success", :mesg => "Thankyou", :registration => @registration}
    else
      render :json => {:status => "error", :error => @registration.errors.full_messages}
    end
  end
  def proceed_to_pay
    @registration = Registration.find_by_order_id(params[:id]) rescue nil
      
  end
  def ccavRequestHandler   
      @registration = Registration.find_by_order_id(params[:order_id]) rescue nil
      if @registration.present?        
      merchant_id="295"
      amount="200000"
      currency="INR"
      language="EN"      
      redirecturl="http://#{request.host}/site/payment_details"
      cancelurl="http://#{request.host}/site/payment_details"
      merchantData=""    
      working_key="2E7B74F0E173E6AF32F1D4189264E82E"   #Put in the 32 Bit Working Key provided by CCAVENUES.  
      @access_code="AVSR00ED98CF60RSFC"   #Put 
      params.each do |key,value|
        if key=="merchant_id"
          merchantData += key+"="+merchant_id+"&" 
        elsif key=="order_id"
          merchantData += key+"="+@registration.order_id+"&" 
        elsif key=="amount"
          merchantData += key+"="+amount+"&" 
        elsif key=="currency"
          merchantData += key+"="+currency+"&" 
        elsif key=="language"
          merchantData += key+"="+language+"&" 
        elsif key=="billing_name"
          merchantData += key+"="+@registration.name+"&" 
        elsif key=="billing_tel"
          merchantData += key+"="+@registration.phone+"&" 
        elsif key=="billing_email"
          merchantData += key+"="+@registration.email+"&"         
        elsif key=="redirect_url"
          merchantData += key+"="+redirecturl+"&" 
        elsif key=="cancel_url"
          merchantData += key+"="+cancelurl+"&" 
        else          
          merchantData += key+"="+value+"&" 
        end
      end   
      crypto = Crypto.new
      @encrypted_data = crypto.encrypt(merchantData,working_key)
      else

      end
  end

  def payment_details
    if params[:encResp]
      workingKey="2E7B74F0E173E6AF32F1D4189264E82E"#Put in the 32 Bit Working Key provided by CCAVENUES.   
      encResponse=params[:encResp]
      crypto = Crypto.new 
      @decResp=crypto.decrypt(encResponse,workingKey) rescue ''
     # decResp1=crypto.decrypt(encResponse,workingKey);
      @decResp = @decResp.split("&") rescue []
      @decResp.each do |key|
        if key.from(0).to(key.index("=")-1)=='amount'
          @amount=key.from(key.index("=")+1).to(-1)  
        end   
        if key.from(0).to(key.index("=")-1)=='order_status'
          @order_status=key.from(key.index("=")+1).to(-1) 
        end
        if key.from(0).to(key.index("=")-1)=='tracking_id'
          @tracking_id=key.from(key.index("=")+1).to(-1)  
        end
        if key.from(0).to(key.index("=")-1)=='bank_ref_no'
          @bank_ref_no=key.from(key.index("=")+1).to(-1)  
        end
        if key.from(0).to(key.index("=")-1)=='failure_message'
          @failure_message=key.from(key.index("=")+1).to(-1)  
        end                   
        # if key.from(0).to(key.index("=")-1)=='billing_address'
        #   @billing_address=key.from(key.index("=")+1).to(-1)  
        # end
        # if key.from(0).to(key.index("=")-1)=='billing_city'
        #   @billing_city=key.from(key.index("=")+1).to(-1) 
        # end
        # if key.from(0).to(key.index("=")-1)=='billing_state'
        #   @billing_state=key.from(key.index("=")+1).to(-1)  
        # end
        # if key.from(0).to(key.index("=")-1)=='billing_zip'
        #   @billing_zip=key.from(key.index("=")+1).to(-1)  
        # end
        # if key.from(0).to(key.index("=")-1)=='billing_country'
        #   @billing_country=key.from(key.index("=")+1).to(-1)  
        # end
        if key.from(0).to(key.index("=")-1)=='order_id'
          @order_id=key.from(key.index("=")+1).to(-1) 
        end                                       
      end 
        #Rails.logger.info "#{@order_id}---- #{@order_status}----#{@billing_zip}"
      if @order_id.present?        
        @registration = Registration.where('order_id=?',@order_id).last
        if @registration.present?
          @registration.update_attributes(:amount=>@amount,:order_status => @order_status,:tracking_id => @tracking_id,:bank_ref_no => @bank_ref_no,:failure_message => @failure_message)
          #@cpfl_reg1 = Registration.where('order_id=?',@order_id).first
          if @order_status=='Success'
          end
        end
      end
    else
      redirect_to "/"
    end
  end
  def cancel_transaction
  
  end


	def set_cookies
    tmp = params[:srd]
    if cookies[:srd].blank?
      if tmp.present?
        cookies[:srd] = tmp
      else
        cookies[:srd] = "website"
      end
    end
  end
  
end
