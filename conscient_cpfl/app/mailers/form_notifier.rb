class FormNotifier < ActionMailer::Base
  add_template_helper(ApplicationHelper)
  default :from => "\"conscient_cpfl\" <no-reply@conscient_cpfl.com>"
  include ApplicationHelper

  def notify_registration_end_user(registration, subj="Registration Successful")
    @registration = registration
    mail(:subject => subj, :to => @registration.email )
  end
  def notify_registration_admin (registration, subj="Registration Successful")
    @registration = registration
    if RAILS_ENV == "development"
      to_mail = "sonu@amuratech.com"
    else
      to_mail = "qa@amuratech.com"
    end
    mail(:subject => subj, :to => to_mail)
  end
end
